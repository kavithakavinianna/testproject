package testData;

import java.util.Locale;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.github.javafaker.Faker;

import configuration.base.TestBase;
import configuration.driverManager.DriverSettings;

public class OoduTestBase extends TestBase {
	
	public Faker faker;

	@BeforeMethod
	public void beforeClass() {
		faker = new Faker(new Locale("en-US"));
		DriverSettings settings = new DriverSettings();		
		
		settings.webDriverType = testEnvironment.getProperty("browsertype");
		webApplication.launch(settings);
	}

	@AfterMethod
	public void afterClass() {
	}
}

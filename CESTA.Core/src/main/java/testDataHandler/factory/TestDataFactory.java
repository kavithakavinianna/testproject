package testDataHandler.factory;

import testDataHandler.ExcelTestDataReader;
import testDataHandler.JsonTestDataReader;
import testDataHandler.interfaces.ITestDataReader;

/**
 * 
 * Test Data Factory class to set the Test Data class object into Singleton
 * based on user input(Test Data type)and get where ever we required in the
 * entire framework.
 *
 * @author
 */
public class TestDataFactory {

	private ITestDataReader DataReader = null;

	/**
	 * 
	 * Method to set and get the Logger object based on the user
	 * input(loggrType) exception.
	 *
	 * @author
	 */
	public ITestDataReader getDataReader(String readerType) {

		switch (readerType.toLowerCase()) {
		case "xlsx":
			DataReader = new ExcelTestDataReader();
			break;
		case "json":
			DataReader = new JsonTestDataReader();
			break;
		default:
			throw new IllegalArgumentException("Invalid test data reader type provided : " + readerType);
		}
		return DataReader;
	}
}

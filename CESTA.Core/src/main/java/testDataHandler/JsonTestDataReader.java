package testDataHandler;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;

import testDataHandler.interfaces.ITestDataReader;
/**
 * JsonTestDataReader class which implements ITestDataReader interface
 * 
 * @author Appu
 */
public class JsonTestDataReader implements ITestDataReader {

	private JSONObject dataObj;

	@Override
	public void loadDataFile(String fullFilePath) {
		JSONParser parser = new JSONParser();
		try {
			FileInputStream fis = new FileInputStream(fullFilePath);
			InputStreamReader is = new InputStreamReader(fis);
			dataObj = (JSONObject) parser.parse(is);
			is.close();
			fis.close();
		} catch (Exception e) {
			Assert.fail("Exception occured while loading test data. Error message : " + e.getMessage()
			+ " :: And testdata location is : " + fullFilePath);
		}
	}

	@Override
	public int getDatasetCount() {
		return dataObj.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getDataset(int rowNum) {
		Map<String, String> dataset = new HashMap<String, String>();
		String dataObjName = "TestDataset_" + rowNum;
		JSONObject datasetObj = (JSONObject) dataObj.get(dataObjName);
		dataset = (HashMap<String, String>) datasetObj;
		return dataset;
	}
}

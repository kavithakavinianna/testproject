package testDataHandler.interfaces;

import java.util.Map;

/**
 * ITestDataReader interface consists of common test data function which can be
 * overridden by any type of Test Data and implement there own functionalities
 * 
 * @author Appu
 */
public interface ITestDataReader {
	/**
	 * Method to load Data File
	 * 
	 * @param fullFilePath
	 */
	void loadDataFile(String fullFilePath);

	/**
	 * Method to get Dataset Count
	 * 
	 * @return int
	 */
	int getDatasetCount();

	/**
	 * Method to get Data set
	 * 
	 * @return Map<String, String>
	 */
	Map<String, String> getDataset(int rowNum);
}
package testDataHandler;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.testng.Assert;

import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;
import testDataHandler.factory.TestDataFactory;
import testDataHandler.interfaces.ITestDataReader;

public class TestData {

	private ITestDataReader TestDataReader = null;
	public Map<String, String> dataSet = new HashMap<String, String>();

	public TestData(Map<String, String> dataSet) {
		this.dataSet = dataSet;
	}

	public TestData() {
		// TODO Auto-generated constructor stub
	}

	public void loadDataFile(String fileName) {
		String testDataLocation = "";
		try {
			File f = new File(fileName);

			if (f.exists() && !f.isDirectory()) {
				testDataLocation = fileName;
			} else {
				String sourcePath = Singleton.getInstance(TestEnvironment.class).TestDataSource;
				testDataLocation = Paths.get(sourcePath, fileName).normalize().toString();
			}

			String testDataFileType = FilenameUtils.getExtension(testDataLocation);

			TestDataFactory reader = new TestDataFactory();
			TestDataReader = reader.getDataReader(testDataFileType);
			TestDataReader.loadDataFile(testDataLocation);
		} catch (Exception e) {
			Assert.fail("Exception occured while loading test data. Exception message is :" + e.getMessage()
					+ " :: And testdata location is : " + testDataLocation);
		}
	}

	public int getDatasetCount() {
		int datasetCount = 0;
		if (TestDataReader == null) {
			Assert.fail("Please load the sheet before trying to get the current row count from test data");
		} else {
			datasetCount = TestDataReader.getDatasetCount();
		}
		return datasetCount;
	}

	public TestData getDataSet(int rowNum) {
		TestData testdata = new TestData(TestDataReader.getDataset(rowNum));
		return testdata;
	}

	public String get(String key) {
		key = key.trim();
		String value = null;
		if (dataSet.containsKey(key)) {
			value = dataSet.get(key);
		}
		return value;
	}

	public void set(String key, String value) {
		key = key.trim();
		dataSet.put(key, value);
	}
}
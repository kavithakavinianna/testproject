package testDataHandler;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;

import testDataHandler.interfaces.ITestDataReader;

/**
 * ExcelTestDataReader class which implements ITestDataReader interface
 * 
 * @author Appu
 */
public class ExcelTestDataReader implements ITestDataReader {

	private XSSFWorkbook wb = null;
	private XSSFSheet sh = null;
	private XSSFRow row = null;
	private XSSFCell cell = null;

	@Override
	public void loadDataFile(String fullFilePath) {
		try {
			FileInputStream fs = new FileInputStream(fullFilePath);
			wb = new XSSFWorkbook(fs);
			sh = wb.getSheetAt(0);
			fs.close();
		} catch (Exception e) {
			Assert.fail("Exception occured while loading Data file. Error message is " + e.getMessage());
		}
	}

	@Override
	public int getDatasetCount() {
		int datasetCount = 0;
		int lastRow = sh.getPhysicalNumberOfRows();
		int lastCol = sh.getRow(0).getLastCellNum();
		for (int i = 0; i < lastRow; i++) {
			row = sh.getRow(i);
			String rowText = "";
			for (int j = 0; j < lastCol; j++) {
				cell = row.getCell(j);
				rowText = rowText + getCellData(cell);
			}
			if (rowText.trim().isEmpty()) {
				break;
			}
			datasetCount = i;
		}
		return datasetCount;
	}

	@Override
	public Map<String, String> getDataset(int rowNum) {
		Map<String, String> dataSet = new HashMap<String, String>();
		row = sh.getRow(rowNum);
		int lastCol = sh.getRow(0).getLastCellNum();
		for (int i = 0; i < lastCol; i++) {
			cell = sh.getRow(0).getCell(i);
			String key = getCellData(cell);
			cell = row.getCell(i);
			String value = getCellData(cell);
			dataSet.put(key, value);
		}
		return dataSet;
	}

	private String getCellData(Cell cell) {
		FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
		DataFormatter formatter = new DataFormatter();
		String value = formatter.formatCellValue(cell, evaluator);
		return value;
	}
}

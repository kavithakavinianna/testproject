package exceptionHandler;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.testng.Assert;

public class CustomExceptionHandler {

	private static FileReader reader;
	private static Properties prop;
	private static Map<String, String> Exceptions = new HashMap<String, String>();

	static {
		loadExceptionMessages();
	}

	public static void loadExceptionMessages() {
		String ExceptionNameFromEnum = "";
		String ExceptionMessage = "";
		// Array for Enumeration Values
		CommonExceptions enumarray[] = CommonExceptions.values();
		try {
			reader = new FileReader("./ExceptionMessages.properties");
			prop = new Properties();
			prop.load(reader);
			// For Exceptions in enumeration get the exception messages from property
			// file
			// and add them to Map Object
			for (CommonExceptions exceptionName : enumarray) {
				ExceptionNameFromEnum = exceptionName.getExceptionName(exceptionName);
				ExceptionMessage = prop.getProperty(ExceptionNameFromEnum);
				// Exceptions.put(ExceptionNameFromEnum,ExceptionMessage );
				if (prop.containsKey(exceptionName.toString())) {
					Exceptions.put(ExceptionNameFromEnum,
							ExceptionMessage == null || ExceptionMessage.trim().length() == 0
									? "No Exception Message in Exception Property file"
									: ExceptionMessage);
				}
			}
		} catch (Exception e) {
			Assert.fail("Exception occured while loading exception messages", e);
		}
	}

	public static void ExceptionHandler(Exception e) {
		String ExceptionName = e.getClass().getSimpleName();
		// checking For Exception in Map Object using ExceptionName as key
		if (Exceptions.containsKey(ExceptionName)) {
			Assert.fail(Exceptions.get(ExceptionName), e);
		} else {
			Assert.fail(e.getMessage(), e);
		}
	}

	public static void ExceptionHandler(Exception e, String ExceptionMessage) {
		// checking For Exception in Map Object using ExceptionName as key
		Assert.fail(ExceptionMessage, e);
	}

	public static void ExceptionHandler(Exception e, String ExceptionMessage, boolean throwException) {
		// checking For Exception in Map Object using ExceptionName as key
		if (!throwException) {
			Assert.fail(ExceptionMessage, e);
		} else {
			System.err.println(ExceptionMessage);
			e.printStackTrace();
		}
	}
}
package exceptionHandler;

/**
 * 
 * This enum represents all the available Common Exceptions
 *
 * @author Sreenivas Kilari
 */
public enum CommonExceptions {

	TimeoutException,

	NoSuchCookieException,

	NoSuchElementException,

	NoSuchFrameException,

	NoSuchWindowException,

	NotFoundException,

	ArrayIndexOutOfBoundsException,

	ArithmeticException,

	ElementClickInterceptedException,

	ElementNotInteractableException,

	ElementNotSelectableException,

	ElementNotVisibleException,

	FileNotFoundException,

	IndexOutOfBoundsException,

	IOException,

	JavascriptException,

	NoAlertPresentException,

	NoSuchSessionException,

	NullPointerException,

	SessionNotCreatedException,

	StaleElementReferenceException,

	UnhandledAlertException,

	WebDriverException;
	/**
	 * Method to get the exception name
	 * 
	 * @param commonExcepName
	 * @return
	 */
	public String getExceptionName(CommonExceptions commonExcepName) {
		return commonExcepName.toString();
	}
}

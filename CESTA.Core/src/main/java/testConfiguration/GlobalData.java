package testConfiguration;

import java.util.HashMap;
import java.util.Map;

/**
 * This class file represents setting, getting and resetting the global test
 * data
 * 
 * @author
 *
 */
public class GlobalData {

	private static ThreadLocal<Map<String, String>> globalData = new ThreadLocal<Map<String, String>>();

	/**
	 * Method to set the global data as a key value pair
	 * 
	 * @param key
	 * @param value
	 */
	public static void set(String key, String value) {

		if (getDatamapThread().get().containsKey(key))
			getDatamapThread().get().remove(key);
		getDatamapThread().get().put(key, value);
	}

	/**
	 * Method to get the value of global test data by using key
	 * 
	 * @param key
	 * @return
	 */
	public static String get(String key) {
		String val = getDatamapThread().get().get(key).toString();
		if (!val.equals(null))
			return val;
		return "";
	}

	/**
	 * Method to reset the global test data
	 */
	public static void reset() {
		if (getDatamapThread().get() != null) {
			getDatamapThread().get().clear();
		}
	}

	/**
	 * Method to get data map thread of global test data
	 * 
	 * @return
	 */
	private static ThreadLocal<Map<String, String>> getDatamapThread() {
		if (globalData.get() == null) {
			Map<String, String> map = new HashMap<String, String>();
			globalData.set(map);
		}
		return globalData;
	}
}

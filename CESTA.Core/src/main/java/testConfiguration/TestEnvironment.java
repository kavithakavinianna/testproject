package testConfiguration;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map.Entry;

import exceptionHandler.CustomExceptionHandler;
import loggers.interfaces.ILogger;
import parsers.PropertiesParser;

/**
 * Class file represents setting up the Test Environment variables
 * 
 * @author Appu
 *
 */
public class TestEnvironment {

	public String EnvironmentName;

	public String ProjectPath;

	public String TestDataSource;

	public int ImplicitWaitTimeout;

	public int ExplicitWaitTimeout;

	public int PageLoadTimeout;

	public int ScriptTimeout;

	public int SmallWait;

	public int MediumWait;

	public int LargeWait;

	private ILogger log;

	public boolean WebNewSessionPerTest;

	public boolean MobileNewSessionPerTest;

	private String apiServer;

	private String apiPort;

	private String OS = null;

	public String getOsName() {
		if (OS == null) {
			OS = System.getProperty("os.name").toLowerCase().toString();
		}
		return OS;
	}

	private HashMap<String, String> AppSettings = new HashMap<String, String>();

	/**
	 * Constructor to load properties and get the logger object instance
	 */
	public TestEnvironment() {
		log = Singleton.getInstance(ILogger.class);
		loadProperties();
	}

	/**
	 * Method to get the Property
	 * 
	 * @param key
	 * @return
	 */
	public String getProperty(String key) {
		String value = null;
		key = key.toLowerCase();
		try {
			if (AppSettings.containsKey(key)) {
				value = AppSettings.get(key);
			}
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return value;
	}

	/**
	 * Method to set the Property
	 * 
	 * @param key
	 * @param value
	 */
	public void setProperty(String key, String value) {
		key = key.toLowerCase();
		try {
			if (AppSettings.containsKey(key)) {
				AppSettings.replace(key, value);
			} else {
				AppSettings.put(key, value);
			}
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
	}

	/**
	 * Method to check the property is exist or not
	 * 
	 * @param key
	 * @return
	 */
	public boolean containsProperty(String key) {
		boolean result = false;
		key = key.toLowerCase();
		try {
			result = AppSettings.containsKey(key);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return result;
	}

	/**
	 * Method to log Loaded Properties Activity
	 * 
	 * @param dataType
	 */
	public void logLoadedPropertiesActivity(String dataType) {
		log.info(
				"Getting value of " + dataType + " from testconfig.local.properties file  :  " + getProperty(dataType));
	}

	/**
	 * Method to log Default Properties Activity
	 * 
	 * @param dataType
	 * @param defaultValue
	 */
	public void logDefaultPropertiesActivity(String dataType, Object defaultValue) {
		log.info(dataType + " value not provided in testconfig.local.properties, so taking default vlaue  : "
				+ defaultValue);
	}

	/**
	 * Method to load Properties
	 */
	public void loadProperties() {
		try {
			log.info("Loading Properties");
			EnvironmentName = Singleton.getInstance(PropertyFileLoader.class).EnvironmentName;
			PropertiesParser propertiesParser = Singleton.getInstance(PropertyFileLoader.class).propertiesParser;

			for (Entry<String, String> entry : propertiesParser.getPropertiesMap().entrySet()) {
				String key = entry.getKey().toLowerCase();
				String value = entry.getValue();
				AppSettings.put(key, value);
			}
			log.info("Successfully loaded the properties file Loading Properties");
			if (containsProperty("ImplicitWaitTimeout")) {
				ImplicitWaitTimeout = Integer.parseInt(getProperty("ImplicitWaitTimeout"));
				logLoadedPropertiesActivity("ImplicitWaitTimeout");
			} else {
				ImplicitWaitTimeout = 15;
				logDefaultPropertiesActivity("ImplicitWaitTimeout", ImplicitWaitTimeout);
			}
			if (containsProperty("ExplicitWaitTimeout")) {
				ExplicitWaitTimeout = Integer.parseInt(getProperty("ExplicitWaitTimeout"));
				logLoadedPropertiesActivity("ExplicitWaitTimeout");
			} else {
				ExplicitWaitTimeout = 120;
				logDefaultPropertiesActivity("ExplicitWaitTimeout", ExplicitWaitTimeout);
			}
			if (containsProperty("PageLoadTimeout")) {
				PageLoadTimeout = Integer.parseInt(getProperty("PageLoadTimeout"));
				logLoadedPropertiesActivity("PageLoadTimeout");
			} else {
				PageLoadTimeout = 180;
				logDefaultPropertiesActivity("PageLoadTimeout", PageLoadTimeout);
			}

			if (containsProperty("ScriptTimeout")) {
				ScriptTimeout = Integer.parseInt(getProperty("ScriptTimeout"));
				logLoadedPropertiesActivity("ScriptTimeout");
			} else {
				ScriptTimeout = 80;
				logDefaultPropertiesActivity("ScriptTimeout", ScriptTimeout);
			}

			if (containsProperty("SmallWait")) {
				SmallWait = Integer.parseInt(getProperty("SmallWait"));
				logLoadedPropertiesActivity("SmallWait");
			} else {
				SmallWait = 10;
				logDefaultPropertiesActivity("SmallWait", SmallWait);
			}

			if (containsProperty("MediumWait")) {
				MediumWait = Integer.parseInt(getProperty("MediumWait"));
				logLoadedPropertiesActivity("MediumWait");
			} else {
				MediumWait = 30;
				logDefaultPropertiesActivity("MediumWait", MediumWait);
			}

			if (containsProperty("LargeWait")) {
				LargeWait = Integer.parseInt(getProperty("LargeWait"));
				logLoadedPropertiesActivity("LargeWait");
			} else {
				LargeWait = 60;
				logDefaultPropertiesActivity("LargeWait", LargeWait);
			}

			ProjectPath = System.getProperty("user.dir");

			if (containsProperty("TestDataSource")) {
				TestDataSource = getProperty("TestDataSource");
				File tempFile = new File(TestDataSource);
				if (!tempFile.exists()) {
					TestDataSource = Paths.get(ProjectPath, TestDataSource).normalize().toString();
				}
				logLoadedPropertiesActivity("TestDataSource");
			} else {
				TestDataSource = ProjectPath;
				logDefaultPropertiesActivity("TestDataSource", ProjectPath);
			}

			if (containsProperty("WebNewSessionPerTest")) {
				WebNewSessionPerTest = Boolean.parseBoolean(getProperty("WebNewSessionPerTest"));
				logLoadedPropertiesActivity("WebNewSessionPerTest");
			} else {
				logDefaultPropertiesActivity("WebNewSessionPerTest", true);
			}

			if (containsProperty("MobileNewSessionPerTest")) {
				MobileNewSessionPerTest = Boolean.parseBoolean(getProperty("MobileNewSessionPerTest"));
				logLoadedPropertiesActivity("MobileNewSessionPerTest");
			} else {
				logDefaultPropertiesActivity("MobileNewSessionPerTest", true);
			}
			// TODO some changes
			if (containsProperty("APIServer")) {
				apiServer = getProperty("APIServer");
				logLoadedPropertiesActivity("APIServer");
			} else {
				apiServer = "192.168.28.74";
				logDefaultPropertiesActivity("APIServer", apiServer);
			}
			// TODO some changes
			if (containsProperty("APIPort")) {
				apiPort = getProperty("APIPort");
				logLoadedPropertiesActivity("APIPort");
			} else {
				apiPort = "5000";
				logDefaultPropertiesActivity("MongoDBPort", apiPort);
			}

		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error while trying to read data from properties file. ");
		}
	}
}

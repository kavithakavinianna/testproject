package testConfiguration;

import exceptionHandler.CustomExceptionHandler;
import parsers.PropertiesParser;

/**
 * Class file to load the properties file
 * 
 * @author shaikmahammeds
 *
 */
public class PropertyFileLoader {

	public String EnvironmentName;
	public PropertiesParser propertiesParser;

	public PropertyFileLoader() {
		loadPropertyFile();
	}

	/**
	 * Method to load the properties file
	 */
	private void loadPropertyFile() {
		String configFileName = "./testconfig.%1$s.properties";
		try {

			EnvironmentName = System.getProperty("environment");
			if (EnvironmentName == null)
				EnvironmentName = "local";

			configFileName = String.format(configFileName, EnvironmentName);
			propertiesParser = new PropertiesParser(configFileName);

		} catch (Exception ex) {
			CustomExceptionHandler.ExceptionHandler(ex, "Error while trying to load property file: " + configFileName);
		}
	}
}

package parsers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import exceptionHandler.CustomExceptionHandler;

import java.util.Properties;

/**
 * This class file represents data parsing from Properties file which consist of
 * Properties Parser like getProperties, getPropertiesMap, getProperty,
 * containsProperty, setProperty and setProperties
 * 
 * @author
 */
public class PropertiesParser {

	private Map<String, String> PropertiesMap;
	private Properties properties;
	private File Propfile;

	/**
	 * Constructor to read the properties from properties file.
	 * 
	 * @return Properties
	 */
	public PropertiesParser(String fileName) {
		try {
			properties = new Properties();
			Propfile = new File(fileName);
			FileInputStream propFile = new FileInputStream(Propfile);
			properties.load(propFile);

			PropertiesMap = new HashMap<String, String>();

			for (Entry<Object, Object> entry : properties.entrySet()) {
				String key = entry.getKey().toString().toLowerCase();
				String value = entry.getValue().toString();
				PropertiesMap.put(key, value);
			}

		} catch (IOException e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occured while trying to read properties from properties file.");
		}
	}

	/**
	 * Method to get the properties
	 * 
	 * @return Properties
	 */
	public Properties getProperties() {
		return properties;
	}

	/**
	 * Method to get the properties in as a Map
	 * 
	 * @return Map<String, String>
	 */
	public Map<String, String> getPropertiesMap() {
		return PropertiesMap;
	}

	/**
	 * Method to get the property
	 * 
	 * @param String
	 *            key
	 * @return String property value
	 */
	public String getProperty(String key) {
		key = key.toLowerCase();
		return PropertiesMap.get(key);
	}

	/**
	 * Method to check the property
	 * 
	 * @param String
	 *            key
	 * @return boolean property present or not
	 */
	public boolean containsProperty(String key) {
		return properties.containsKey(key.toLowerCase());
	}

	/**
	 * Method to set property
	 * 
	 * @param String
	 *            key, String value
	 */
	public void setProperty(String key, String value) {
		OutputStream output = null;
		key = key.toLowerCase();
		try {
			output = new FileOutputStream(Propfile);
			if (properties.containsKey(key)) {
				properties.remove(key);
			}
			properties.setProperty(key, value);
			properties.store(output, null);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occured while trying to write into properties file.");
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					CustomExceptionHandler.ExceptionHandler(e);
				}
			}
		}
	}

	/**
	 * Method to set properties
	 * 
	 * @param Map<Object,
	 *            Object>
	 */
	public void setProperties(Map<Object, Object> map) {
		for (Entry<Object, Object> entry : map.entrySet()) {
			String key = entry.getKey().toString();
			String value = entry.getValue().toString();
			setProperty(key, value);
		}
	}
}

package parsers;

import org.testng.Assert;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class file represents data parsing from CSV file which consist of the
 * functions like JsonVerify, loadJson and convertJsonFileToMap
 * 
 * @author
 */
public class CsvParser {
	private char recordDelimiter;
	private char bracketDelimiter;
	private boolean hasHeader;
	private String filePath;
	private HashMap<String, Integer> columns;

	public CsvParser(char recordDelimiter, char bracketDelimiter, boolean hasHeader, String filePath)
			throws IOException {
		this.recordDelimiter = recordDelimiter;
		this.bracketDelimiter = bracketDelimiter;
		this.hasHeader = hasHeader;
		this.filePath = filePath;
		if (hasHeader) {
			columns = new HashMap<>();
			BufferedReader reader = new BufferedReader(new FileReader(this.filePath));
			String line = reader.readLine();
			reader.close();
			String[] headers = line.split("" + recordDelimiter); // Header does
																	// not
																	// include
																	// Quotes
			for (int i = 0; i < headers.length; i++) {
				columns.put(headers[i], i);
			}
		}
	}

	private int getColumnPositionFromName(String columnName) {
		if (!hasHeader)
			Assert.fail("Cannot use a column name because the file did not contain a header row");
		if (!columns.containsKey(columnName))
			Assert.fail("No column named " + columnName + " in the csv file");
		return columns.get(columnName);
	}

	public boolean containsARecordWithAtLeastOneValueInColumn(List<String> values, String columnName)
			throws IOException {
		if (values.size() == 0)
			Assert.fail("No values were passed in to find in column " + columnName);
		int columnPosition = getColumnPositionFromName(columnName);
		return containsARecordWithAtLeastOneValueInColumn(values, columnPosition);
	}

	public boolean containsARecordWithAtLeastOneValueInColumn(List<String> values, int columnPosition)
			throws IOException {
		if (values.size() == 0)
			Assert.fail("No values were passed in to find in column number (starting with 0) " + columnPosition);
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader(new FileReader(this.filePath));
		boolean readFirstLine = false;
		String line = "";
		while (line != null) {
			line = reader.readLine();
			if (hasHeader && !readFirstLine) {
				line = reader.readLine();
			}
			readFirstLine = true;
			List<String> lineItemArray = splitWithDelimiters(line); // to deal
																	// with
																	// quotes in
																	// the body
			for (int i = 0; i < values.size(); i++) {
				if (lineItemArray.get(columnPosition).equalsIgnoreCase(values.get(i))) {
					return true;
				}
			}
		}
		return false;
	}

	private List<String> splitWithDelimiters(String lineToSplit) {
		List<String> result = new ArrayList<>();
		int start = 0;
		boolean inQuotes = false;
		for (int current = 0; current < lineToSplit.length(); current++) {
			if (lineToSplit.charAt(current) == bracketDelimiter)
				inQuotes = !inQuotes; // toggle state
			boolean atLastChar = (current == lineToSplit.length() - 1);
			if (atLastChar)
				result.add(lineToSplit.substring(start).replace("" + bracketDelimiter, ""));
			else if (lineToSplit.charAt(current) == recordDelimiter && !inQuotes) {
				result.add(lineToSplit.substring(start, current).replace("" + bracketDelimiter, ""));
				start = current + 1;
			}
		}
		return result;
	}

	public BigDecimal getSumOfColumn(String columnName) throws IOException {
		int columnPosition = getColumnPositionFromName(columnName);
		BigDecimal result = getSumOfColumn(columnPosition);

		return result;
	}

	public BigDecimal getSumOfColumn(int columnPosition) throws IOException {
		BigDecimal sum = new BigDecimal(0);
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader(new FileReader(this.filePath));
		boolean readFirstLine = false;
		String line = "";
		while (line != null) {
			line = reader.readLine();
			if (hasHeader && !readFirstLine) {
				line = reader.readLine();
			}
			readFirstLine = true;
			if (line != null && !line.trim().isEmpty()) {
				List<String> lineItemArray = splitWithDelimiters(line); // to
																		// deal
																		// with
																		// quotes
																		// in
																		// the
																		// body
				sum = sum.add(ConvertToBigDecimal(lineItemArray.get(columnPosition)));
			}
		}
		return sum;
	}

	public static BigDecimal ConvertToBigDecimal(String s) {
		// Converting to BigDecimal instead of float for tests that need to
		// perform arithmetic
		// Float is not precise enough to perform calculations
		if (s.isEmpty())
			throw new IllegalArgumentException("Failed to convert an empty string to a number.");
		String value = s.replace("$", "").replace(",", "").trim();
		if (value.contains("(") && value.contains(")")) {
			return new BigDecimal(value.replace("(", "") // Removing parenthesis
					.replace(")", "") // Removing parenthesis
					.replace(",", "") // Removing commas
					.replace("$", "") // Removing dollar sign
			).multiply(new BigDecimal(-1)); // Making negative value
		}
		return new BigDecimal(value.replace("(", "") // Removing parenthesis
				.replace(")", "") // Removing parenthesis
				.replace(",", "") // Removing commas
				.replace("$", "")); // Removing dollar sign
	}

	public boolean hasEmptyStringValueInColumn(String columnName) throws IOException {
		int columnPosition = getColumnPositionFromName(columnName);
		return hasEmptyStringValueInColumn(columnPosition);
	}

	private boolean hasEmptyStringValueInColumn(int columnPosition) throws IOException {
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader(new FileReader(this.filePath));
		boolean readFirstLine = false;
		String line = "";
		while (line != null) {
			line = reader.readLine();
			if (hasHeader && !readFirstLine) {
				line = reader.readLine();
			}
			readFirstLine = true;

			if (line != null && !line.trim().isEmpty()) {
				List<String> lineItemArray = splitWithDelimiters(line); // to
																		// deal
																		// with
																		// quotes
																		// in
																		// the
																		// body
				if (lineItemArray.get(columnPosition).isEmpty()) {
					return true;
				}
			}
		}
		return false;
	}

	public String getHighestValueByLookUpKeyUsingColumnNames(String lookupKey, String lookupColumnName,
			String valueColumnName) throws IOException {
		int lookupColumnPosition = getColumnPositionFromName(lookupColumnName);
		int valueColumnPosition = getColumnPositionFromName(valueColumnName);
		BigDecimal highestValue = new BigDecimal("0");
		System.out.println(highestValue.toPlainString());
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader(new FileReader(this.filePath));
		boolean readFirstLine = false;
		String line = "";
		while (line != null) {
			line = reader.readLine();
			if (hasHeader && !readFirstLine) {
				line = reader.readLine();
			}
			readFirstLine = true;

			if (line != null && !line.trim().isEmpty()) {
				List<String> lineItemArray = splitWithDelimiters(line); // to
																		// deal
																		// with
																		// quotes
																		// in
																		// the
																		// body
				if (lineItemArray.get(lookupColumnPosition).equalsIgnoreCase(lookupKey)) {
					BigDecimal currentValue = new BigDecimal(lineItemArray.get(valueColumnPosition));
					if (currentValue.compareTo(highestValue) == 1) {
						highestValue = currentValue;
					}
				}
			}
		}
		System.out.println(highestValue.toPlainString());
		return highestValue.toPlainString();
	}
}
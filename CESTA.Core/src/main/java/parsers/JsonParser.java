package parsers;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import exceptionHandler.CustomExceptionHandler;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class file represents data parsing from JSON file which consist of the
 * functions like JsonVerify, loadJson and convertJsonFileToMap
 * 
 * @author
 */
public class JsonParser {

	Map<String, Object> map = new HashMap<String, Object>();
	ObjectMapper mapper = new ObjectMapper();

	/**
	 * Method to verify key values in the json file
	 * 
	 * @param filePath,
	 *            key and value
	 * @return status
	 */
	public boolean JsonVerify(String filePath, String key, String value) {
		boolean status = false;
		try {
			convertJsonFileToMap(filePath);
			if (map.get(key).toString().equals(value) || map.get(key).toString().equals(null)
					|| map.get(key).toString().contains(value)) {
				status = true;
			}
		} catch (NullPointerException e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return status;
	}

	/**
	 * Method to load json file or json string
	 * 
	 * @param filePathOrJsonString
	 * 
	 * @return map
	 */
	public Map<String, Object> loadJson(String filePathOrJsonString) {
		try {
			if (new File(filePathOrJsonString).exists()) {
				convertJsonFileToMap(filePathOrJsonString);
			} else {
				map = mapper.readValue(filePathOrJsonString, new TypeReference<HashMap<String, String>>() {
				});
			}
		} catch (IOException e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return map;
	}

	public Map<String, Object> loadJsonString(String filePathOrJsonString) {
		try {
			if (new File(filePathOrJsonString).exists()) {
				convertJsonFileToMap(filePathOrJsonString);
			} else {
				map = mapper.readValue(filePathOrJsonString, new TypeReference<HashMap<String, String>>() {
				});
			}
		} catch (IOException e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return map;
	}

	/**
	 * Method to convert Json File To Map
	 * 
	 * @param filePath
	 */
	private void convertJsonFileToMap(String filePath) {
		JSONObject jsonObject = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			jsonObject = (JSONObject) parser.parse(new FileReader(filePath));

			map = mapper.readValue(jsonObject.toString(), new TypeReference<Map<String, Object>>() {
			});
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
	}
	
	public <T> T convertJsonFileToObject(String filePathOrJsonString, T type){
        //System.out.println(type.getClass().getName() + " = " + type);
        try {
			if (new File(filePathOrJsonString).exists()) {
				convertJsonFileToMap(filePathOrJsonString);
			} else {
				type = mapper.readValue(filePathOrJsonString,(Class<T>)type.getClass());
			}
		} catch (IOException e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
        return type;
	}
}
package reporters;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.testng.ISuite;
import org.testng.ITestResult;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import exceptionHandler.CustomExceptionHandler;
import loggers.interfaces.ILogger;
import parsers.PropertiesParser;
import reporters.interfaces.IReporter;
import testConfiguration.PropertyFileLoader;
import testConfiguration.Singleton;
import utilities.FileUtility;

/**
 * ExtentReport class which implements IReporter interface
 * 
 * @author Appu and sohail
 */
public class ExtentReport implements IReporter {

	private ExtentReports extent = null;
	private ThreadLocal<ExtentTest> testThread = new ThreadLocal<ExtentTest>();
	private ILogger log;

	public ExtentReport() {
		log = Singleton.getInstance(ILogger.class);
	}

	@Override
	public void initializeReport(ISuite suiteObj) {
		try {
			String reportDir;
			ExtentHtmlReporter htmlReporter = null;
			PropertiesParser propertiesParser = Singleton.getInstance(PropertyFileLoader.class).propertiesParser;
			reportDir = propertiesParser.containsProperty("reportOutputPath")
					? Paths.get(propertiesParser.getProperty("reportOutputPath"), "ExtentReports").toString()
					: Paths.get(System.getProperty("user.dir"), "ExtentReports").toString();

			File dir = new File(reportDir);
			if (!dir.exists()) {
				dir.mkdir();
			}
			log.info("Extent Report Output Directory Created Successful : " + reportDir);
			String reportFileName = new SimpleDateFormat("dd_MMM_yyyy_HH_mm_ss").format(new Date()).concat(".html");
			log.info("Extent Report Output File Created Successful : " + reportDir + "\\" + reportFileName);
			htmlReporter = new ExtentHtmlReporter(Paths.get(reportDir, reportFileName).toString());
			extent = new ExtentReports();
			extent.attachReporter(htmlReporter);
			InetAddress localhost = null;
			try {
				localhost = InetAddress.getLocalHost();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			extent.setSystemInfo("Host", propertiesParser.containsProperty("host")
					? propertiesParser.getProperty("host") : localhost.getHostAddress());
			extent.setSystemInfo("Environment", propertiesParser.containsProperty("Environment")
					? propertiesParser.getProperty("Environment") : System.getProperty("os.name"));
			extent.setSystemInfo("UserName", propertiesParser.containsProperty("UserName")
					? propertiesParser.getProperty("UserName") : System.getProperty("user.name"));
			htmlReporter.config().setDocumentTitle(propertiesParser.containsProperty("DocumentTitle")
					? propertiesParser.getProperty("DocumentTitle") : "CESTA");
			htmlReporter.config().setReportName(propertiesParser.containsProperty("ReportName")
					? propertiesParser.getProperty("ReportName") : "CESTA");
			htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
			htmlReporter.config().setTheme(Theme.STANDARD);
			log.info("Startup Extent Reporter Activites..");
		} catch (Exception ex) {
			CustomExceptionHandler.ExceptionHandler(ex);
		}
	}

	@Override
	public void closeReporter() {
		extent.flush();
	}

	@Override
	public void endTest(TestResultType testResultType, Object... testResultContext) {
		switch (testResultType) {
		case FAIL:
			try {
				if (testResultContext.length > 0) {
					String encodedfile = FileUtility.convertFileToBase64((File) testResultContext[1]);
					MediaEntityModelProvider mediaProvider;
					mediaProvider = MediaEntityBuilder.createScreenCaptureFromBase64String(encodedfile).build();
					getTestThread().log(Status.FAIL,
							MarkupHelper.createLabel((String) testResultContext[0], ExtentColor.RED));
					getTestThread().log(Status.FAIL, "", mediaProvider);
				} else {
					getTestThread().log(Status.FAIL,
							MarkupHelper.createLabel((String) testResultContext[0], ExtentColor.RED));
				}
			} catch (Exception e) {
				getTestThread().log(Status.FAIL,
						MarkupHelper.createLabel((String) testResultContext[0], ExtentColor.RED));
			}
			break;
		case PASS:
			getTestThread().log(Status.PASS,
					MarkupHelper.createLabel((String) testResultContext[0], ExtentColor.GREEN));
			break;
		case SKIP:
			getTestThread().log(Status.SKIP,
					MarkupHelper.createLabel((String) testResultContext[0], ExtentColor.ORANGE));
			break;
		default:
			break;
		}
	}

	@Override
	public void startTest(ITestResult testName) {
		setTestThread(extent.createTest(testName.getName()));
	}

	@Override
	public void logReport(LogType logtype, String logMessage) {
		switch (logtype) {
		case INFO:
			getTestThread().log(Status.INFO, MarkupHelper.createLabel(logMessage, ExtentColor.BLUE));
			break;
		case ERROR:
			getTestThread().log(Status.ERROR, logMessage);
			break;
		case WARN:
			getTestThread().log(Status.WARNING, logMessage);
			break;
		case FATAL:
			getTestThread().log(Status.FATAL, MarkupHelper.createLabel(logMessage, ExtentColor.RED));
			break;

		default:
			break;
		}

	}

	private void setTestThread(ExtentTest test) {
		testThread.set(test);
	}

	private ExtentTest getTestThread() {
		return testThread.get();
	}
}

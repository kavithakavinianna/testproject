package reporters;

import java.io.File;
import java.net.InetAddress;
import org.json.simple.JSONObject;
import org.testng.ISuite;
import org.testng.ITestResult;
import exceptionHandler.CustomExceptionHandler;
import reporters.interfaces.IReporter;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;
import utilities.DateTimeUtil;
import utilities.FileUtility;
import utilities.apiUtils.APIUtils;

/**
 * CESTAReport class which implements IReporter interface
 * 
 * @author sohail
 */
public class CESTAReport implements IReporter {

	private String suiteID;
	private String suiteStartTime;
	private ThreadLocal<String> testID = new ThreadLocal<String>();
	private ThreadLocal<String> testStartTime = new ThreadLocal<String>();

	@SuppressWarnings("unchecked")
	@Override
	public void initializeReport(ISuite suiteObj) {
		try {
			suiteID = suiteObj.getName() + "_" + System.currentTimeMillis();
			String hostAddress = InetAddress.getLocalHost().getHostAddress();
			String os = System.getProperty("os.name");
			suiteStartTime = DateTimeUtil.getCurrentTime();
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("browsername", Singleton.getInstance(TestEnvironment.class).getProperty("BrowserType"));
			jsonObj.put("hostaddress", hostAddress);
			jsonObj.put("os", os);
			jsonObj.put("starttime", suiteStartTime);
			jsonObj.put("status", "in-progress");
			jsonObj.put("suiteid", suiteID);
			jsonObj.put("suitename", suiteObj.getName());
			Singleton.getInstance(APIUtils.class).postDataInAPI(jsonObj.toJSONString(), "/api/createtestsuite");
		} catch (Exception ex) {
			CustomExceptionHandler.ExceptionHandler(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void closeReporter() {
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("endtime", DateTimeUtil.getCurrentTime());
			jsonObj.put("status", "completed");
			jsonObj.put("suiteid", suiteID);
			Singleton.getInstance(APIUtils.class).updateDataInAPI(jsonObj.toJSONString(), "/api/closetestsuite");
		} catch (Exception ex) {
			CustomExceptionHandler.ExceptionHandler(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void startTest(ITestResult result) {
		try {
			testID.set(result.getName() + "_" + result.getStartMillis());
			testStartTime.set(DateTimeUtil.getCurrentTime());
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("starttime", testStartTime.get());
			jsonObj.put("status", "in-progress");
			jsonObj.put("suiteid", suiteID);
			jsonObj.put("testid", testID.get());
			jsonObj.put("testname", result.getName());
			Singleton.getInstance(APIUtils.class).postDataInAPI(jsonObj.toJSONString(), "/api/createtest");
		} catch (Exception ex) {
			CustomExceptionHandler.ExceptionHandler(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void endTest(TestResultType testResultType, Object... testResultContext) {
		String testEndTime = DateTimeUtil.getCurrentTime();
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("endtime", testEndTime);
			jsonObj.put("testid", testID.get());
			switch (testResultType) {
			case PASS:
				jsonObj.put("status", "PASS");
				break;
			case FAIL:
				jsonObj.put("status", "FAILED");
				jsonObj.put("screenshot", FileUtility.convertFileToBase64((File) testResultContext[1]));
				break;
			case SKIP:
				jsonObj.put("status", "SKIPPED");
				break;
			default:
				break;
			}
			Singleton.getInstance(APIUtils.class).updateDataInAPI(jsonObj.toJSONString(), "/api/updatetestresult");
		} catch (Exception ex) {
			CustomExceptionHandler.ExceptionHandler(ex);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void logReport(LogType logtype, String logMessage) {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("date", DateTimeUtil.getCurrentTime());
			jsonObj.put("logtext", logMessage);
			switch (logtype) {
			case INFO:
				jsonObj.put("logtype", "INFO");
				break;
			case ERROR:
				jsonObj.put("logtype", "ERROR");
				break;
			case WARN:
				jsonObj.put("logtype", "WARN");
				break;
			case FATAL:
				jsonObj.put("logtype", "FATAL");
				break;
			default:
				break;
			}
			jsonObj.put("testid", testID.get());
			Singleton.getInstance(APIUtils.class).postDataInAPI(jsonObj.toJSONString(), "/api/testlog");
		} catch (Exception ex) {
			CustomExceptionHandler.ExceptionHandler(ex);
		}
	}
}

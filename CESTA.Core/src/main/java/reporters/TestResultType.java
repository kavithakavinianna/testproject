package reporters;
/**
 * This enum represents Test Result Type like PASS, FAIL and SKIP
 * 
 * @author sohail
 */
public enum TestResultType {

	FAIL(0), PASS(1), SKIP(2);

	private int testResultType;

	int getTestResultType() {
		return testResultType;
	}

	TestResultType(int testResultType) {
		this.testResultType = testResultType;
	}
}

package reporters.factory;

import org.testng.Assert;
import reporters.CESTAReport;
import reporters.ExtentReport;
import reporters.interfaces.IReporter;
import testConfiguration.Singleton;

/**
 * 
 * Report factory class to set the reporter object into Singleton based on user
 * input(reporter type) and get where ever we required in the entire framework
 *
 * @author Appu
 */
public class ReporterFactory {

	private IReporter Reporter = null;

	public IReporter getReporterType(String reporterType) {

		switch (reporterType.toLowerCase()) {
		case "extentreport":
			Singleton.setInstance(IReporter.class, new ExtentReport());
			Reporter = Singleton.getInstance(IReporter.class);
			break;
		case "cestareport":
			Singleton.setInstance(IReporter.class, new CESTAReport());
			Reporter = Singleton.getInstance(IReporter.class);
			break;
		default:
			Assert.fail("Invalid reporter type provided : " + reporterType);
			break;
		}
		return Reporter;
	}
}
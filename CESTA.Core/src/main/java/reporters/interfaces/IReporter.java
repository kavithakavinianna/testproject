package reporters.interfaces;

import org.testng.ISuite;
import org.testng.ITestResult;
import reporters.LogType;
import reporters.TestResultType;

/**
 * IReporter interface consists of common reporter function which can be
 * overridden by any type of logger and implement there own functionalities
 * 
 * @author sohail
 */
public interface IReporter {
	/**
	 * Method to Initialize Reporter
	 * 
	 * @return void
	 */
	void initializeReport(ISuite suiteObj);

	/**
	 * Method to Close Reporter.
	 * 
	 * @return void
	 */
	void closeReporter();

	/**
	 * Method to Start Reporter, need to call for each test case before test
	 * case starts
	 * 
	 * @return void
	 * 
	 * @param testCaseName
	 */
	void startTest(ITestResult result);

	/**
	 * Method to Start Reporter, need to call for each test case after test case
	 * ends
	 * 
	 * @return void
	 * 
	 */
	void endTest(TestResultType testResultType, Object... testResultContext);

	/**
	 * Method to Write Report log
	 * 
	 * @return void
	 * 
	 */
	void logReport(LogType logtype, String logMessage);

}

package reporters;

/**
 * This enum represents LogType like INFO, ERROR, WARN and FATAL
 * 
 * @author sohail
 */
public enum LogType {

	INFO(0), ERROR(1), WARN(2), FATAL(3);

	private int reportLogTypeType;

	int getTestResultType() {
		return reportLogTypeType;
	}

	LogType(int reportLogTypeType) {
		this.reportLogTypeType = reportLogTypeType;
	}

}
package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import exceptionHandler.CustomExceptionHandler;

/**
 * Class file represents File Utilities
 * 
 * @author sohail
 *
 */
public class FileUtility {
	/**
	 * Method that converts File To Base64
	 * 
	 * @param file
	 * @return
	 */
	public static String convertFileToBase64(File file) {
		String encodedfile = "";
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(file);
			byte[] bytes = new byte[(int) file.length()];
			fileInputStreamReader.read(bytes);
			encodedfile = Base64.getEncoder().encodeToString(bytes);
			fileInputStreamReader.close();
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return encodedfile;
	}
	
	/**
	 * Method that converts File To string
	 * 
	 * @param file
	 * @return
	 */
	public static String convertFileToString(String filePath) {
		String fileString = "";
		try {
			return new String(Files.readAllBytes(Paths.get(filePath)));
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return fileString;
	}
}

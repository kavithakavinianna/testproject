package utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import exceptionHandler.CustomExceptionHandler;

/**
 * Class file represents the data time utilities
 * 
 * @author
 *
 */
public class DateTimeUtil {
	/**
	 * Method that Returns LocalDate with the specified number of days
	 * subtracted.
	 * 
	 * @param noOfDays
	 * @return
	 */
	public static LocalDate currentDateMinusDays(int noOfDays) {
		return LocalDate.now().minusDays(noOfDays);
	}

	/**
	 * Method that Returns LocalDate with the specified number of days
	 * 
	 * @param date
	 * @param noOfDays
	 * @return
	 */
	public static LocalDate anyDateMinusDays(LocalDate localDate, int noOfDays) {
		return localDate.minusDays(noOfDays);
	}

	/**
	 * Method that Returns LocalDate with the specified number of days added
	 * 
	 * @param noOfDays
	 * @return
	 */
	public static LocalDate currentDatePlusDays(int noOfDays) {
		LocalDate date = LocalDate.now().plusDays(noOfDays);
		return date;
	}

	/**
	 * Method that Returns LocalDate with the specified number of days added
	 * 
	 * @param date
	 * @param noOfDays
	 * @return
	 */
	public static LocalDate anyDatePlusDays(LocalDate localDate, int noOfDays) {
		return localDate.plusDays(noOfDays);
	}

	/**
	 * Method to get the current time with dat time format as - yyyy-MM-dd
	 * HH:mm:ss
	 * 
	 * @return String currentTime
	 */
	public static String getCurrentTime() {
		DateFormat dateFormat;
		Date date;
		String currentTime = "";
		try {
			dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = new Date();
			currentTime = dateFormat.format(date);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return currentTime;
	}
}

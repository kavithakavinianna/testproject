package utilities.apiUtils;

import java.io.File;
import java.util.Map;
import org.apache.http.entity.ContentType;

public class RequestParameters {
	public String uriString = "";
	public String contentJSONString = "";
	public File contentFile;
	public ContentType type;
	public Map<String, String> headers;
}

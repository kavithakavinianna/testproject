package utilities.apiUtils;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;

import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;

public class APIUtils {

	public void postDataInAPI(String jsonBody, String endPoint) {
		Map<String, String> headers = new LinkedHashMap<String, String>();
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		RequestParameters requestParameters = new RequestParameters();
		requestParameters.uriString = "http://" + Singleton.getInstance(TestEnvironment.class).getProperty("APIServer")
				+ ":" + Singleton.getInstance(TestEnvironment.class).getProperty("APIPort") + endPoint;
		requestParameters.contentJSONString = jsonBody;
		requestParameters.headers = headers;
		requestParameters.type = ContentType.APPLICATION_JSON;
		Response response = performPostRequest(requestParameters);
		System.out.println(response);
	}

	public void updateDataInAPI(String jsonBody, String endPoint) {
		Map<String, String> headers = new LinkedHashMap<String, String>();
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		RequestParameters requestParameters = new RequestParameters();
		requestParameters.uriString = "http://" + Singleton.getInstance(TestEnvironment.class).getProperty("APIServer")
				+ ":" + Singleton.getInstance(TestEnvironment.class).getProperty("APIPort") + endPoint;
		requestParameters.contentJSONString = jsonBody;
		requestParameters.headers = headers;
		requestParameters.type = ContentType.APPLICATION_JSON;
		Response response = performPutResquest(requestParameters);
		System.out.println(response);
	}

	public Response performPostRequest(RequestParameters helperParameters) {
		HttpPost post = null;
		try {
			post = new HttpPost(new URI(helperParameters.uriString));
			if (helperParameters.headers != null) {
				post.setHeaders(getCustomHeaders(helperParameters.headers));
			}
			post.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type));
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		return performRequest(post);
	}

	public Response performPutResquest(RequestParameters helperParameters) {
		HttpUriRequest put = null;
		try {
			put = RequestBuilder.put(new URI(helperParameters.uriString))
					.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type)).build();
			if (null != helperParameters.headers)
				put.setHeaders(getCustomHeaders(helperParameters.headers));
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		return performRequest(put);
	}

	private Response performRequest(HttpUriRequest method) {
		CloseableHttpResponse response = null;
		try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
			response = client.execute(method);
			ResponseHandler<String> body = new BasicResponseHandler();
			return new Response(response.getStatusLine().getStatusCode(), body.handleResponse(response));
		} catch (Exception e) {
			if (e instanceof HttpResponseException)
				return new Response(response.getStatusLine().getStatusCode(), e.getMessage());
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private Header[] getCustomHeaders(Map<String, String> headers) {
		Header[] customHeaders = new Header[headers.size()];
		int i = 0;
		for (String key : headers.keySet()) {
			customHeaders[i++] = new BasicHeader(key, headers.get(key));
		}
		return customHeaders;
	}

	private HttpEntity getHttpEntity(Object content, ContentType type) {
		if (content instanceof String)
			return new StringEntity((String) content, type);
		else if (content instanceof File)
			return new FileEntity((File) content, type);
		else
			throw new RuntimeException("Entity Type not found ");
	}

}

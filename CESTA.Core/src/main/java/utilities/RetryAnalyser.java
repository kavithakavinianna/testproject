package utilities;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

/**
 * Class file represents the Retry Analyzer
 * 
 * @author
 *
 */
public class RetryAnalyser implements IRetryAnalyzer {

	private int count = 0;
	private static int maxTry = 1;

	/**
	 * Method to retry execution based in the max try count
	 */
	@Override
	public boolean retry(ITestResult result) {
		if (!result.isSuccess()) {
			if (count < maxTry) {
				System.out.println("Retying to execute test case: " + result.getName());
				count++;
				result.setStatus(ITestResult.FAILURE);
				return true;
			} else {
				result.setStatus(ITestResult.FAILURE);
			}
		} else {
			result.setStatus(ITestResult.SUCCESS);
		}
		return false;
	}
}
package utilities;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class file represents Number Utilities
 * 
 * @author
 *
 */
public class NumberUtility {

	public static final int MAX_INT_NUMBER = 100000;

	/**
	 * Method to get the random number
	 * 
	 * @return Integer
	 */
	public static Integer getRandomNumber() {

		Random random = new Random();
		return random.nextInt(MAX_INT_NUMBER);
	}

	/**
	 * Method to get the random number by limiting to the given max and min
	 * number param, and return an integer
	 * 
	 * @param minNumber
	 * @param maxNumber
	 * @return Integer
	 */
	public static Integer getRandomNumber(int minNumber, int maxNumber) {

		int randomNum = ThreadLocalRandom.current().nextInt(minNumber, maxNumber + 1);
		return randomNum;
	}

	/**
	 * Method to get the random number by limiting to the given max and min
	 * number param, and return an Long
	 * 
	 * @param minNumber
	 * @param maxNumber
	 * @return Integer
	 */
	public static Long getRandomNumber(long minNumber, long maxNumber) {

		long randomNum = ThreadLocalRandom.current().nextLong(minNumber, maxNumber + 1);
		return randomNum;
	}
	
	public static String getRandomNumber(int length) {
		Long num = (long) Math.floor(Math.pow(10, length-1) + Math.random() * 9 * Math.pow(10, length-1));
		System.out.println(num);
		return Double.toString(num);

	}

}

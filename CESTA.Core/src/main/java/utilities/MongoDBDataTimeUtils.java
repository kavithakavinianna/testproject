package utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MongoDBDataTimeUtils {

	public static String getCurrentTime() {
		DateFormat dateFormat;
		Date date;
		String currentTime = "";
		try {
			dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = new Date();
			currentTime = dateFormat.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return currentTime;
	}

	public static String getTotalExecutionTime(String endTime, String startTime) {
		long diff;
		long diffSeconds = 0;
		long diffMinutes = 0;
		long diffHours = 0;
		long diffDays = 0;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date beginDate = dateFormat.parse(startTime);
			Date endDate = dateFormat.parse(endTime);
			diff = endDate.getTime() - beginDate.getTime();
			diffSeconds = diff / 1000 % 60;
			diffMinutes = diff / (60 * 1000) % 60;
			diffHours = diff / (60 * 60 * 1000) % 24;
			diffDays = diff / (24 * 60 * 60 * 1000);
			System.out.print(diffDays + " days, ");
			System.out.print(diffHours + " hours, ");
			System.out.print(diffMinutes + " minutes, ");
			System.out.print(diffSeconds + " seconds.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return diffDays + " days " + diffHours + " hours " + diffMinutes + " minutes " + diffSeconds + " seconds";
	}
}
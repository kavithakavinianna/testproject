package utilities;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.ThreadLocalRandom;

import exceptionHandler.CustomExceptionHandler;

/**
 * Class files represents String Utilities
 * 
 * @author
 *
 */
public class StringUtility {
	/**
	 * Method to remove HTML And White Space
	 * 
	 * @param string
	 * @return String
	 */
	public static String removeHTMLAndWhiteSpace(String string) {
		return string.replaceAll("\\s+", "").replaceAll("\\<[^>]*>", "");
	}

	/**
	 * Method to remove remove White Space
	 * 
	 * @param string
	 * @return
	 */
	public static String removeWhiteSpace(String string) {
		return string.replaceAll("\\s+", "");
	}

	/**
	 * Method to remove HTML Tags
	 * 
	 * @param string
	 * @return
	 */
	public static String removeHTMLTags(String string) {
		return string.replaceAll("\\<[^>]*>", "");
	}

	/**
	 * Method to get random string
	 * 
	 * @param lengthOfString
	 * @return
	 */
	public static String getRandomString(int lengthOfString) {
		StringBuffer randomString = new StringBuffer();

		for (int i = 0; i < 2; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(65, 90 + 1);

			char c = (char) (randomNum);
			randomString.append(c);
		}
		return randomString.toString();
	}

	/**
	 * Method to read file
	 * 
	 * @param path
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	public static String readFile(String path, Charset encoding) {
		byte[] encoded = null;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
		} catch (IOException e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return new String(encoded, encoding);
	}
}

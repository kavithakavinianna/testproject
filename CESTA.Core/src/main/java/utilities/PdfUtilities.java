package utilities;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.TimeUnit;

import org.testng.Assert;

import exceptionHandler.CustomExceptionHandler;

/**
 * Class file represents the PDF Utilities
 * 
 * @author
 *
 */
public class PdfUtilities {

	public static String getDownloads() {
		String path = System.getProperty("user.home");
		path = path + "\\Downloads\\";
		return path;
	}

	/**
	 * Method to get Downloaded Document Name
	 * 
	 * @param fileExtension
	 * @return String downloadedFileName
	 */
	public static String getDownloadedDocumentName(String fileExtension) {
		String downloadedFileName = null;
		boolean valid = true;
		boolean found = false;
		// default timeout in seconds
		long timeOut = 20;
		try {
			Path downloadFolderPath = Paths.get(getDownloads());
			WatchService watchService = downloadFolderPath.getFileSystem().newWatchService();
			downloadFolderPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
			long startTime = System.currentTimeMillis();
			do {
				WatchKey watchKey;
				watchKey = watchService.poll(timeOut, TimeUnit.SECONDS);
				long currentTime = (System.currentTimeMillis() - startTime) / 1000;
				if (currentTime > timeOut) {
					Assert.fail("Download operation timed out.. Expected file was not downloaded");
				}

				for (@SuppressWarnings("rawtypes")
				WatchEvent event : watchKey.pollEvents()) {
					@SuppressWarnings("rawtypes")
					WatchEvent.Kind kind = event.kind();
					if (kind.equals(StandardWatchEventKinds.ENTRY_CREATE)) {
						String fileName = event.context().toString();
						System.out.println("New File Created:" + fileName);
						if (fileName.endsWith(fileExtension)) {
							downloadedFileName = fileName;
							System.out.println("Downloaded file found with extension " + fileExtension
									+ ". File name is " + fileName);
							found = true;
							break;
						}
					}
				}
				if (found) {
					return downloadedFileName;
				} else {
					currentTime = (System.currentTimeMillis() - startTime) / 1000;
					if (currentTime > timeOut) {
						System.out.println("Failed to download expected file");
						return downloadedFileName;
					}
					valid = watchKey.reset();
				}
			} while (valid);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return downloadedFileName;
	}

}
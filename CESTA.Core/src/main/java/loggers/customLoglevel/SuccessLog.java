package loggers.customLoglevel;

import org.apache.log4j.Level;

@SuppressWarnings("serial")
public class SuccessLog extends Level {

	/**
	 * Value of SuccessLog4jLevel level. This value is lesser than DEBUG_INT and
	 * higher than TRACE_INT}
	 */
	public static final int SUCCESS_INT = INFO_INT;

	/**
	 * Level representing my log level
	 */
	public static final Level SUCCESS = new SuccessLog(SUCCESS_INT, "SUCCESS", 0);

	/**
	 * Constructor
	 */
	protected SuccessLog(int arg0, String arg1, int arg2) {
		super(arg0, arg1, arg2);

	}

	/**
	 * Checks whether logArgument is "SUCCESS" level. If yes then returns
	 * SUCCESS}, else calls SuccessLog4jLevel#toLevel(String, Level) passing it
	 * Level#DEBUG as the defaultLevel.
	 */
	public static Level toLevel(String logArgument) {
		if (logArgument != null && logArgument.toUpperCase().equals("SUCCESS")) {
			return SUCCESS;
		}
		return (Level) toLevel(logArgument, Level.DEBUG);
	}

	/**
	 * Checks whether val is SuccessLog4jLevel#SUCCESS_INT. If yes then returns
	 * SuccessLog4jLevel#SUCCESS, else calls SuccessLog4jLevel#toLevel(int,
	 * Level) passing it Level#DEBUG as the defaultLevel
	 * 
	 */
	public static Level toLevel(int val) {
		if (val == SUCCESS_INT) {
			return SUCCESS;
		}
		return (Level) toLevel(val, Level.DEBUG);
	}

	/**
	 * Checks whether val is SuccessLog4jLevel#SUCCESS_INT. If yes then returns
	 * SuccessLog4jLevel#SUCCESS, else calls Level#toLevel(int,
	 * org.apache.log4j.Level)
	 * 
	 */
	public static Level toLevel(int val, Level defaultLevel) {
		if (val == SUCCESS_INT) {
			return SUCCESS;
		}
		return Level.toLevel(val, defaultLevel);
	}

	/**
	 * Checks whether logArgument is "SUCCESS" level. If yes then returns
	 * SuccessLog4jLevel#SUCCESS, else calls Level#toLevel(java.lang.String,
	 * org.apache.log4j.Level)
	 * 
	 */
	public static Level toLevel(String logArgument, Level defaultLevel) {
		if (logArgument != null && logArgument.toUpperCase().equals("PASS")) {
			return SUCCESS;
		}
		return Level.toLevel(logArgument, defaultLevel);
	}
}
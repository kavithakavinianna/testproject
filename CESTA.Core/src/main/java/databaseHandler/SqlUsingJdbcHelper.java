package databaseHandler;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import exceptionHandler.CustomExceptionHandler;
import loggers.interfaces.ILogger;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;

public class SqlUsingJdbcHelper {
	
	private static TestEnvironment testEnvironment = null;
	private static ILogger log = null;
	
	static {
		testEnvironment = Singleton.getInstance(TestEnvironment.class);
		log = Singleton.getInstance(ILogger.class);
	}
	
	public static Connection getConnection() {
		
		Connection conn = null;
		try {			

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

			String hostName = testEnvironment.getProperty("hostName");
			String dbName = testEnvironment.getProperty("dbName");
			String user = testEnvironment.getProperty("dbUser");
			String password = testEnvironment.getProperty("dbPassword");

			String url = String.format("jdbc:sqlserver://%s;databaseName=%s;user=%s;password=%s;", hostName, dbName, user, password ); 

			conn = DriverManager.getConnection(url);

			log.info("Connected to database");

		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return conn;
	}	
	
	public static Statement getStatement(Connection con) {
		Statement stmt = null;
		try {
			stmt = con.createStatement();
			log.info("created the statement");

		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return stmt;
	}
	
	public static ResultSet executeDbQuery(Statement stmt, String query) {
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery(query);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e);
		}
		return rs;
	}
	
	public static void closeStatement(Statement stmt) {
		try {
			if (stmt != null)
				stmt.close();
		} catch (Exception e) {
			// Ignored
		}
		log.info("Closed the Statement");
	}

	public static void closeConnection(Connection con) {
		try {
			if (con != null)
				con.close();
		} catch (Exception e) {
			// Ignored
		}		
		log.info("Closed the Connection");
	}
}
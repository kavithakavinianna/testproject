package configuration.applications.utils;

public enum AutomationType {
	WebAutomation(0), MobileAutomation(1), ApiAutomation(2);

	private final int automationType;

	AutomationType(int automationType) {
		this.automationType = automationType;
	}

	public int getAutomationType() {
		return this.automationType;
	}
}

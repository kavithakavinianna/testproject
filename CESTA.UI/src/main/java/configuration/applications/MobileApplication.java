package configuration.applications;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import configuration.base.PageBase;
import configuration.driverManager.DriverManager;
import configuration.driverManager.DriverSettings;
import configuration.exceptions.DriverTypeMissingException;
import configuration.pagefactory.MobilePageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;

public class MobileApplication extends Application {

	private MobilePageFactory mobilePageFactory;
	private TestEnvironment testEnvironment;
	private boolean NewSessionPerTest = true;

	public MobileApplication() {
		mobilePageFactory = new MobilePageFactory();
		testEnvironment = Singleton.getInstance(TestEnvironment.class);
	}

	@Override
	public void launch(DriverSettings settings) {

		if (!NewSessionPerTest)
			return;
		AppiumDriver<MobileElement> appiumDriver;
		String driverType = getDriverType(settings);

		switch (driverType.toLowerCase()) {
		case "android":
			appiumDriver = getAndroidDriver(settings);
			break;
		case "ios":
			appiumDriver = getIOSDriver(settings);
			break;
		default:
			throw new IllegalArgumentException("Invalid browser type provided: " + driverType);
		}

		DriverManager.setMobileDriver(appiumDriver);
		setTimeouts();		
		Singleton.getInstance(MobileApplication.class).NewSessionPerTest = testEnvironment.MobileNewSessionPerTest;
	}

	private String getDriverType(DriverSettings settings) {
		String driverType = null;

		if (System.getProperty("mobileDriverType") != null
				&& !System.getProperty("mobileDriverType").equalsIgnoreCase("default")) {
			driverType = System.getProperty("mobileDriverType");
		} else if (settings.mobileDriverType != null) {
			driverType = settings.mobileDriverType;
		} else {
			throw new DriverTypeMissingException(
					"'mobileDriverType' is not provided through commant line argument, neither through "
							+ DriverSettings.class);
		}
		return driverType;
	}

	private AndroidDriver<MobileElement> getAndroidDriver(DriverSettings settings) {
		AndroidDriver<MobileElement> androidDriver = null;
		androidDriver = new AndroidDriver<MobileElement>(settings.androidRemoteAddress, settings.androidCapabilities);
		return androidDriver;
	}

	private IOSDriver<MobileElement> getIOSDriver(DriverSettings settings) {
		IOSDriver<MobileElement> iosDriver = null;
		iosDriver = new IOSDriver<MobileElement>(settings.iosRemoteAddress, settings.iosCapabilities);
		return iosDriver;
	}

	@Override
	public void dispose(boolean dispose) {
		if (dispose) {
			AppiumDriver<MobileElement> driver = DriverManager.getMobileDriver();
			if (driver != null && !driver.toString().contains("null"))
				DriverManager.getMobileDriver().quit();
		}
	}

	@Override
	public <P extends PageBase> P newPage(Class<P> pageType) {
		P newpage = null;
		newpage = mobilePageFactory.createPageObjectInstance(pageType);
		return newpage;
	}

	@Override
	public void setTimeouts() {
		TestEnvironment testEnvironment = Singleton.getInstance(TestEnvironment.class);
		DriverManager.getMobileDriver().manage().timeouts().implicitlyWait(testEnvironment.ImplicitWaitTimeout,
				TimeUnit.SECONDS);
	}

	@Override
	public File takeScreenshot() {
		TakesScreenshot scrShot = ((TakesScreenshot) DriverManager.getMobileDriver());
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		return SrcFile;
	}
}

package configuration.applications;

import java.io.File;

import configuration.base.PageBase;
import configuration.driverManager.DriverSettings;

public abstract class Application {
	
	public abstract void launch(DriverSettings settings) ;

	public abstract <P extends PageBase> P newPage(Class<P> pageType);
	
	public abstract void dispose(boolean dispose);
	
	public abstract void setTimeouts();
	
	public abstract File takeScreenshot();

}

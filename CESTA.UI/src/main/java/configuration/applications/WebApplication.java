package configuration.applications;

import java.io.File;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import configuration.base.PageBase;
import configuration.driverManager.DriverManager;
import configuration.driverManager.DriverSettings;
import configuration.exceptions.DriverTypeMissingException;
import configuration.pagefactory.WebPageFactory;
import loggers.interfaces.ILogger;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;

public class WebApplication extends Application {

	private WebPageFactory webPageFactory;
	private ILogger log;
	private TestEnvironment testEnvironment;
	private boolean NewSessionPerTest = true;

	public WebApplication() {
		webPageFactory = new WebPageFactory();
		log = Singleton.getInstance(ILogger.class);
		testEnvironment = Singleton.getInstance(TestEnvironment.class);
	}

	@Override
	public void launch(DriverSettings settings) {
		if (!NewSessionPerTest)
			return;

		checkOSAndPrintOSName();

		WebDriver driver = null;
		String browserType = getDriverType(settings);
		log.info("Opening " + browserType + " browser......");

		switch (browserType.toLowerCase()) {
		case "chrome":
			driver = getChromeDriver(settings);
			break;
		case "firefox":
			driver = getFirefoxDriver(settings);
			break;
		case "ie":
			driver = getInternetExplorerDriver(settings);
			break;
		case "safari":
			driver = getSafariDriver(settings);
			break;
		default:
			throw new IllegalArgumentException("Invalid browser type provided: " + browserType);
		}
		DriverManager.setWebDriver(driver);
		DriverManager.getWebDriver().manage().window().maximize();
		Singleton.getInstance(WebApplication.class).NewSessionPerTest = testEnvironment.WebNewSessionPerTest;
	}

	private String getDriverType(DriverSettings settings) {
		String driverType = null;

		if (System.getProperty("webDriverType") != null
				&& !System.getProperty("webDriverType").equalsIgnoreCase("default")) {
			driverType = System.getProperty("webDriverType");
		} else if (settings.webDriverType != null) {
			driverType = settings.webDriverType;
		} else {
			throw new DriverTypeMissingException(
					"'webDriverType' is not provided through commant line argument, neither through "
							+ DriverSettings.class);
		}
		return driverType;
	}

	@SuppressWarnings("deprecation")
	private ChromeDriver getChromeDriver(DriverSettings settings) {
		ChromeDriver chromeDriver = null;
		Capabilities chromeCapabilities = settings.chromeCapabilities;
		
		ChromeDriverService chromeDriverService = settings.chromeDriverService;

		if (isUnix()) {
			if (settings.chromeOptions == null) {
				settings.chromeOptions = new ChromeOptions();

				settings.chromeOptions.addArguments("--no-sandbox");
				settings.chromeOptions.addArguments("--headless");
				settings.chromeOptions.addArguments("start-maximized");
				settings.chromeOptions.addArguments("--disable-gpu");
				settings.chromeOptions.addArguments("disable-infobars");
				settings.chromeOptions.addArguments("--disable-extensions");

				settings.chromeOptions.addArguments("--disable-dev-shm-usage");
				settings.chromeOptions.setBinary(new File("/usr/bin/google-chrome"));
			}
		}
		
		ChromeOptions chromeOptions = settings.chromeOptions;

		if (chromeDriverService != null && chromeCapabilities != null)
			chromeDriver = new ChromeDriver(chromeDriverService, chromeCapabilities);
		else if (chromeDriverService != null && chromeOptions != null)
			chromeDriver = new ChromeDriver(chromeDriverService, chromeOptions);
		else if (chromeCapabilities != null)
			chromeDriver = new ChromeDriver(chromeCapabilities);
		else if (chromeOptions != null)
			chromeDriver = new ChromeDriver(chromeOptions);
		else {
			if (isWindows()) {
				String driverPath = Paths.get(getDriverPath(), "chromedriver.exe").normalize().toString();
				System.setProperty("webdriver.chrome.driver", driverPath);
			}
			chromeDriver = new ChromeDriver();
		}

		return chromeDriver;
	}

	private FirefoxDriver getFirefoxDriver(DriverSettings settings) {
		FirefoxDriver firefoxDriver = null;
		FirefoxOptions firefoxOptions = settings.firefoxOptions;
		if (firefoxOptions != null)
			firefoxDriver = new FirefoxDriver(firefoxOptions);
		else {
			if (isWindows()) {
				String driverPath = Paths.get(getDriverPath(), "geckodriver.exe").toString();
				System.setProperty("webdriver.gecko.driver", driverPath);
			}
			firefoxDriver = new FirefoxDriver();
		}
		return firefoxDriver;
	}

	@SuppressWarnings("deprecation")
	private InternetExplorerDriver getInternetExplorerDriver(DriverSettings settings) {
		InternetExplorerDriver internetExplorerDriver = null;
		Capabilities ieCapabilities = settings.ieCapabilities;
		InternetExplorerDriverService internetExplorerDriverService = settings.internetExplorerDriverService;

		if (ieCapabilities != null)
			internetExplorerDriver = new InternetExplorerDriver(ieCapabilities);
		else if (internetExplorerDriverService != null)
			internetExplorerDriver = new InternetExplorerDriver(internetExplorerDriverService);
		else {
			if (isWindows()) {
				String driverPath = Paths.get(getDriverPath(), "IEDriverServer.exe").toString();
				System.setProperty("webdriver.ie.driver", driverPath);
			}
			internetExplorerDriver = new InternetExplorerDriver();
		}
		return internetExplorerDriver;
	}

	@SuppressWarnings("deprecation")
	private SafariDriver getSafariDriver(DriverSettings settings) {
		SafariDriver safariDriver = null;
		Capabilities safariCapabilities = settings.safariCapabilities;
		SafariOptions safariOptions = settings.safariOptions;

		if (safariCapabilities != null)
			safariDriver = new SafariDriver(safariCapabilities);
		else if (safariOptions != null)
			safariDriver = new SafariDriver(safariOptions);
		else {
			if (isWindows()) {
				String driverPath = Paths.get(getDriverPath(), "SafariDriver.exe").toString();
				System.setProperty("webdriver.safari.driver", driverPath);
			}
			safariDriver = new SafariDriver();
		}
		return safariDriver;
	}

	private String getDriverPath() {
		String driverPath = null;
		String workingDirectory = System.getProperty("user.dir");
		if (testEnvironment.containsProperty("driverpath")) {
			File tmpFile = new File(testEnvironment.getProperty("driverpath"));
			if (!tmpFile.exists()) {
				driverPath = Paths.get(workingDirectory, testEnvironment.getProperty("driverpath")).normalize()
						.toString();
			}
		} else {
			driverPath = Paths.get(workingDirectory).normalize().toString();
		}
		System.out.println("DriverPath :" + driverPath);
		return driverPath;
	}

	@Override
	public void dispose(boolean dispose) {
		if (dispose) {
			WebDriver driver = DriverManager.getWebDriver();
			if (driver != null && !driver.toString().contains("null"))
				DriverManager.getWebDriver().quit();
		}
	}

	@Override
	public <P extends PageBase> P newPage(Class<P> pageType) {
		P newpage = null;
		newpage = webPageFactory.createPageObjectInstance(pageType);
		return newpage;
	}

	@Override
	public void setTimeouts() {
		TestEnvironment testEnvironment = Singleton.getInstance(TestEnvironment.class);
		DriverManager.getWebDriver().manage().timeouts().implicitlyWait(testEnvironment.ImplicitWaitTimeout,
				TimeUnit.SECONDS);
		DriverManager.getWebDriver().manage().timeouts().pageLoadTimeout(testEnvironment.PageLoadTimeout,
				TimeUnit.SECONDS);
		DriverManager.getWebDriver().manage().timeouts().setScriptTimeout(testEnvironment.ScriptTimeout,
				TimeUnit.SECONDS);
	}

	@Override
	public File takeScreenshot() {
		TakesScreenshot scrShot = ((TakesScreenshot) DriverManager.getWebDriver());
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		return SrcFile;
	}

	private void checkOSAndPrintOSName() {
		if (isWindows()) {
			System.out.println("This is Windows");
		} else if (isMac()) {
			System.out.println("This is Mac");
		} else if (isUnix()) {
			System.out.println("This is Unix or Linux");
		} else if (isSolaris()) {
			System.out.println("This is Solaris");
		} else {
			System.out.println("Your OS is not windows/mac/linux/unix/solaris!!");
		}
	}

	public boolean isWindows() {
		return (testEnvironment.getOsName().indexOf("win") >= 0);
	}

	public boolean isMac() {
		return (testEnvironment.getOsName().indexOf("mac") >= 0);
	}

	public boolean isUnix() {
		return (testEnvironment.getOsName().indexOf("nix") >= 0 || testEnvironment.getOsName().indexOf("nux") >= 0
				|| testEnvironment.getOsName().indexOf("aix") > 0);
	}

	public boolean isSolaris() {
		return (testEnvironment.getOsName().indexOf("sunos") >= 0);
	}
}

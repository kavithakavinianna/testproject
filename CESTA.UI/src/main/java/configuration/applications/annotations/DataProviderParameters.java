package configuration.applications.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * This annotation can be used to supply the external test data file name to be used in the {@link DataProvider}
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface DataProviderParameters {
	String path();
}

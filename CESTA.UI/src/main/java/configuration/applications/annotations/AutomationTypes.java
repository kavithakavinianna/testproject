package configuration.applications.annotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import configuration.applications.utils.AutomationType;

/**
 * This annotation can be used to specify the type of the test {@code TestType(}
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface AutomationTypes {
	AutomationType[] types();
}

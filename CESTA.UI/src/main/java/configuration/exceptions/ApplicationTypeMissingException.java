package configuration.exceptions;

public class ApplicationTypeMissingException extends RuntimeException {

	private static final long serialVersionUID = -2197534426355538863L;

	public ApplicationTypeMissingException(String meggage) {
		super(meggage);
	}

}

package configuration.exceptions;

public class DriverTypeMissingException extends RuntimeException {

	private static final long serialVersionUID = -7810423163636624195L;

	public DriverTypeMissingException(String message) {
		super(message);
	}
}

package configuration.driverManager;

import org.openqa.selenium.WebDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.windows.WindowsDriver;
import io.appium.java_client.windows.WindowsElement;

public class DriverManager {

	private static ThreadLocal<WebDriver> webDriverThread = new ThreadLocal<WebDriver>();

	private static ThreadLocal<AppiumDriver<MobileElement>> appiumDriverThread = new ThreadLocal<AppiumDriver<MobileElement>>();

	private static ThreadLocal<WindowsDriver<WindowsElement>> desktopDriverThread = new ThreadLocal<WindowsDriver<WindowsElement>>();
	
	public static WebDriver getWebDriver() {
		return webDriverThread.get();
	}

	public static void setWebDriver(WebDriver webdriver) {
		webDriverThread.set(webdriver);
	}
	
	public static AppiumDriver<MobileElement> getMobileDriver() {
		return appiumDriverThread.get();
	}

	public static void setMobileDriver(AppiumDriver<MobileElement> mobiledriver) {
		appiumDriverThread.set(mobiledriver);
	}
	
	public static WindowsDriver<WindowsElement> getDesktopDriver() {
		return desktopDriverThread.get();
	}

	public static void setDesktopDriver(WindowsDriver<WindowsElement> desktopdriver) {
		desktopDriverThread.set(desktopdriver);
	}
		
	
		
	

}

package configuration.driverManager;

import java.net.URL;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.safari.SafariOptions;

public class DriverSettings {
	
	public String webDriverType = null;
	public String mobileDriverType = null;

	public Capabilities chromeCapabilities = null;
	public ChromeOptions chromeOptions = null;
	public ChromeDriverService chromeDriverService = null;

	public Capabilities firefoxCapabilities = null;
	public FirefoxOptions firefoxOptions = null;
	public FirefoxProfile firefoxProfile = null;

	public Capabilities ieCapabilities = null;
	public InternetExplorerDriverService internetExplorerDriverService = null;

	public Capabilities safariCapabilities = null;
	public SafariOptions safariOptions = null;

	public Capabilities androidCapabilities = null;
	public URL androidRemoteAddress = null;

	public Capabilities iosCapabilities = null;
	public URL iosRemoteAddress = null;
}

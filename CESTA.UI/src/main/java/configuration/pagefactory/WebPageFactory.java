package configuration.pagefactory;

import configuration.base.PageBase;
import loggers.interfaces.ILogger;
import reporters.interfaces.IReporter;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;
import testDataHandler.TestData;
import uiWrappers.webWrappers.interfaces.IWebActions;
import uiWrappers.webWrappers.interfaces.IWebVerifies;
import uiWrappers.webWrappers.interfaces.IWebWaits;
import uiWrappers.webWrappers.wrappers.WebActions;
import uiWrappers.webWrappers.wrappers.WebVerify;
import uiWrappers.webWrappers.wrappers.WebWaits;

public class WebPageFactory extends PageFactory {

	public ILogger log;
	public IReporter report;
	public TestData testData;
	public TestEnvironment testEnvironment;

	public IWebActions webActions;
	public IWebWaits webWaits;
	public IWebVerifies webVerifies;

	public WebPageFactory() {

		log = Singleton.getInstance(ILogger.class);
		report = Singleton.getInstance(IReporter.class);
		testData = Singleton.getInstance(TestData.class);
		testEnvironment = Singleton.getInstance(TestEnvironment.class);
		webActions = Singleton.getInstance(WebActions.class);
		webWaits = Singleton.getInstance(WebWaits.class);
		webVerifies = Singleton.getInstance(WebVerify.class);
	}

	@Override
	public <P extends PageBase> P createPageObjectInstance(Class<P> pageType) {

		P newpage = null;
		try {
			newpage = pageType.newInstance();
			newpage.log = this.log;
			newpage.report = this.report;
			newpage.testData = this.testData;
			newpage.testEnvironment = this.testEnvironment;
			newpage.webActions = this.webActions;
			newpage.webWaits = this.webWaits;
			newpage.webVerify = this.webVerifies;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newpage;
	}
}

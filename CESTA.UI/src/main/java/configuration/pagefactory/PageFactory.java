package configuration.pagefactory;

import configuration.base.PageBase;

public abstract class PageFactory {

	public abstract <P extends PageBase> P createPageObjectInstance(Class<P> page);
	
}

package configuration.pagefactory;

import configuration.base.PageBase;
import loggers.interfaces.ILogger;
import reporters.interfaces.IReporter;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;
import testDataHandler.TestData;
import uiWrappers.mobileWrappers.interfaces.IMobileActions;
import uiWrappers.mobileWrappers.interfaces.IMobileVerifies;
import uiWrappers.mobileWrappers.interfaces.IMobileWaits;
import uiWrappers.mobileWrappers.wrappers.MobileActions;
import uiWrappers.mobileWrappers.wrappers.MobileVerify;
import uiWrappers.mobileWrappers.wrappers.MobileWaits;

public class MobilePageFactory extends PageFactory {

	public ILogger log;
	public IReporter report;
	public TestData testData;
	public TestEnvironment testEnvironment;

	public IMobileActions mobileActions;
	public IMobileWaits mobileWaits;
	public IMobileVerifies mobileVerifies;

	public MobilePageFactory() {

		log = Singleton.getInstance(ILogger.class);
		report = Singleton.getInstance(IReporter.class);
		testData = Singleton.getInstance(TestData.class);
		testEnvironment = Singleton.getInstance(TestEnvironment.class);
		mobileActions = Singleton.getInstance(MobileActions.class);
		mobileWaits = Singleton.getInstance(MobileWaits.class);
		mobileVerifies = Singleton.getInstance(MobileVerify.class);
	}

	@Override
	public <P extends PageBase> P createPageObjectInstance(Class<P> pageType) {

		P newpage = null;
		try {
			newpage = pageType.newInstance();
			newpage.log = this.log;
			newpage.report = this.report;
			newpage.testData = this.testData;
			newpage.testEnvironment = this.testEnvironment;
			newpage.mobileActions = this.mobileActions;
			newpage.mobileWaits = this.mobileWaits;
			newpage.mobileVerify = this.mobileVerifies;
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newpage;
	}
}

package configuration.base;

import configuration.applications.WebApplication;
import loggers.interfaces.ILogger;
import reporters.interfaces.IReporter;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;
import testDataHandler.TestData;
import uiWrappers.mobileWrappers.interfaces.IMobileActions;
import uiWrappers.mobileWrappers.interfaces.IMobileVerifies;
import uiWrappers.mobileWrappers.interfaces.IMobileWaits;
import uiWrappers.mobileWrappers.wrappers.MobileActions;
import uiWrappers.mobileWrappers.wrappers.MobileVerify;
import uiWrappers.mobileWrappers.wrappers.MobileWaits;
import uiWrappers.webWrappers.interfaces.IWebActions;
import uiWrappers.webWrappers.interfaces.IWebVerifies;
import uiWrappers.webWrappers.interfaces.IWebWaits;
import uiWrappers.webWrappers.wrappers.WebActions;
import uiWrappers.webWrappers.wrappers.WebVerify;
import uiWrappers.webWrappers.wrappers.WebWaits;

public class PageBase {

	public ILogger log;
	public IReporter report;
	public TestData testData;
	public TestEnvironment testEnvironment;
	public WebApplication webApplication;

	public IWebActions webActions;
	public IWebWaits webWaits;
	public IWebVerifies webVerify;

	public IMobileActions mobileActions;
	public IMobileWaits mobileWaits;
	public IMobileVerifies mobileVerify;

	public PageBase() {

		log = Singleton.getInstance(ILogger.class);
		report = Singleton.getInstance(IReporter.class);
		testData = Singleton.getInstance(TestData.class);
		testEnvironment = Singleton.getInstance(TestEnvironment.class);
		webApplication = Singleton.getInstance(WebApplication.class);

		webActions = Singleton.getInstance(WebActions.class);
		webWaits = Singleton.getInstance(WebWaits.class);
		webVerify = Singleton.getInstance(WebVerify.class);

		mobileActions = Singleton.getInstance(MobileActions.class);
		mobileWaits = Singleton.getInstance(MobileWaits.class);
		mobileVerify = Singleton.getInstance(MobileVerify.class);
	}
}

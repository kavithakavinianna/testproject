package configuration.base;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;

import apiWrappers.interfaces.IAuthentication;
import apiWrappers.interfaces.IHttpClientAsyncHelpers;
import apiWrappers.interfaces.IHttpClientHelpers;
import apiWrappers.interfaces.IHttpsClientAsyncHelpers;
import apiWrappers.interfaces.IHttpsClientHelpers;
import apiWrappers.wrappers.Authentication;
import apiWrappers.wrappers.HttpClientAsyncHelpers;
import apiWrappers.wrappers.HttpClientHelpers;
import apiWrappers.wrappers.HttpsClientAsyncHelpers;
import apiWrappers.wrappers.HttpsClientHelpers;
import configuration.applications.MobileApplication;
import configuration.applications.WebApplication;
import configuration.applications.annotations.AutomationTypes;
import configuration.applications.annotations.DataProviderParameters;
import configuration.applications.utils.AutomationType;
import configuration.exceptions.ApplicationTypeMissingException;
import configuration.listeners.TestNGListeners;
import loggers.interfaces.ILogger;
import reporters.LogType;
import reporters.TestResultType;
import reporters.interfaces.IReporter;
import testConfiguration.GlobalData;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;
import testDataHandler.TestData;

@Listeners(TestNGListeners.class)
public class TestBase {

	protected ILogger log;
	protected IReporter report;
	protected TestData testData;
	protected TestEnvironment testEnvironment;
	protected WebApplication webApplication;
	protected MobileApplication mobileApplication;
	protected IHttpClientHelpers httpClientHelpers;
	protected IHttpClientAsyncHelpers httpClientAsyncHelpers;
	protected IHttpsClientHelpers httpsClientHelpers;
	protected IHttpsClientAsyncHelpers httpsClientAsyncHelpers;
	protected IAuthentication authentication;

	@BeforeSuite(alwaysRun = true)
	public void globalSetup() {
		
	}

	@BeforeMethod(alwaysRun = true)
	public void globalTestSetup(ITestResult result) {
		log = Singleton.getInstance(ILogger.class);
		report = Singleton.getInstance(IReporter.class);
		testData = Singleton.getInstance(TestData.class);
		testEnvironment = Singleton.getInstance(TestEnvironment.class);
		webApplication = Singleton.getInstance(WebApplication.class);
		mobileApplication = Singleton.getInstance(MobileApplication.class);
		authentication = Singleton.getInstance(Authentication.class);
		httpClientHelpers = Singleton.getInstance(HttpClientHelpers.class);
		httpClientAsyncHelpers = Singleton.getInstance(HttpClientAsyncHelpers.class);
		httpsClientHelpers = Singleton.getInstance(HttpsClientHelpers.class);
		httpsClientAsyncHelpers = Singleton.getInstance(HttpsClientAsyncHelpers.class);
	}

	@AfterMethod(alwaysRun = true)
	public void globalTestTeardown(ITestResult result, Method method) {
		String testName = result.getName();
		File screenshotFile = null;
		AutomationType[] applicationTypes;
		switch (result.getStatus()) {
		case ITestResult.SUCCESS:
			report.endTest(TestResultType.PASS, "Test passed: " + testName);
			break;
		case ITestResult.FAILURE:
			AutomationTypes types = method.getAnnotation(AutomationTypes.class);
			if (types != null) {
				applicationTypes = types.types();
			} else {
				throw new ApplicationTypeMissingException(
						"ApplicationTypes annotation is missing in test method, please specify an application type(s) for this test method.");
			}
			StringWriter sw = new StringWriter();
			result.getThrowable().printStackTrace(new PrintWriter(sw));
			String stacktrace = sw.toString();
			report.logReport(LogType.ERROR, stacktrace);
			for (AutomationType applicationType : applicationTypes) {
				switch (applicationType) {
				case WebAutomation:
					screenshotFile = Singleton.getInstance(WebApplication.class).takeScreenshot();
					report.endTest(TestResultType.FAIL, "Test failed: " + testName, screenshotFile);
					break;
				case MobileAutomation:
					screenshotFile = Singleton.getInstance(MobileApplication.class).takeScreenshot();
					report.endTest(TestResultType.FAIL, "Test failed: " + testName, screenshotFile);
					break;
				case ApiAutomation:
					report.endTest(TestResultType.FAIL, "Test failed: " + testName);
					break;
				default:
					throw new IllegalArgumentException(
							"Invalid ApplicationType provided: " + applicationType.toString());
				}
			}
			//report.endTest(TestResultType.FAIL, "Test failed: " + testName, screenshotFile);
			break;
		case ITestResult.SKIP:
			report.endTest(TestResultType.SKIP, "Test skipped: " + testName);
			break;
		default:
			throw new RuntimeException("Invalid Test Execution Status");
		}
		Singleton.getInstance(WebApplication.class)
				.dispose(Singleton.getInstance(TestEnvironment.class).WebNewSessionPerTest);
		Singleton.getInstance(MobileApplication.class)
				.dispose(Singleton.getInstance(TestEnvironment.class).MobileNewSessionPerTest);
		GlobalData.reset();
	}

	@AfterSuite(alwaysRun = true)
	public void globalTeardown() {
		// dispose all the application instance
		report.closeReporter();
		Singleton.getInstance(WebApplication.class).dispose(true);
		Singleton.getInstance(MobileApplication.class).dispose(true);
	}

	@DataProvider(name = "TestDataProvider")
	public TestData[] dataProvider(Method method) {
		String dataFilePath = "";
		DataProviderParameters parameters = method.getAnnotation(DataProviderParameters.class);

		if (parameters != null) {
			dataFilePath = parameters.path();
		} else {
			Assert.fail("Test data file name is empty.");
		}

		TestData dSet = new TestData();
		dSet.loadDataFile(dataFilePath);

		int dataSetCount = dSet.getDatasetCount();
		TestData[] dataset = new TestData[dataSetCount];

		for (int i = 0; i < dataSetCount; i++) {
			int j = i + 1;
			dataset[i] = dSet.getDataSet(j);
		}

		return dataset;
	}
}
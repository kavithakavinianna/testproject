package apiWrappers.model;

import java.io.File;
import java.net.URI;
import java.util.Map;
import org.apache.http.entity.ContentType;

public class HelperParameters {
	public String uriString = "";
	public URI uri;
	public String contentJSONString = "";
	public File contentFile;
	public ContentType type;
	public Map<String, String> headers;
}

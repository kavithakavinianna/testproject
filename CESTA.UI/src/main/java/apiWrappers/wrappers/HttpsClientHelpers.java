package apiWrappers.wrappers;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import javax.net.ssl.SSLContext;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import apiWrappers.interfaces.IHttpsClientHelpers;
import apiWrappers.model.HelperParameters;
import apiWrappers.model.Response;

public class HttpsClientHelpers implements IHttpsClientHelpers {

	Response response;

	private HttpEntity getHttpEntity(Object content, ContentType type) {
		if (content instanceof String)
			return new StringEntity((String) content, type);
		else if (content instanceof File)
			return new FileEntity((File) content, type);
		else
			throw new RuntimeException("Entity Type not found ");

	}

	private Header[] getCustomHeaders(Map<String, String> headers) {
		Header[] customHeaders = new Header[headers.size()];
		int i = 0;
		for (String key : headers.keySet()) {
			customHeaders[i++] = new BasicHeader(key, headers.get(key));
		}
		return customHeaders;

	}

	public SSLContext getSSLContext() {
		TrustStrategy trust = new TrustStrategy() {
			@Override
			public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				return true;
			}
		};
		try {
			return SSLContextBuilder.create().loadTrustMaterial(trust).build();
		} catch (KeyManagementException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (KeyStoreException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public CloseableHttpClient getHttpClient(SSLContext sslContext) {
		return HttpClientBuilder.create().setSSLContext(sslContext).build();
	}

	private Response performRequest(HttpUriRequest method) {
		CloseableHttpResponse response = null;
		try (CloseableHttpClient client = getHttpClient(getSSLContext())) {
			response = client.execute(method);
			ResponseHandler<String> handler = new BasicResponseHandler();
			return new Response(response.getStatusLine().getStatusCode(), handler.handleResponse(response));
		} catch (Exception e) {
			if (e instanceof HttpResponseException) {
				return new Response(response.getStatusLine().getStatusCode(), e.getMessage());
			} else {
				throw new RuntimeException(e.getMessage(), e);
			}
		}
	}

	public Response performGetRequestWithSSL(String uri, Map<String, String> headers) {
		try {
			return performGetRequestWithSSL(new URI(uri), headers);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public Response performGetRequestWithSSL(URI uri, Map<String, String> headers) {
		logHttpsClientHelpersEvents(
				"Performing https GET Request with given URI : " + uri + ", " + "and Headers : " + headers);
		HttpGet get = new HttpGet(uri);
		if (null != headers)
			get.setHeaders(getCustomHeaders(headers));
		response = performRequest(get);
		logHttpsClientHelpersEvents(
				"Successfully able to perform the https GET Request and the response is : " + response);
		return response;
	}

	public Response performPostWithSSL(HelperParameters helperParameters) {
		HttpUriRequest post = null;
		try {
			if (!helperParameters.uriString.isEmpty()) {
				if (!helperParameters.contentJSONString.isEmpty()) {
					post = RequestBuilder.post(new URI(helperParameters.uriString))
							.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type))
							.build();
				} else if (!helperParameters.contentFile.toString().isEmpty()) {
					if (helperParameters.contentFile.exists()) {
						post = RequestBuilder.post(new URI(helperParameters.uriString))
								.setEntity(getHttpEntity(helperParameters.contentFile, helperParameters.type)).build();
					} else {
						throw new RuntimeException("Entity content File Type not found ");
					}
				} else {
					throw new RuntimeException("Entity content Type not found ");
				}
				logHttpsClientHelpersEvents("Performing https POST Request with given URI : "
						+ helperParameters.uriString + ", " + "and Headers : " + helperParameters.headers);
			} else if (!helperParameters.uri.toString().isEmpty()) {
				if (!helperParameters.contentJSONString.isEmpty()) {
					post = RequestBuilder.post(helperParameters.uri)
							.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type))
							.build();
				} else if (!helperParameters.contentFile.toString().isEmpty()) {
					if (helperParameters.contentFile.exists()) {
						post = RequestBuilder.post(helperParameters.uri)
								.setEntity(getHttpEntity(helperParameters.contentFile, helperParameters.type)).build();
					} else {
						throw new RuntimeException("Entity content File Type not found ");
					}
				} else {
					throw new RuntimeException("Entity content Type not found ");
				}
				logHttpsClientHelpersEvents("Performing https POST Request with given URI : "
						+ helperParameters.uriString + ", " + "and Headers : " + helperParameters.headers);
			} else {
				throw new RuntimeException("Entity helperParameters Type not found ");
			}
			if (null != helperParameters.headers)
				post.setHeaders(getCustomHeaders(helperParameters.headers));
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		response = performRequest(post);
		logHttpsClientHelpersEvents(
				"Successfully able to perform the https POST Request and the responce is : " + response);
		return response;
	}

	public Response performPutRequestWithSSL(HelperParameters helperParameters) {
		HttpUriRequest put = null;
		try {
			if (!helperParameters.uriString.isEmpty()) {
				if (!helperParameters.contentJSONString.isEmpty()) {
					put = RequestBuilder.put(new URI(helperParameters.uriString))
							.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type))
							.build();
				} else if (!helperParameters.contentFile.toString().isEmpty()) {
					if (helperParameters.contentFile.exists()) {
						put = RequestBuilder.put(new URI(helperParameters.uriString))
								.setEntity(getHttpEntity(helperParameters.contentFile, helperParameters.type)).build();
					} else {
						throw new RuntimeException("Entity content File Type not found ");
					}
				} else {
					throw new RuntimeException("Entity content Type not found ");
				}
				logHttpsClientHelpersEvents("Performing https PUT Request with given URI : "
						+ helperParameters.uriString + ", " + "and Headers : " + helperParameters.headers);
			} else if (!helperParameters.uri.toString().isEmpty()) {
				if (!helperParameters.contentJSONString.isEmpty()) {
					put = RequestBuilder.put(helperParameters.uri)
							.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type))
							.build();
				} else if (!helperParameters.contentFile.toString().isEmpty()) {
					if (helperParameters.contentFile.exists()) {
						put = RequestBuilder.put(helperParameters.uri)
								.setEntity(getHttpEntity(helperParameters.contentFile, helperParameters.type)).build();
					} else {
						throw new RuntimeException("Entity content File Type not found ");
					}
				} else {
					throw new RuntimeException("Entity content Type not found ");
				}
				logHttpsClientHelpersEvents("Performing https PUT Request with given URI : " + helperParameters.uri
						+ ", " + "and Headers : " + helperParameters.headers);
			}
			if (null != helperParameters.headers)
				put.setHeaders(getCustomHeaders(helperParameters.headers));
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		response = performRequest(put);
		logHttpsClientHelpersEvents(
				"Successfully able to perform the https POST Request and the response is : " + response);
		return response;
	}

	public Response performDeleteRequestWithSSL(URI uri, Map<String, String> headers) {
		logHttpsClientHelpersEvents(
				"Performing https DELETE Request with given URI : " + uri + ", " + "and Headers : " + headers);
		HttpUriRequest delete = RequestBuilder.delete(uri).build();
		if (null != headers)
			delete.setHeaders(getCustomHeaders(headers));
		response = performRequest(delete);
		logHttpsClientHelpersEvents(
				"Successfully able to perform the https DELETE Request and the response is : " + response);
		return response;
	}

	public Response performDeleteRequestWithSSL(String uri, Map<String, String> headers) {
		try {
			return performDeleteRequestWithSSL(new URI(uri), headers);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private void logHttpsClientHelpersEvents(String msg) {
		// TODO
	}

}

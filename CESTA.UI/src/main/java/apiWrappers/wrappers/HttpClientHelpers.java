package apiWrappers.wrappers;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import apiWrappers.interfaces.IHttpClientHelpers;
import apiWrappers.model.HelperParameters;
import apiWrappers.model.Response;

public class HttpClientHelpers implements IHttpClientHelpers {

	Response response;

	public Response performGetRequest(String url, Map<String, String> headers) {
		try {
			return performGetRequest(new URI(url), headers);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public Response performGetRequest(URI uri, Map<String, String> headers) {
		logHttpClientHelpersEvents(
				"Performing http GET Request with given URI : " + uri + ", " + "and Headers : " + headers);
		HttpGet get;
		try {
			get = new HttpGet(uri);
			if (headers != null) {
				get.setHeaders(getCustomHeaders(headers));
			}
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		response = performRequest(get);
		logHttpClientHelpersEvents(
				"Successfully able to perform the http GET Request and the responce is :" + response);
		return response;
	}

	public Response performPostRequest(HelperParameters helperParameters) {
		HttpPost post = null;
		try {
			if (!helperParameters.uriString.isEmpty()) {
				post = new HttpPost(new URI(helperParameters.uriString));
				logHttpClientHelpersEvents("Performing http POST Request with given URI : " + helperParameters.uriString
						+ ", " + "and Headers : " + helperParameters.headers);
			} else if (!helperParameters.uri.toString().isEmpty()) {
				post = new HttpPost(helperParameters.uri);
				logHttpClientHelpersEvents("Performing http POST Request with given URI : " + helperParameters.uri
						+ ", " + "and Headers : " + helperParameters.headers);
			} else {
				throw new RuntimeException("Entity helperParameters Type not found ");
			}
			if (helperParameters.headers != null)
				post.setHeaders(getCustomHeaders(helperParameters.headers));
			if (!helperParameters.contentJSONString.isEmpty()) {
				post.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type));
			} else if (!helperParameters.contentFile.toString().isEmpty()) {
				if (helperParameters.contentFile.exists()) {
					post.setEntity(getHttpEntity(helperParameters.contentFile, helperParameters.type));
				} else {
					throw new RuntimeException("Entity content File not found");
				}
			} else {
				throw new RuntimeException("Entity content Type not found ");
			}
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		response = performRequest(post);
		logHttpClientHelpersEvents(
				"Successfully able to perform the http POST Request and the output response is : " + response);
		return response;
	}

	public Response performDeleteRequest(String url, Map<String, String> headers) {
		try {
			return performDeleteRequest(new URI(url), headers);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public Response performDeleteRequest(URI uri, Map<String, String> headers) {
		logHttpClientHelpersEvents(
				"Performing http DELETE Request with given URI : " + uri + ", " + "and Headers : " + headers);
		HttpUriRequest delete = RequestBuilder.delete(uri).build();
		if (null != headers)
			delete.setHeaders(getCustomHeaders(headers));
		response = performRequest(delete);
		logHttpClientHelpersEvents(
				"Successfully able to perform the http GET Request and the output response is : " + response);
		return response;
	}

	public Response performPutResquest(HelperParameters helperParameters) {
		HttpUriRequest put = null;
		try {
			if (!helperParameters.uriString.isEmpty()) {
				if (!helperParameters.contentJSONString.isEmpty()) {
					put = RequestBuilder.put(new URI(helperParameters.uriString))
							.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type))
							.build();
				} else if (!helperParameters.contentFile.toString().isEmpty()) {
					if (helperParameters.contentFile.exists()) {
						put = RequestBuilder.put(new URI(helperParameters.uriString))
								.setEntity(getHttpEntity(helperParameters.contentFile, helperParameters.type)).build();
					} else {
						throw new RuntimeException("Entity content File not found");
					}
				} else {
					throw new RuntimeException("Entity content Type not found ");
				}
				logHttpClientHelpersEvents("Performing http PUT Request with given URI : " + helperParameters.uriString
						+ ", " + "and Headers : " + helperParameters.headers);
			} else if (!helperParameters.uri.toString().isEmpty()) {
				if (!helperParameters.contentJSONString.isEmpty()) {
					put = RequestBuilder.put(helperParameters.uri)
							.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type))
							.build();
				} else if (!helperParameters.contentFile.toString().isEmpty()) {
					if (helperParameters.contentFile.exists()) {
						put = RequestBuilder.put(helperParameters.uri)
								.setEntity(getHttpEntity(helperParameters.contentFile, helperParameters.type)).build();
					} else {
						throw new RuntimeException("Entity content File not found");
					}
				} else {
					throw new RuntimeException("Entity content Type not found ");
				}
				logHttpClientHelpersEvents("Performing http PUT Request with given URI : " + helperParameters.uri + ", "
						+ "and Headers : " + helperParameters.headers);
			} else {
				throw new RuntimeException("Entity helperParameters Type not found ");
			}
			if (null != helperParameters.headers)
				put.setHeaders(getCustomHeaders(helperParameters.headers));
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		response = performRequest(put);
		logHttpClientHelpersEvents(
				"Successfully able to perform the http PUT Request and the output response is : " + response);
		return response;
	}

	private Header[] getCustomHeaders(Map<String, String> headers) {
		Header[] customHeaders = new Header[headers.size()];
		int i = 0;
		for (String key : headers.keySet()) {
			customHeaders[i++] = new BasicHeader(key, headers.get(key));
		}
		return customHeaders;
	}

	private Response performRequest(HttpUriRequest method) {
		CloseableHttpResponse response = null;
		try (CloseableHttpClient client = HttpClientBuilder.create().build()) {
			response = client.execute(method);
			ResponseHandler<String> body = new BasicResponseHandler();
			return new Response(response.getStatusLine().getStatusCode(), body.handleResponse(response));
		} catch (Exception e) {
			if (e instanceof HttpResponseException)
				return new Response(response.getStatusLine().getStatusCode(), e.getMessage());
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private HttpEntity getHttpEntity(Object content, ContentType type) {
		if (content instanceof String)
			return new StringEntity((String) content, type);
		else if (content instanceof File)
			return new FileEntity((File) content, type);
		else
			throw new RuntimeException("Entity Type not found ");
	}

	private void logHttpClientHelpersEvents(String msg) {
		// TODO
	}

}

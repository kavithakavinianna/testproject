package apiWrappers.wrappers;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.message.BasicHeader;

import apiWrappers.interfaces.IHttpClientAsyncHelpers;
import apiWrappers.model.HelperParameters;
import apiWrappers.model.RequestStatus;
import apiWrappers.model.Response;

public class HttpClientAsyncHelpers implements IHttpClientAsyncHelpers {

	Response response;

	private Header[] getCustomHeaders(Map<String, String> headers) {
		Header[] customHeaders = new Header[headers.size()];
		int i = 0;
		for (String key : headers.keySet()) {
			customHeaders[i++] = new BasicHeader(key, headers.get(key));
		}
		return customHeaders;
	}

	private HttpEntity getHttpEntity(Object content, ContentType type) {
		if (content instanceof String)
			return new StringEntity((String) content, type);
		else if (content instanceof File)
			return new FileEntity((File) content, type);
		else
			throw new RuntimeException("Entity Type not found ");
	}

	private CloseableHttpAsyncClient getHttpAsyncClient() {
		return HttpAsyncClientBuilder.create().build();
	}

	private Response performRequest(HttpUriRequest method) {
		Future<HttpResponse> response = null;
		try (CloseableHttpAsyncClient client = getHttpAsyncClient()) {
			client.start();
			response = client.execute(method, new RequestStatus());
			ResponseHandler<String> handler = new BasicResponseHandler();
			return new Response(response.get().getStatusLine().getStatusCode(), handler.handleResponse(response.get()));
		} catch (Exception e) {
			if (e instanceof HttpResponseException)
				try {
					return new Response(response.get().getStatusLine().getStatusCode(), e.getMessage());
				} catch (InterruptedException e1) {
					throw new RuntimeException(e.getMessage(), e);
				} catch (ExecutionException e1) {
					throw new RuntimeException(e.getMessage(), e);
				}
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public Response performGetRequestAsync(String uri, Map<String, String> headers) {
		try {
			return performGetRequestAsync(new URI(uri), headers);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	public Response performGetRequestAsync(URI uri, Map<String, String> headers) {
		logHttpClientAsyncHelpersEvents(
				"Performing Async http GET Request with given URI : " + uri + ", " + "and Headers : " + headers);
		HttpGet get = new HttpGet(uri);
		try {
			if (headers != null)
				get.setHeaders(getCustomHeaders(headers));
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		response = performRequest(get);
		logHttpClientAsyncHelpersEvents(
				"Successfully able to perform Async http GET Request and the output response is : " + response);
		return response;
	}

	public Response performPostRequestAsync(HelperParameters helperParameters) {
		HttpPost post = null;
		try {
			if (!helperParameters.uriString.isEmpty()) {
				post = new HttpPost(new URI(helperParameters.uriString));
				logHttpClientAsyncHelpersEvents("Performing Async http POST Request with given URI : "
						+ helperParameters.uriString + ", " + "and Headers : " + helperParameters.headers);
			} else if (!helperParameters.uri.toString().isEmpty()) {
				post = new HttpPost(helperParameters.uri);
				logHttpClientAsyncHelpersEvents("Performing Async http POST Request with given URI : "
						+ helperParameters.uri + ", " + "and Headers : " + helperParameters.headers);
			} else {
				throw new RuntimeException("Entity helperParameters Type not found ");
			}
			if (helperParameters.headers != null)
				post.setHeaders(getCustomHeaders(helperParameters.headers));
			if (!helperParameters.contentJSONString.isEmpty()) {
				post.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type));
			} else if (!helperParameters.contentFile.toString().isEmpty()) {
				if (helperParameters.contentFile.exists()) {
					post.setEntity(getHttpEntity(helperParameters.contentFile, helperParameters.type));
				} else {
					throw new RuntimeException("Entity content File Type not found ");
				}
			} else {
				throw new RuntimeException("Entity content Type not found ");
			}

		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		response = performRequest(post);
		logHttpClientAsyncHelpersEvents(
				"Successfully able to perform the Async http POST Request and the output response is : " + response);
		return response;
	}

	public Response performPutRequestAsync(HelperParameters helperParameters) {
		HttpPut put = null;
		try {
			if (!helperParameters.uriString.isEmpty()) {
				put = new HttpPut(new URI(helperParameters.uriString));
				logHttpClientAsyncHelpersEvents("Performing Async http PUT Request with given URI : "
						+ helperParameters.uriString + ", " + "and Headers : " + helperParameters.headers);
			} else if (!helperParameters.uri.toString().isEmpty()) {
				put = new HttpPut(helperParameters.uri);
				logHttpClientAsyncHelpersEvents("Performing Async http PUT Request with given URI : "
						+ helperParameters.uri + ", " + "and Headers : " + helperParameters.headers);
			} else {
				throw new RuntimeException("Entity helperParameters Type not found ");
			}
			if (helperParameters.headers != null)
				put.setHeaders(getCustomHeaders(helperParameters.headers));
			if (!helperParameters.contentJSONString.isEmpty()) {
				put.setEntity(getHttpEntity(helperParameters.contentJSONString, helperParameters.type));
			} else if (!helperParameters.contentFile.toString().isEmpty()) {
				if (helperParameters.contentFile.exists()) {
					put.setEntity(getHttpEntity(helperParameters.contentFile, helperParameters.type));
				} else {
					throw new RuntimeException("Entity content File Type not found ");
				}
			} else {
				throw new RuntimeException("Entity content Type not found ");
			}
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		response = performRequest(put);
		logHttpClientAsyncHelpersEvents(
				"Successfully able to perform the Async http POST Request and the output response is : " + response);
		return response;
	}

	public Response performDeleteRequestAsync(URI uri, Map<String, String> headers) {
		logHttpClientAsyncHelpersEvents(
				"Performing Async http DELETE Request with given URI : " + uri + ", " + "and Headers : " + headers);
		HttpDelete delete;
		try {
			delete = new HttpDelete(uri);
			if (headers != null)
				delete.setHeaders(getCustomHeaders(headers));
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
		response = performRequest(delete);
		logHttpClientAsyncHelpersEvents(
				"Successfully able to perform Async http DELETE Request and the output response is : " + response);
		return response;
	}

	public Response performDeleteRequestAsync(String uri, Map<String, String> headers) {
		try {
			return performDeleteRequestAsync(new URI(uri), headers);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	private void logHttpClientAsyncHelpersEvents(String msg) {
		// TODO
	}

}

package apiWrappers.wrappers;

import org.apache.commons.codec.binary.Base64;

import apiWrappers.interfaces.IAuthentication;


public class Authentication implements IAuthentication {
	
	public String generateAndGetBasicAuthentication(String userName, String password) {
		return "Basic " + Base64.encodeBase64String((userName + ":" + password).getBytes());
	}
}
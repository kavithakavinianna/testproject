package apiWrappers.interfaces;

public interface IAuthentication {
	
	String generateAndGetBasicAuthentication(String userName,String password);

}

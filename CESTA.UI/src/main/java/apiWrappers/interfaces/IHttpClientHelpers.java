package apiWrappers.interfaces;

import java.net.URI;
import java.util.Map;

import apiWrappers.model.HelperParameters;
import apiWrappers.model.Response;

public interface IHttpClientHelpers {
	/**
	 * Method to perform GET Request and return RestResponse, parameters are
	 * String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	public Response performGetRequest(String url, Map<String, String> headers);

	/**
	 * Method to perform GET Request and return RestResponse, parameters are URI
	 * url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	public Response performGetRequest(URI uri, Map<String, String> headers);

	/**
	 * Method to perform POST Request and return RestResponse, parameters are
	 * String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	public Response performPostRequest(HelperParameters helperParameters);

	/**
	 * Method to perform DELETE Request and return RestResponse, parameters are
	 * String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	public Response performDeleteRequest(String url, Map<String, String> headers);

	/**
	 * Method to perform DELETE Request and return RestResponse, parameters are
	 * URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	public Response performDeleteRequest(URI uri, Map<String, String> headers);

	/**
	 * Method to perform PUT Request and return RestResponse, parameters are URI
	 * url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	public Response performPutResquest(HelperParameters helperParameters);
}

package apiWrappers.interfaces;

import java.net.URI;
import java.util.Map;

import apiWrappers.model.HelperParameters;
import apiWrappers.model.Response;

public interface IHttpsClientHelpers {

	/**
	 * Method to perform GET Request with SSL and return RestResponse,
	 * parameters are String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performGetRequestWithSSL(String uri, Map<String, String> headers);

	/**
	 * Method to perform GET Request with SSL and return RestResponse,
	 * parameters are URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performGetRequestWithSSL(URI uri, Map<String, String> headers);

	/**
	 * Method to perform POST Request with SSL and return RestResponse,
	 * parameters are URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performPostWithSSL(HelperParameters helperParameters);

	/**
	 * Method to perform POST Request with SSL and return RestResponse,
	 * parameters are String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	// Response performPostWithSSL(String uri, Object content, ContentType type,
	// Map<String, String> headers);

	/**
	 * Method to perform PUT Request with SSL and return RestResponse,
	 * parameters are URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performPutRequestWithSSL(HelperParameters helperParameters);

	/**
	 * Method to perform PUT Request with SSL and return RestResponse,
	 * parameters are String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	// Response performPutRequestWithSSL(String uri, Object content, ContentType
	// type, Map<String, String> headers);

	/**
	 * Method to perform DELETE Request with SSL and return RestResponse,
	 * parameters are URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performDeleteRequestWithSSL(URI uri, Map<String, String> headers);

	/**
	 * Method to perform DELETE Request with SSL and return RestResponse,
	 * parameters are String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performDeleteRequestWithSSL(String uri, Map<String, String> headers);

}

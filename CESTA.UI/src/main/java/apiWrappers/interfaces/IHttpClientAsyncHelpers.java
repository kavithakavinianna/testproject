package apiWrappers.interfaces;

import java.net.URI;
import java.util.Map;

import apiWrappers.model.HelperParameters;
import apiWrappers.model.Response;

public interface IHttpClientAsyncHelpers {

	/**
	 * Method to perform Asynchronous GET Request and return RestResponse,
	 * parameters are String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performGetRequestAsync(String uri, Map<String, String> headers);

	/**
	 * Method to perform Asynchronous GET Request and return RestResponse,
	 * parameters are URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performGetRequestAsync(URI uri, Map<String, String> headers);

	/**
	 * Method to perform Asynchronous POST Request and return RestResponse,
	 * parameters are String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performPostRequestAsync(HelperParameters postAndPutParameters);

	/**
	 * Method to perform Asynchronous PUT Request and return RestResponse,
	 * parameters are String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performPutRequestAsync(HelperParameters postAndPutParameters);

	/**
	 * Method to perform Asynchronous DELETE Request and return RestResponse,
	 * parameters are URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performDeleteRequestAsync(URI uri, Map<String, String> headers);

	/**
	 * Method to perform Asynchronous DELETE Request and return RestResponse,
	 * parameters are String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performDeleteRequestAsync(String uri, Map<String, String> headers);
}

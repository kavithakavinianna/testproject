package apiWrappers.interfaces;

import java.net.URI;
import java.util.Map;

import apiWrappers.model.HelperParameters;
import apiWrappers.model.Response;

public interface IHttpsClientAsyncHelpers {

	/**
	 * Method to perform Asynchronous GET Request with SSL and return
	 * RestResponse, parameters are String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performGetSSLRequestAsync(String uri, Map<String, String> headers);

	/**
	 * Method to perform Asynchronous GET Request with SSL and return
	 * RestResponse, parameters are URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performGetSSLRequestAsync(URI uri, Map<String, String> headers);

	/**
	 * Method to perform Asynchronous POST Request with SSL and return
	 * RestResponse, parameters are URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performPostSLLRequestAsync(HelperParameters helperParameters);

	/**
	 * Method to perform Asynchronous PUT Request with SSL and return
	 * RestResponse, parameters are URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performPutSSLRequestAsync(HelperParameters helperParameters);

	/**
	 * Method to perform Asynchronous DELETE Request with SSL and return
	 * RestResponse, parameters are URI url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performDeleteSSLRequestAsync(URI uri, Map<String, String> headers);

	/**
	 * Method to perform Asynchronous DELETE Request with SSL and return
	 * RestResponse, parameters are String url, Map<String, String> headers
	 * 
	 * @return RestResponse
	 */
	Response performDeleteSSLRequestAsync(String uri, Map<String, String> headers);

}

package uiWrappers.webWrappers.interfaces;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public interface IWebActions {
	/**
	 * Method to find if the element is enabled in the DOM
	 * 
	 * @param locator
	 * @return true if the element is enabled else false
	 */
	boolean isEnabled(By locator);

	/**
	 * Method to find if the element is displayed in the DOM
	 * 
	 * @param locator
	 * @return true if the element is enabled else false
	 */
	boolean isDisplayed(By locator);

	/**
	 * Method to find the given locator within the DOM. Returns the WebElement
	 * if the element is present within the DOM. This method performs retry for
	 * FindElement, if 'RetryCount' key is added to testcinfig.env.json file and
	 * has a valid value assigned. By default RetryCount will be 0.
	 * 
	 * @param locator
	 */
	WebElement findElement(By locator);
	
	List<WebElement> findElements(By locator);

	/**
	 * Method for navigating to URL
	 * 
	 * @param url
	 */
	void navigateToURL(String url);

	/**
	 * Returns page title.
	 * 
	 * @return page title.
	 */
	 String getWindowTitle();

	/**
	 * Method to perform a click operation on the given element.
	 * 
	 * @param locator
	 */
	void click(By locator);

	/**
	 * Method to perform send keys operation
	 * 
	 * @param locator
	 * @param value
	 *            that to be enter.
	 */
	void sendKeys(By locator, String value);

	/**
	 * Method to perform a clear operation on the given element.
	 * 
	 * @param locator
	 */
	void clear(By locator);

	/**
	 * Method to return the text of the element
	 * 
	 * @param locator
	 * @return textValue
	 */
	public String getText(By locator);

	/**
	 * Method to simulates a key stroke into the given element.
	 * 
	 * @param key,
	 *            locator
	 */
	void PressKey(By locator, Keys key);

	/**
	 * Method to send a sequence of keystrokes to the browser.
	 * 
	 * @param key
	 */
	void PressKey(Keys key);

	/**
	 * Method to perform a double click operation on the given element.
	 * 
	 * @param locator
	 */
	void doubleClick(By locator);

	/**
	 * Method to get Attribute Value
	 * 
	 * @param locator
	 * @param attributeName
	 */
	String getAttributeValue(By locator, String attributeName);

	/**
	 * Method to scroll to specific web-element position
	 * 
	 * @param locator
	 */
	void scrollToElement(By locator);

	/**
	 * Method to select from drop down using visible text
	 * 
	 * @param locator
	 * @param Matching_Text
	 */
	void selectByVisibleText(By locator, String text);

	/**
	 * Method to select from drop down using Value
	 * 
	 * @param locator
	 * @param Matching_value
	 */
	void selectByValue(By locator, String value);

	/**
	 * Method to select from drop down using Index
	 * 
	 * @param locator
	 * @param index
	 */
	void selectByIndex(By locator, int index);

	/**
<<<<<<< HEAD
	 * Method to select a random value from drop down
	 * 
	 * @param locator
	 */
	void selectByRandomIndex(By locator);

	/**
=======
>>>>>>> f4edf59d0f909ef3eaf84c2410e955c0b0c4ce5e
	 * Method to deselect from drop down using visible text
	 * 
	 * @param WebElement
	 * @param Matching_Text
	 */
	void deSelectByVisibleText(By locator, String text);

	/**
	 * Method to deselect from drop down using value
	 * 
	 * @param locator
	 * @param Matching_value
	 */
	void deSelectByValue(By locator, String value);

	/**
	 * Method to deselect from drop down using index
	 * 
	 * @param locator
	 * @param index
	 */
	void deSelectByindex(By locator, int index);

	/**
	 * Method to deselect all
	 * 
	 * @param locator
	 */
	void deSelectAll(By locator);

	/**
	 * Method to check if the element is displayed
	 * 
	 * @param WebElement
	 * @return true if the element is displayed else return false
	 */
	boolean isElementPresent(By locator);

	/**
	 * Method to switch to desired frame
	 * 
	 * @param locator
	 */
	public void switctoIFrame(By locator);

	/**
	 * Method to Switch to Active Window. Switches to the element that currently
	 * has focus within the document currently "switched to", or the body
	 * element if this cannot be detected. This matches the semantics of calling
	 * "document.activeElement" in Javascript.
	 * 
	 * @return The WebElement with focus, or the body element if no element with
	 *         focus can be detected.
	 */
	WebElement switchToActiveWindow();

	/**
	 * Checks if an browser alert is present or not.Returns true, if any browser
	 * alert is present.Returns false, if no browser alert is present.
	 * 
	 * @return boolean
	 */
	boolean isAlertPresent();

	/**
	 * Dismiss the alert.
	 * 
	 */
	void dismissAlert();

	/**
	 * Accepts the alert
	 */
	void acceptAlert();

	/**
	 * Gets source code of the current web page.
	 * 
	 * @return String ::Source code of the current web page
	 */
	String getPageSource();

	/**
	 * Refreshes the current page.
	 * 
	 */
	void pageRefresh();

	/**
	 * Returns URL of current window.
	 * 
	 * @return String
	 * 
	 */
	String getCurrentUrl();

	/**
	 * Quits the browser, closing every associated window.
	 * 
	 */
	void quitBrowser();

	/**
	 * Close the current window, quitting the browser if there's only one window
	 * available.
	 * 
	 */
	void closeBrowser();

	/**
	 * Executes javascript in the context of the currently selected frame or
	 * window.
	 * 
	 * @param script
	 *            in the form of string
	 */
	void executeJavascript(String script);

	/**
	 * Method to Performs a click operation on the given element using Actions
	 * classes
	 * 
	 * @param locator
	 */
	void actionClick(By locator);

	/**
	 * Method to check wether
	 * 
	 * @param locator
	 */
	boolean isSelected(WebElement element);

	/**
	 * Method to select uncheckbox
	 * 
	 * @param locator
	 */
	void unSelectCheckbox(By locator);

	/**
	 * Method to select checkbox
	 * 
	 * @param locator
	 */
	void selectCheckbox(By locator);

	/**
	 * Method to fetch selected option from dropdown
	 * 
	 * @param locator
	 * @return selected option
	 */
	List<WebElement> getAllOption(By locator);

	/**
	 * Performs mouse right click on the given element.
	 * 
	 * @param locator
	 */
	void contextClick(By locator);

	/**
	 * Performs a mouse click on the given element using javascript.
	 * 
	 * @param locator
	 */
	void javascriptClick(By locator);

	/**
	 * An expectation to check if any browser alert is present and return.
	 */
	Alert getAlert();

	/**
	 * Sets the text to the input alert.
	 * 
	 * @param value
	 */
	void setAlertText(String value);

	/**
	 * Gets the text of the alert.
	 * 
	 * @return String alert text
	 */
	String getAlertText();

	/**
	 * Gets the current window handle, which is an opaque handle to this window
	 * that uniquely identifies it within this driver instance.
	 * 
	 * @return String
	 */
	String getCurrentWindowHandle();

	/**
	 * Gets the window handles of open browser windows.
	 * 
	 * @return Set<String>
	 */
	Set<String> getWindowHandles();

	/**
	 * Returns the number of open browser windows.
	 * 
	 * @return int
	 */
	int getWindowHandlesCount();

	/**
	 * Determines whether pop-up window exist in open browser windows.
	 * 
	 * @param parentWindowHandle
	 * @return boolean
	 */
	boolean isPopupWindowExist(String parentWindowHandle);

	/**
	 * Determines whether the page title is equals to specified value.
	 * 
	 * @param pageTitle
	 * @return boolean
	 */
	boolean isWindowTitle(String pageTitle);

	/**
	 * Scrolls the document by the specified number of pixels.
	 * 
	 * @param xnum
	 * @param ynum
	 */
	void scrollToScreenCoordinates(int xnum, int ynum);

	/**
	 * An expectation to select either the first frame on the page or the main
	 * document when a page contains iFrames.
	 */
	void switchToDefaultFrame();

	/**
	 * An expectation to select a frame using its previously located
	 * 
	 * @param element
	 */
	void switchToFrameByElement(WebElement element);

	/**
	 * An expectation to select a frame by its (zero-based) index.
	 * 
	 * @param frameIndex
	 */
	void switchToFrameByIndex(int frameIndex);

	/**
	 * An expectation to select a frame by its name.
	 * 
	 * @param frameName
	 */
	void switchToFrameByName(String frameName);

	/**
	 * An expectation to select the parent frame of the currently selected
	 * frame.
	 */
	void switchToParentFrame();

	/**
	 * Switches the focus of future commands for this driver to the window with
	 * the given page title.
	 * 
	 * @param title
	 * @return boolean
	 */
	boolean switchToWindowByTitle(String title);

	/**
	 * It will open a new blank tab
	 */
	void openNewTab();

	/**
	 * It will open a new blank browser window
	 */
	void openNewBrowserWindow();

	/**
	 * Method that navigate window back
	 */
	void navigateBack();

	/**
	 * Method that navigate window forward
	 */
	void navigateForward();
}

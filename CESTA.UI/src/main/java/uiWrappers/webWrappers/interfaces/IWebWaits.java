package uiWrappers.webWrappers.interfaces;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;

import uiWrappers.WaitType;

public interface IWebWaits {
	/**
	 * Method to get Wait type
	 * 
	 * @param waitType
	 *            (SmallWait, MediumWait, LargeWait, ImplicitWaitTime,
	 *            PageLoadTimeout, ExplisitWaitTime)
	 * @return waitTime wait time based on WaitType
	 */
	int getWait(WaitType waitType);

	/**
	 * An expectation to wait and check an element is visible and enabled such
	 * that it can be clicked.
	 * 
	 * @param locator
	 *            :: Locator for the web element
	 * @param waitType
	 *            :: Type of wait from WaitType
	 */
	void waitForElementToBeClickable(By locator, WaitType waitType);

	/**
	 * An expectation for checking that an element is present on the DOM of a
	 * page. This does not necessarily mean that the element is visible or
	 * enabled.
	 * 
	 * @param locator
	 *            :: Locator for the web element
	 * @param waitType
	 *            :: Type of wait from WaitType
	 */
	void waitForElementToBePresent(By locator, WaitType waitType);

	/**
	 * An expectation for checking that an element is present on the DOM of a
	 * page and visible.
	 * 
	 * @param locator
	 *            :: Locator for the web element
	 * @param waitType
	 *            :: Type of wait from WaitType
	 */
	void waitForElementToBeVisible(By locator, WaitType waitType);

	/**
	 * An expectation to wait and check an element is either invisible or not
	 * present in the DOM.
	 * 
	 * @param locator
	 *            :: Locator for the web element
	 * @param waitType
	 *            :: Type of wait from WaitType
	 */
	void waitForElementToBeInvisible(By locator, WaitType waitType);

	/**
	 * Method to wait for page to load
	 */
	void waitForPageLoad();

	/**
	 * Reusable method to make the driver to wait until the document loads
	 * completely within given maximum wait time
	 * 
	 * @param maxWaitTimeInSec
	 */
	void waitForBrowserLoad(int maxWaitTimeInSec);

	/**
	 * Reusable method to make the driver to wait for document to load
	 * completely within 120 Seconds.
	 * 
	 * @param maxWaitTimeInSec
	 */
	void waitForBrowserLoad();

	/**
	 * Fluent wait for locator to be enabled polls page every second for time
	 * set in minutes(timeout)
	 * 
	 * @param element(getter)
	 */
	void fluentWaitIsEnabledInSec(By locator);

	/**
	 * Fluent wait for WebElement to be enabled polls page every Second for time
	 * set in minutes(timeout)
	 * 
	 * @param element(getter)
	 * @param timeoutInMin
	 */
	void fluentWaitIsEnabledInSec(By locator, int timeoutInMin);

	/**
	 * Fluent wait for WebElement to be displayed polls page every second for
	 * time set in minutes(timeout)
	 * 
	 * @param element(getter)
	 * @param timeoutInMin
	 */
	void fluentWaitIsDisplayedInSec(By locator);

	/**
	 * Fluent wait for WebElement to be displayed polls page every second for
	 * time set in seconds(timeout)
	 * 
	 * @param locator,
	 *            element id, xpath, cssSelector, name, class etc..
	 * @param timeoutInSec
	 * @return
	 */
	void fluentWaitIsDisplayedInSec(By locator, int timeoutInSec);

	/**
	 * Waits for alert to be present.
	 * 
	 * @param waitType
	 */
	void waitForAlertToBePresent(WaitType waitType);

	/**
	 * Waits for the specified timespan and determines whether the page title is
	 * equals to specified value.
	 * 
	 * @param pageTitle
	 * @param waitTime
	 * @return boolean
	 */
	boolean waitAndVerifyPageTitle(String pageTitle, WaitType waitTime);

	/**
	 * An expectation to wait and check the title of a page.
	 * 
	 * @param pageTitle
	 * @param waitTime
	 * @return
	 */
	boolean waitForPageTitle(String pageTitle, WaitType waitType);

	/**
	 * An expectation to wait and switch to the currently active modal dailog
	 * for this particular driver instance.
	 * 
	 * @param waitTime
	 * @return Alert
	 */
	Alert waitAndGetAlert(WaitType waitTime);

	/**
	 * An expectation to wait and check that the web element with text is either
	 * invisible or not present on the DOM.
	 * 
	 * @param locator
	 * @param text
	 * @param waitType
	 */
	void waitForElementWithTextToBeInvisible(By locator, String text, WaitType waitType);

	/**
	 * An expectation to wait and check that the given text is present in the
	 * given locator.
	 * 
	 * @param locator
	 * @param text
	 * @param waitType
	 */
	void waitForTextToBePresentInElement(By locator, String text, WaitType waitType);

	/**
	 * An expectation for wait and check whether the given frame is available to
	 * switch to. If the frame is available it switches the driver to the
	 * specified frame.
	 * 
	 * @param locator
	 * @param waitType
	 */
	void waitAndSwitchToFrame(By locator, WaitType waitType);

	/**
	 * An expectation to wait for ajax load to be completed.
	 */
	void waitForAjaxLoad();

	/**
	 * An expectation to wait for network calls to be finished.
	 */
	void waitForNetworkCalls();
}

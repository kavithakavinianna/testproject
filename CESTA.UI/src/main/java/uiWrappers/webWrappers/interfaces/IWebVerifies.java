package uiWrappers.webWrappers.interfaces;

import org.openqa.selenium.By;

import uiWrappers.WaitType;

public interface IWebVerifies {

	/**
	 * Method to verify the element is Present in DOM or not
	 * 
	 * @param by
	 */
	void verifyElementIsPresent(By by);

	/**
	 * Method to verify the element is displayed or not
	 * 
	 * @param by
	 */
	void verifyElementIsDisplayed(By by);

	/**
	 * Method to verify the element is enabled or not
	 * 
	 * @param by
	 */
	void verifyElementIsEnabled(By by);

	/**
	 * Method to verify the element is clickable or not
	 * 
	 * @param by
	 */
	void verifyElementIsClickable(By by);

	/**
	 * Method to verify the Attribute is present in DOM or not
	 * 
	 * @param by,
	 *            attribute, expectedValue
	 */
	void verifyAttributeHasValue(By by, String attribute, String expectedValue);

	/**
	 * Asserts if {@link by} element has the specified attribute
	 * {@link attribute}
	 * 
	 * @param by
	 *            {@link By} locator
	 * @param attribute
	 *            HTML attribute
	 */
	public void verifyElementHasAttribute(By by, String attributeName);

	/**
	 * Method to validates that the given text box does not contain any text in
	 * it, else FAIL the test case.Passes the assert if given text box does not
	 * contain any text in it.
	 * 
	 * @param by
	 *            locator of the web element
	 */
	void verifyTextboxIsEmpty(By by);

	/**
	 * Validates that the web element contains desired text, else FAIL the test
	 * case.Passes the assert if web element contains desired text.
	 * 
	 * @param by
	 *            locator of the web element.
	 * @param text
	 *            Expected text.
	 */
	void verifyElementContainsText(By by, String text);

	/**
	 * Validate that the web element does not contain the particular text, else
	 * FAIL the test case.Passes the assert if the web element does not contain
	 * the particular text.
	 * 
	 * @param by
	 *            locator of the web element.
	 * @param text
	 *            The text that web element should NOT contain.
	 */
	void verifyElementDoesNotContainsText(By by, String text);

	/**
	 * Validate that the web element has exactly the same text as passed while
	 * calling the method, else FAIL the test case.>Passes the assert if the web
	 * element has exactly the same text.
	 * 
	 * @param by
	 *            locator of the web element.
	 * @param text
	 *            expected text that web element should EXACTLY match
	 */
	void verifyExactTextInElementIs(By by, String text);

	/**
	 * Validates that the web element does not match the particular text, else
	 * FAIL the test case.Passes the assert if the web element does not match
	 * the particular text.
	 * 
	 * @param by
	 *            locator of the web element.
	 * @param text
	 *            The text that web element should NOT match with
	 */
	void verifyExactTextInElementIsNot(By by, String text);

	/**
	 * Asserts exact page title.
	 * 
	 * @param pageTitle
	 *            expected page title.
	 */
	void verifyPageTitle(String pageTitle);

	/**
	 * Asserts partial page title.
	 * 
	 * @param partialPageTitle
	 *            partial page title.
	 */
	void verifyPartialPageTitle(String partialPageTitle);

	/**
	 * Validate that the check-box is Unchecked, else FAIL the test case.Passes
	 * the assert if the check-box is Unchecked.
	 * 
	 * @param by
	 */
	void verifyElementIsSelected(By by);

	/**
	 * Validate that the web element is Disabled, else FAIL the test case.Passes
	 * the assert if the web element is Disabled.
	 * 
	 * @param by
	 */
	void verifyElementIsDisabled(By by);

	/**
	 * Asserts exact page title.Passes the assert if page title matches the
	 * expected page title.
	 * 
	 * @param expectedPageTitle
	 */
	void verifyExactPageTitle(String expectedPageTitle);

	/**
	 * Verifies message on browser alert.Passes the assert if message on browser
	 * alert matches expected message.
	 * 
	 * @param msg
	 * @param waitType
	 */
	void verifyBrowserAlertMsg(String msg, WaitType waitType);

	/**
	 * Verifies that the file exist in the file path, if not wait for 1
	 * minute(polling every 3 sec) and check the existence.Passes the assert if
	 * file exists in the specified file path.
	 * 
	 * @param AbsoluteFilePath
	 * @param waitType
	 */
	void verifyFileExists(String AbsoluteFilePath, WaitType waitType);
}

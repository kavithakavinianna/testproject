package uiWrappers.webWrappers.wrappers;

import java.io.File;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import configuration.driverManager.DriverManager;
import exceptionHandler.CustomExceptionHandler;
import loggers.interfaces.ILogger;
import reporters.interfaces.IReporter;
import testConfiguration.Singleton;
import uiWrappers.WaitType;
import uiWrappers.webWrappers.interfaces.IWebVerifies;

/**
 * @author CESTA Team
 *
 */
public class WebVerify implements IWebVerifies {
	WebActions webActions;
	WebWaits webWaits;
	ILogger log;
	IReporter report;

	public WebVerify() {
		webActions = Singleton.getInstance(WebActions.class);
		webWaits = Singleton.getInstance(WebWaits.class);
		log = Singleton.getInstance(ILogger.class);
		report = Singleton.getInstance(IReporter.class);
	}

	/**
	 * Method to verify the element is Present in DOM or not
	 * 
	 * @param by
	 */
	public void verifyElementIsPresent(By by) {
		WebElement element = webActions.findElement(by);
		Assert.assertTrue(element != null,
				"Validation Failed : Expected Element: " + by + ", is not present in the DOM.");
	}

	/**
	 * Method to verify the element is displayed or not
	 * 
	 * @param by
	 */
	public void verifyElementIsDisplayed(By by) {
		Assert.assertTrue(webActions.isDisplayed(by), "Validation Failed : Element is NOT Displayed : " + by);
	}

	/**
	 * Method to verify the element is enabled or not
	 * 
	 * @param by
	 */
	public void verifyElementIsEnabled(By by) {
		Assert.assertTrue(webActions.isEnabled(by), "Validation Failed : Element is NOT Enabled : " + by);
	}

	/**
	 * Method to verify the element is clickable or not
	 * 
	 * @param by
	 */
	public void verifyElementIsClickable(By by) {
		Assert.assertTrue(webActions.isEnabled(by) && webActions.isDisplayed(by),
				"Validation Failed : Element is NOT Clickable : " + by);
	}

	/**
	 * Method to verify the Attribute is present in DOM or not
	 * 
	 * @param by,
	 *            attribute, expectedValue
	 */
	public void verifyAttributeHasValue(By by, String attribute, String expectedValue) {
		String actualValue = DriverManager.getWebDriver().findElement(by).getAttribute(attribute);
		Assert.assertTrue(actualValue.contains(expectedValue), "Validation Failed : Attribute Has No Value : " + by);
		log.info("Verified that the attribute : " + attribute + " having the expected value :" + expectedValue
				+ " at locator :" + by);
	}

	/**
	 * Method to validates that the given text box does not contain any text in
	 * it, else FAIL the test case.Passes the assert if given text box does not
	 * contain any text in it.
	 * 
	 * @param by
	 *            locator of the web element
	 */
	public void verifyTextboxIsEmpty(By by) {
		String textBoxValue = webActions.getAttributeValue(by, "value");
		if (!textBoxValue.trim().equals("")) {
			String failureMsg = "Validation Failed : The text-box '" + by + "' is NOT Empty; it contains : "
					+ textBoxValue;
			Assert.fail(failureMsg, new NotFoundException(failureMsg));
		}
	}

	/**
	 * Validates that the web element contains desired text, else FAIL the test
	 * case.Passes the assert if web element contains desired text.
	 * 
	 * @param by
	 *            locator of the web element.
	 * @param text
	 *            Expected text.
	 */
	public void verifyElementContainsText(By by, String text) {
		String uiText = webActions.getText(by);
		if (!uiText.contains(text)) {
			String failureMsg = "Validation Failed : Element text '" + uiText + "' does NOT contain expected '" + text
					+ "' : " + by;
			Assert.fail(failureMsg, new NotFoundException(failureMsg));
		}
	}

	/**
	 * Asserts if {@link by} element has the specified attribute
	 * {@link attribute}
	 * 
	 * @param by
	 *            {@link By} locator
	 * @param attribute
	 *            HTML attribute
	 */
	public void verifyElementHasAttribute(By by, String attributeName) {
		String value = webActions.getAttributeValue(by, attributeName);
		if (value == null)
			Assert.fail("Validation Failed :" + by + " does not contains the specified attribute: " + attributeName);
	}

	/**
	 * Validate that the web element does not contain the particular text, else
	 * FAIL the test case.Passes the assert if the web element does not contain
	 * the particular text.
	 * 
	 * @param by
	 *            locator of the web element.
	 * @param text
	 *            The text that web element should NOT contain.
	 */
	public void verifyElementDoesNotContainsText(By by, String text) {
		String uiText = webActions.getText(by);
		if (uiText.contains(text)) {
			String failureMsg = "Validation Failed : Element(" + by + ") contains unexpected text '" + text
					+ ", Actual text " + uiText;
			Assert.fail(failureMsg, new NotFoundException(failureMsg));
		}
	}

	/**
	 * Validate that the web element has exactly the same text as passed while
	 * calling the method, else FAIL the test case.>Passes the assert if the web
	 * element has exactly the same text.
	 * 
	 * @param by
	 *            locator of the web element.
	 * @param text
	 *            expected text that web element should EXACTLY match
	 */
	public void verifyExactTextInElementIs(By by, String text) {
		String uiText = webActions.getText(by);
		if (!uiText.equals(text)) {
			String failureMsg = "Validation Failed : Element text '" + uiText + "' does NOT match expected '" + text
					+ "' : " + by;
			Assert.fail(failureMsg, new NotFoundException(failureMsg));
		}
	}

	/**
	 * Validates that the web element does not match the particular text, else
	 * FAIL the test case.Passes the assert if the web element does not match
	 * the particular text.
	 * 
	 * @param by
	 *            locator of the web element.
	 * @param text
	 *            The text that web element should NOT match with
	 */
	public void verifyExactTextInElementIsNot(By by, String text) {
		String uiText = webActions.getText(by);
		if (uiText.equals(text)) {
			String failureMsg = "Validation Failed : Element text '" + uiText + "' is SAME as '" + text + "' : " + by;
			Assert.fail(failureMsg, new NotFoundException(failureMsg));
		}
	}

	/**
	 * Validate selected Option in a drop-down.Passes the assert if the
	 * specified option is Selected.
	 * 
	 * @param by
	 * @param option
	 */
	public void verifySelectedOption(By by, String option) {
		try {
			String selectedOption = webActions.getSelectedOption(by);
			if (selectedOption.equals(option)) {
				// Pass
				String successMsg = "Validation Successful : Selected Option '" + selectedOption
						+ "' matches expected '" + option + "' : " + by;
				log.debug(successMsg);
				// report.logPassedTest(successMsg);
			} else {
				// Fail
				String failureMsg = "Validation Failed : Selected Option '" + selectedOption
						+ "' does NOT match expected '" + option + "' : " + by;
				CustomExceptionHandler.ExceptionHandler(new Exception(failureMsg), failureMsg);
			}
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception thrown while checking if selected option in drop-down '" + by.toString() + "' is '"
							+ option);
		}
	}

	/**
	 * Asserts exact page title.
	 * 
	 * @param pageTitle
	 *            expected page title.
	 */
	public void verifyPageTitle(String pageTitle) {
		String actualPageTitle = webActions.getWindowTitle();
		Assert.assertEquals(pageTitle, actualPageTitle);
	}

	/**
	 * Asserts partial page title.
	 * 
	 * @param partialPageTitle
	 *            partial page title.
	 */
	public void verifyPartialPageTitle(String partialPageTitle) {
		String actualPageTitle = webActions.getWindowTitle();
		Assert.assertTrue(actualPageTitle.contains(partialPageTitle));
	}

	/**
	 * Validate that the check-box is Checked, else FAIL the test case.Passes
	 * the assert if the check-box is Checked.
	 * 
	 * @param by
	 */
	public void verifyElementIsSelected(By by) {
		WebElement element = webActions.findElement(by);
		if (!webActions.isSelected(element)) {
			String failureMsg = "Validation Failed : Element" + by + " is NOT selected.";
			CustomExceptionHandler.ExceptionHandler(new Exception(failureMsg), failureMsg);
		}
	}

	/**
	 * Validate that the check-box is Unchecked, else FAIL the test case.Passes
	 * the assert if the check-box is Unchecked.
	 */
	public void verifyElementIsNotSelected(By by) {
		WebElement element = webActions.findElement(by);
		if (webActions.isSelected(element)) {
			String failureMsg = "Validation Failed : Element: " + by + " is selected.";
			CustomExceptionHandler.ExceptionHandler(new Exception(failureMsg), failureMsg);
		}
	}

	/**
	 * Validate that the web element is Disabled, else FAIL the test case.Passes
	 * the assert if the web element is Disabled.
	 * 
	 * @param by
	 */
	public void verifyElementIsDisabled(By by) {
		if (!webActions.isEnabled(by)) {
			String failureMsg = "Validation Failed : Element is NOT Disabled : " + by;
			CustomExceptionHandler.ExceptionHandler(new Exception(failureMsg), failureMsg);
		}
	}

	/**
	 * Asserts exact page title.Passes the assert if page title matches the
	 * expected page title.
	 * 
	 * @param expectedPageTitle
	 */
	public void verifyExactPageTitle(String expectedPageTitle) {
		try {
			String ActualPageTitle = webActions.getWindowTitle();
			Assert.assertTrue(ActualPageTitle.equals(expectedPageTitle), "Mismatch in page title. Actual page title : "
					+ ActualPageTitle + ", Expected page title : " + expectedPageTitle);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception thrown while trying to validate exact page title: " + expectedPageTitle);
		}
	}

	/**
	 * Verifies message on browser alert.Passes the assert if message on browser
	 * alert matches expected message.
	 * 
	 * @param msg
	 * @param waitType
	 */
	public void verifyBrowserAlertMsg(String msg, WaitType waitType) {
		try {
			webWaits.waitForAlertToBePresent(waitType);
			String alertMsg = webActions.getAlertText();
			Assert.assertEquals(alertMsg, msg);
			log.info("Verified alert message: " + msg + ", with wait type: " + waitType.toString());
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception thrown while trying to validate alert message: " + msg
					+ ", with wait type: " + waitType.toString());
		}
	}

	/**
	 * Verifies that the file exist in the file path, if not wait for 1
	 * minute(polling every 3 sec) and check the existence.Passes the assert if
	 * file exists in the specified file path.
	 * 
	 * @param AbsoluteFilePath
	 * @param waitType
	 */
	public void verifyFileExists(String AbsoluteFilePath, WaitType waitType) {
		boolean result = false;
		int i = 0;
		try {
			File f = new File(AbsoluteFilePath);
			do {
				result = f.exists();
				if (f.exists()) {
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						CustomExceptionHandler.ExceptionHandler(e);
					}
				}
				i++;
			} while (!result && i < Integer.parseInt(waitType.toString()));
			log.info("Verified file exists at path : " + AbsoluteFilePath);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while checking if file exists at path: '" + AbsoluteFilePath);
		}
	}
}

package uiWrappers.webWrappers.wrappers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import configuration.driverManager.DriverManager;
import exceptionHandler.CustomExceptionHandler;
import loggers.interfaces.ILogger;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;
import uiWrappers.webWrappers.interfaces.IWebActions;

/**
 * @author CESTA Team
 *
 */
public class WebActions implements IWebActions {

	private TestEnvironment testEnvironment;

	private ILogger log;

	public WebActions() {
		testEnvironment = Singleton.getInstance(TestEnvironment.class);
		log = Singleton.getInstance(ILogger.class);
	}

	/**
	 * Method to find if the element is enabled in the DOM
	 * 
	 * @param locator
	 * @return true if the element is enabled else false
	 */
	public boolean isEnabled(By locator) {
		boolean flag = false;
		try {
			log.info("Verifying if the element is enabled in the DOM");
			WebElement element = findElement(locator);
			flag = element.isEnabled();
		} catch (Exception e) {
			//do nothing
		}
		return flag;
	}

	/**
	 * Method to find if the element is displayed in the DOM
	 * 
	 * @param locator
	 * @return true if the element is enabled else false
	 */
	public boolean isDisplayed(By locator) {
		boolean flag = false;
		try {
			log.info("Verifying if the element is displayed or not");
			WebElement element = findElement(locator);
			flag = element.isDisplayed();
		} catch (Exception e) {
			//do nothing
		}
		return flag;
	}

	/**
	 * Method to find the given locator within the DOM. Returns the WebElement
	 * if the element is present within the DOM. This method performs retry for
	 * FindElement, if 'RetryCount' key is added to testcinfig.env.json file and
	 * has a valid value assigned. By default RetryCount will be 0.
	 * 
	 * @param locator
	 */
	public WebElement findElement(By locator) {
		WebElement element = null;
		int retry = 0;
		try {
			if (testEnvironment.containsProperty("RetryCount")) {
				retry = Integer.parseInt(testEnvironment.getProperty("RetryCount"));
			} else {
				retry = 0;
			}
			do {
				try {
					retry--;
					log.info("");
					element = DriverManager.getWebDriver().findElement(locator);
				} catch (Exception e) {
					if (retry > 0) {
						continue;
					} else {
						throw e;
					}
				}
			} while ((retry > 0) && (element == null));
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to find element using locator:"
					+ locator + ", with RetryCount:" + retry);
		}
		return element;
	}

	/**
	 * Method to find the given locator within the DOM. Returns the List of
	 * WebElements if the elements are present within the DOM. This method
	 * performs retry for FindElements, if 'RetryCount' key is added to
	 * testcinfig.env.json file and has a valid value assigned. By default
	 * RetryCount will be 0.
	 * 
	 * @param locator
	 */
	public List<WebElement> findElements(By locator) {
		List<WebElement> element = null;
		int retry = 0;
		try {
			if (testEnvironment.containsProperty("RetryCount")) {
				retry = Integer.parseInt(testEnvironment.getProperty("RetryCount"));
			} else {
				retry = 0;
			}
			do {
				try {
					retry--;
					element = DriverManager.getWebDriver().findElements(locator);
				} catch (Exception e) {
					if (retry > 0) {
						continue;
					} else {
						throw e;
					}
				}
			} while ((retry > 0) && (element == null));
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to find elements using locator:"
					+ locator + ", with RetryCount:" + retry);
		}
		return element;
	}

	/**
	 * Method for navigating to URL
	 * 
	 * @param url
	 */
	public void navigateToURL(String url) {
		try {
			DriverManager.getWebDriver().get(url);
			log.info("Navigating to the URL: " + url);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while navigating to URL ::" + url);
		}
	}

	/**
	 * Method to perform sendkeys operation
	 * 
	 * @param locator
	 */
	public void sendKeys(By locator, String value) {
		WebElement element;
		try {
			element = findElement(locator);
			element.clear();
			element.sendKeys(value);
			log.info("Typing " + value + " to the locator " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception while entering text to: " + locator);
		}
	}

	/**
	 * Method to return the text of the element
	 * 
	 * @param locator
	 * @return textValue
	 */
	public String getText(By locator) {
		WebElement element;
		String textValue = null;
		try {
			element = findElement(locator);
			textValue = element.getText();
			log.info("Getting text from the locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error while fetching text of element " + locator);
		}
		return textValue;
	}

	/**
	 * Method to simulates a key stroke into the given element.
	 * 
	 * @param key,
	 *            locator
	 */
	public void PressKey(By locator, Keys key) {
		WebElement element;
		try {
			element = findElement(locator);
			getActionsObj().sendKeys(element, key).build().perform();
			log.info("Press Key: " + key + " at locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Error while perfoming Press Key - " + key + " operation at element :: " + locator);
		}
	}

	/**
	 * Method to send a sequence of keystrokes to the browser.
	 * 
	 * @param key
	 */
	public void PressKey(Keys key) {
		try {
			getActionsObj().sendKeys(key).build().perform();
			log.info("Press Key: " + key);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error while perfoming Press Key :: " + key + " operation");
		}
	}

	/**
	 * Method to get Action Class object
	 * 
	 * @return action class reference
	 */
	public Actions getActionsObj() {
		Actions actions = null;
		try {
			actions = new Actions(DriverManager.getWebDriver());
			log.info("Getting Action Object");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception while geting Actions class Object");
		}
		return actions;
	}

	/**
	 * Method to perform a click operation on the given element.
	 * 
	 * @param locator
	 */
	public void click(By locator) {
		WebElement element;
		try {
			element = findElement(locator);
			element.click();
			log.info("Click on: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception in clicking element: " + locator);
		}
	}

	/**
	 * Method to perform a double click operation on the given element.
	 * 
	 * @param locator
	 */
	public void doubleClick(By locator) {
		WebElement element;
		try {
			element = findElement(locator);
			WebDriverWait wait = getWebDriverWait();
			wait.until(ExpectedConditions.elementToBeClickable(element));
			getActionsObj().doubleClick(element).build().perform();
			log.info("Double click on: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception in double clicking element: " + locator);
		}
	}

	/**
	 * Method to get WebDriverWait Class object
	 * 
	 * @return wait class reference
	 */
	public WebDriverWait getWebDriverWait() {
		WebDriverWait wait = null;
		try {
			wait = new WebDriverWait(DriverManager.getWebDriver(), testEnvironment.ImplicitWaitTimeout);
			log.info("Getting WebDriverWait object");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception while getting WebDriverWait class object");
		}
		return wait;
	}

	/**
	 * Method to perform clear operation
	 * 
	 * @param locator
	 */
	public void clear(By locator) {
		WebElement element;
		try {
			element = findElement(locator);
			element.clear();
			log.info("Clear text from the locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception while clearing text: " + locator);
		}
	}

	/**
	 * Method to get Attribute Value
	 * 
	 * @param locator
	 * @param attributeName
	 */
	public String getAttributeValue(By locator, String attributeName) {
		String value = null;
		WebElement element;
		try {
			element = findElement(locator);
			value = element.getAttribute(attributeName);
			log.info("Getting Attribute Value from Attribute name: " + attributeName + "and Locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to get attribute: "
					+ attributeName + " value from element:" + locator);
		}
		return value;
	}

	/**
	 * Method to scroll to specific web-element position
	 * 
	 * @param locator
	 */
	public void scrollToElement(By locator) {
		try {
			getActionsObj().moveToElement(DriverManager.getWebDriver().findElement(locator));
			getActionsObj().perform();
			log.info("Scrolling to the element: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error scrolling to desired element " + locator);
		}
	}

	/**
	 * Select all options that display text matching the argument. That is, when
	 * given "Bar" this would select an option like:
	 *
	 * &lt;option value="foo"&gt;Bar&lt;/option&gt;
	 * 
	 * @param WebElement
	 * @param Matching_Text
	 */
	public void selectByVisibleText(By locator, String text) {
		try {
			scrollToElement(locator);
			Select sel = new Select(DriverManager.getWebDriver().findElement(locator));
			sel.selectByVisibleText(text);
			log.info("Selecting the text: " + text + " by locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error in selecting from dropdown " + locator);
		}
	}

	/**
	 * Deselect all options that display text matching the argument. That is,
	 * when given "Bar" this would deselect an option like:
	 *
	 * &lt;option value="foo"&gt;Bar&lt;/option&gt;
	 * 
	 * @param locator
	 * @param Matching_Text
	 */
	public void deSelectByVisibleText(By locator, String text) {
		try {
			scrollToElement(locator);
			Select sel = new Select(DriverManager.getWebDriver().findElement(locator));
			sel.deselectByVisibleText(text);
			log.info("DeSelect by the visible text: " + text + " and locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error in Deselecting from dropdown By Visible Text" + locator);
		}
	}

	/**
	 * Deselect all options that have a value matching the argument. That is,
	 * when given "foo" this would deselect an option like:
	 *
	 * &lt;option value="foo"&gt;Bar&lt;/option&gt;
	 * 
	 * @param locator
	 * @param Matching_value
	 */
	public void deSelectByValue(By locator, String value) {
		try {
			scrollToElement(locator);
			Select sel = new Select(DriverManager.getWebDriver().findElement(locator));
			sel.deselectByValue(value);
			log.info("DeSelecting by the value: " + value + " and locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error in Deselecting from dropdown by value" + locator);
		}
	}

	/**
	 * Method to Deselect the option at the given index. This is done by
	 * examining the "index" attribute of an element, and not merely by
	 * counting.
	 * 
	 * @param locator
	 * @param index
	 */
	public void deSelectByindex(By locator, int index) {
		try {
			scrollToElement(locator);
			Select sel = new Select(DriverManager.getWebDriver().findElement(locator));
			sel.deselectByIndex(index);
			log.info("DeSelecting by the index: " + index + " and locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error in Deselecting from dropdown by index" + locator);
		}
	}

	/**
	 * Method to Clear all selected entries. This is only valid when the SELECT
	 * supports multiple selections.
	 * 
	 * @param locator
	 */
	public void deSelectAll(By locator) {
		try {
			scrollToElement(locator);
			Select sel = new Select(DriverManager.getWebDriver().findElement(locator));
			sel.deselectAll();
			log.info("DeSelecting all by locator" + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error in Deselecting All from dropdown" + locator);
		}
	}

	/**
	 * Method to check if the element is displayed
	 * 
	 * @param WebElement
	 * @return true if the element is displayed else return false
	 */
	public boolean isElementPresent(By locator) {
		boolean flag = false;
		try {
			return flag = DriverManager.getWebDriver().findElement(locator).isDisplayed();
		} catch (NoSuchElementException e) {
			CustomExceptionHandler.ExceptionHandler(e, "The element " + locator + " is not displayed yet...");
		}
		log.info("Verifing if the element is present or not");
		return flag;
	}

	/**
	 * Method to get the number of rows present in the table
	 * 
	 * @param tableName
	 * @return row count
	 */
	public int getRowSize(By locator) {
		int rowsCount = 0;
		Reporter.log("Getting rows count of table " + locator);
		try {
			List<WebElement> rows_table = DriverManager.getWebDriver().findElements(By.tagName("tr"));
			rowsCount = rows_table.size();
			log.info("Getting the row size");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Excpetion in finding table: " + locator);
		}
		return rowsCount;
	}

	/**
	 * Method to get reference to all rows in the table
	 * 
	 * @param element
	 * @return List of rows in the table as web element
	 */
	public List<WebElement> getTablerows(By locator) {
		List<WebElement> tableRows = null;
		try {
			tableRows = DriverManager.getWebDriver().findElements(By.tagName("tr"));
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception in getting Table rows: " + locator);
		}
		return tableRows;
	}

	/**
	 * Select all options that have a value matching the argument. That is, when
	 * given "foo" this would select an option like:
	 *
	 * &lt;option value="foo"&gt;Bar&lt;/option&gt;
	 * 
	 * @param WebElement
	 * @param value
	 */
	public void selectByValue(By locator, String value) {
		try {
			scrollToElement(locator);
			Select sel = new Select(DriverManager.getWebDriver().findElement(locator));
			sel.selectByValue(value);
			log.info("Selecting by the value: " + value + " and locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error in selecting from dropdown by value" + locator);
		}
	}

	/**
	 * Select the option at the given index. This is done by examining the
	 * "index" attribute of an element, and not merely by counting.
	 * 
	 * @param locator
	 * @param index
	 */
	public void selectByIndex(By locator, int index) {
		try {
			scrollToElement(locator);
			Select sel = new Select(DriverManager.getWebDriver().findElement(locator));
			sel.selectByIndex(index);
			log.info("Selecting by the index: " + index + " and locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error in selecting from dropdown by given index" + locator);
		}
	}

	/**
	 * Method to Select the option from drop down at the random index
	 * 
	 * @param locator
	 */
	public void selectByRandomIndex(By locator) {
		try {
			scrollToElement(locator);
			Select sel = new Select(DriverManager.getWebDriver().findElement(locator));
			int max = 0, min = 2;
			max = sel.getOptions().size();
			if (max < min)
				Assert.fail(locator.toString() + " Drop down is empty");
			Random r = new Random();
			int index = r.nextInt(max - min) + min;
			sel.selectByIndex(index);
			log.info("Selecting by the Random Index: " + index);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error in selecting from dropdown by random index" + locator);
		}
	}

	/**
	 * Method to fetch all the option from select tag
	 * 
	 * @param locator
	 * @return All options belonging to this select tag
	 */
	public List<WebElement> getAllOption(By locator) {
		List<WebElement> options = new ArrayList<WebElement>();
		try {
			scrollToElement(locator);
			Select sel = new Select(DriverManager.getWebDriver().findElement(locator));
			options = sel.getOptions();
			log.info("Getting all the options by locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred in extension method while trying to get all options from drop-down:" + locator);
		}
		return options;
	}

	/**
	 * Method to Switch to Active Window. Switches to the element that currently
	 * has focus within the document currently "switched to", or the body
	 * element if this cannot be detected. This matches the semantics of calling
	 * "document.activeElement" in Javascript.
	 * 
	 * @return The WebElement with focus, or the body element if no element with
	 *         focus can be detected.
	 */
	public WebElement switchToActiveWindow() {
		WebElement ele = null;
		try {
			ele = DriverManager.getWebDriver().switchTo().activeElement();
			log.info("Switching to Active window");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to switch into active window.");
		}
		return ele;
	}

	/**
	 * Method to switch to desired IFrame
	 * 
	 * @param locator
	 */
	public void switctoIFrame(By locator) {
		WebElement element;
		try {
			element = DriverManager.getWebDriver().findElement(locator);
			DriverManager.getWebDriver().switchTo().frame(element);
			log.info("Switching to IFrame");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to switch into frame:" + locator);
		}
	}

	/**
	 * Method to select checkbox
	 * 
	 * @param locator
	 */
	public void selectCheckbox(By locator) {
		WebElement element;
		try {
			element = findElement(locator);
			if (!isSelected(element)) {
				click(locator);
				log.info("Selecting check box at the locator: " + locator);
			}
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to select check-box:" + locator);
		}
	}

	/**
	 * Method to select uncheckbox
	 * 
	 * @param locator
	 */
	public void unSelectCheckbox(By locator) {
		WebElement element;
		try {
			element = findElement(locator);
			if (isSelected(element)) {
				click(locator);
				log.info("UnSelecting check box at the locator: " + locator);
			}
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to unselect check-box:" + locator);
		}
	}

	/**
	 * Method to check whether the given element is selected or not
	 * 
	 * @param locator
	 */
	public boolean isSelected(WebElement element) {
		boolean flag = false;
		try {
			flag = element.isSelected();
			log.info("Verifying if the check box is selected or not for the element: " + element);
		} catch (Exception e) {
			flag = false;
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while validating element :" + element + " is Selected ");
		}
		return flag;
	}

	/**
	 * Method to Performs a click operation on the given element using Actions
	 * class classes
	 * 
	 * @param locator
	 */
	public void actionClick(By locator) {
		WebElement element;
		try {
			element = findElement(locator);
			WebDriverWait wait = getWebDriverWait();
			wait.until(ExpectedConditions.elementToBeClickable(element));
			getActionsObj().moveToElement(element).click().perform();
			log.info("Prforming action click for the locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred in clicking element: " + locator + " (using Selenium Actions)");
		}
	}

	/**
	 * Executes javascript in the context of the currently selected frame or
	 * window.
	 * 
	 * @param script
	 *            in the form of string
	 */
	public void executeJavascript(String script) {
		try {
			JavascriptExecutor jsDriver = (JavascriptExecutor) DriverManager.getWebDriver();
			jsDriver.executeScript(script);
			log.info("Executing Javascript: " + script);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while executing javascript in the context of the currently selected frame or window.");
		}
	}

	/**
	 * 
	 * Close the current window, quitting the browser if there's only one window
	 * available.
	 * 
	 */
	public void closeBrowser() {
		try {
			if (DriverManager.getWebDriver() != null)
				DriverManager.getWebDriver().close();
			log.info("Closing web browser");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to close the current window.");
		}
	}

	/**
	 * Quits the browser, closing every associated window.
	 * 
	 */
	public void quitBrowser() {
		try {
			if (DriverManager.getWebDriver() != null) {
				DriverManager.getWebDriver().quit();
				log.info("Quitting web browser");
			}
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to quit the browser.");
		}
	}

	/**
	 * Returns URL of current window.
	 * 
	 * @return String
	 * 
	 */
	public String getCurrentUrl() {
		String getCurrentUrl = "";
		try {
			getCurrentUrl = DriverManager.getWebDriver().getCurrentUrl();
			log.info("Getting current URL");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to return URL of current window.");
		}
		return getCurrentUrl;
	}

	/**
	 * Refreshes the current page.
	 * 
	 */
	public void pageRefresh() {
		try {
			DriverManager.getWebDriver().navigate().refresh();
			log.info("Refreshing the page");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to refresh the page.");
		}
	}

	/**
	 * Gets source code of the current web page.
	 * 
	 * @return String ::Source code of the current web page
	 */
	public String getPageSource() {
		String source = "";
		try {
			source = DriverManager.getWebDriver().getPageSource();
			log.info("Getting page source");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to get page source.");
		}
		return source;
	}

	/**
	 * Accepts the alert
	 */
	public void acceptAlert() {
		try {
			DriverManager.getWebDriver().switchTo().alert().accept();
			log.info("Accepting alert");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to accept alert.");
		}
	}

	/**
	 * Dismiss the alert.
	 * 
	 */
	public void dismissAlert() {
		try {
			DriverManager.getWebDriver().switchTo().alert().dismiss();
			log.info("Dismissing alert");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to dismiss alert.");
		}
	}

	/**
	 * Checks if an browser alert is present or not.Returns true, if any browser
	 * alert is present.Returns false, if no browser alert is present.
	 * 
	 * @return boolean
	 */
	public boolean isAlertPresent() {
		boolean flag = false;
		Alert alert = null;
		try {
			alert = DriverManager.getWebDriver().switchTo().alert();
			log.info("Verifying if the alert is present or not");
			if (alert != null) {
				flag = true;
			}
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * Returns page title.
	 * 
	 * @return page title.
	 */
	public String getWindowTitle() {
		String getWindowTitle = "";
		try {
			getWindowTitle = DriverManager.getWebDriver().getTitle();
			log.info("Getting window title");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to get the title of current browser window.");
		}
		return getWindowTitle;
	}

	/**
	 * Performs mouse right click on the given element.
	 * 
	 * @param locator
	 */
	public void contextClick(By locator) {
		WebElement element;
		try {
			element = findElement(locator);
			WebDriverWait wait = getWebDriverWait();
			wait.until(ExpectedConditions.elementToBeClickable(element));
			getActionsObj().contextClick(element).build().perform();
			log.info("Performing context click on: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred in right clicking element: " + locator + " (using Selenium Actions)");
		}
	}

	/**
	 * Performs a mouse click on the given element using javascript.
	 * 
	 * @param locator
	 */
	public void javascriptClick(By locator) {
		WebElement element;
		try {
			element = findElement(locator);
			WebDriverWait wait = getWebDriverWait();
			wait.until(ExpectedConditions.elementToBeClickable(element));
			JavascriptExecutor jsDriver = (JavascriptExecutor) DriverManager.getWebDriver();
			jsDriver.executeScript("arguments[0].click();", element);
			log.info("Performing Java script click on: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred in clicking element: " + locator + " (using javaScript)");
		}
	}

	/**
	 * An expectation to check if any browser alert is present and return.
	 */
	public Alert getAlert() {
		Alert alert = null;
		try {
			alert = DriverManager.getWebDriver().switchTo().alert();
			log.info("Getting alert");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while checking if any browser alert is present.");
		}
		return alert;
	}

	/**
	 * Sets the text to the input alert.
	 * 
	 * @param value
	 */
	public void setAlertText(String value) {
		try {
			if (isAlertPresent()) {
				getAlert().sendKeys(value);
				log.info("Setting alert text: " + value);
			}
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to set text in alert input box.");
		}
	}

	/**
	 * Gets the text of the alert.
	 * 
	 * @return String alert text
	 */
	public String getAlertText() {
		String getalerttext = "";
		try {
			if (isAlertPresent())
				getalerttext = getAlert().getText();
			log.info("Getting alert text: " + getalerttext);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to get alert text.");
		}
		return getalerttext;
	}

	/**
	 * Gets the current window handle, which is an opaque handle to this window
	 * that uniquely identifies it within this driver instance.
	 * 
	 * @return String
	 */
	public String getCurrentWindowHandle() {
		String getCurrentWindowHandles = "";
		try {
			getCurrentWindowHandles = DriverManager.getWebDriver().getWindowHandle();
			log.info("Getting current window handle");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to get the current window handle.");
		}
		return getCurrentWindowHandles;
	}

	/**
	 * Gets the window handles of open browser windows.
	 * 
	 * @return Set<String>
	 */
	public Set<String> getWindowHandles() {
		Set<String> getWindowHandles = null;
		try {
			getWindowHandles = DriverManager.getWebDriver().getWindowHandles();
			log.info("Getting current window handles");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to get all the window handles.");
		}
		return getWindowHandles;
	}

	/**
	 * Returns the number of open browser windows.
	 * 
	 * @return int
	 */
	public int getWindowHandlesCount() {
		int getWindowHandlesCount = 0;
		try {
			getWindowHandlesCount = DriverManager.getWebDriver().getWindowHandles().size();
			log.info("Getting current window handles count");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to get the number of window handles.");
		}
		return getWindowHandlesCount;
	}

	/**
	 * Determines whether pop-up window exist in open browser windows.
	 * 
	 * @param parentWindowHandle
	 * @return boolean
	 */
	public boolean isPopupWindowExist(String parentWindowHandle) {
		boolean result = false;
		try {
			for (String handle : DriverManager.getWebDriver().getWindowHandles()) {
				if (handle == parentWindowHandle)
					result = true;
			}
			log.info("Verifying if the popup window: " + parentWindowHandle + " exist");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to check if any pop-up window exists.");
		}
		return result;
	}

	/**
	 * Determines whether the page title is equals to specified value.
	 * 
	 * @param pageTitle
	 * @return boolean
	 */
	public boolean isWindowTitle(String pageTitle) {
		boolean isWindowTitle = false;
		try {
			isWindowTitle = (pageTitle.trim() == DriverManager.getWebDriver().getTitle().trim()) ? true : false;
			log.info("Verifying window title: " + pageTitle + " present or not");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to determines whether the page title is equals to:" + pageTitle);
		}
		return isWindowTitle;
	}

	/**
	 * Scrolls the document to bottom position.
	 */
	public void scrollToBottom() {
		try {
			executeJavascript("window.scrollTo(0, document.body.scrollHeight)");
			log.info("Scrolling to buttom");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while trying to scroll to bottom.");
		}
	}

	/**
	 * Scrolls the document by the specified number of pixels.
	 * 
	 * @param xnum
	 * @param ynum
	 */
	public void scrollToScreenCoordinates(int xnum, int ynum) {
		try {
			executeJavascript("window.scrollBy(" + xnum + "," + ynum + ")");
			log.info("Scrolling to screen coordinates: x: " + xnum + " y:" + ynum);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to scroll the document by the specified number of pixels. x:y :: "
							+ xnum + ":" + ynum);
		}
	}

	/**
	 * Scrolls the document to top position.
	 */
	public void scrollToTop() {
		try {
			executeJavascript("window.scrollTo(0, 0)");
			log.info("Scrollin to top");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to scroll the document to top position ");
		}
	}

	/**
	 * An expectation to select either the first frame on the page or the main
	 * document when a page contains iFrames.
	 */
	public void switchToDefaultFrame() {
		try {
			DriverManager.getWebDriver().switchTo().defaultContent();
			log.info("Switching to default frame");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to select either the first frame on the page or the main document when a page contains iFrames.");
		}
	}

	/**
	 * An expectation to select a frame using its previously located
	 * 
	 * @param element
	 */
	public void switchToFrameByElement(WebElement element) {
		try {
			DriverManager.getWebDriver().switchTo().frame(element);
			log.info("Switching to frame by element: " + element);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to select a frame using its previously located web element: "
							+ element);
		}
	}

	/**
	 * An expectation to select a frame by its (zero-based) index.
	 * 
	 * @param frameIndex
	 */
	public void switchToFrameByIndex(int frameIndex) {
		try {
			DriverManager.getWebDriver().switchTo().frame(frameIndex);
			log.info("Switching to frame by frameIndex: " + frameIndex);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to select a frame by index:" + frameIndex);
		}
	}

	/**
	 * An expectation to select a frame by its name.
	 * 
	 * @param frameName
	 */
	public void switchToFrameByName(String frameName) {
		try {
			DriverManager.getWebDriver().switchTo().frame(frameName);
			log.info("Switching to frame by name: " + frameName);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to select a frame by its name:" + frameName);
		}
	}

	/**
	 * An expectation to select the parent frame of the currently selected
	 * frame.
	 */
	public void switchToParentFrame() {
		try {
			DriverManager.getWebDriver().switchTo().parentFrame();
			log.info("Switching to parent frame");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to select the parent frame of the currently selected frame.");
		}
	}

	/**
	 * Switches the focus of future commands for this driver to the window with
	 * the given page title.
	 * 
	 * @param title
	 * @return boolean
	 */
	// TODO changes
	public boolean switchToWindowByTitle(String title) {
		boolean result = false;
		try {
			for (String window : DriverManager.getWebDriver().getWindowHandles()) {
				DriverManager.getWebDriver().switchTo().window(window);
				if (DriverManager.getWebDriver().getTitle().trim().equals(title)) {
					result = true;
					break;
				}
			}
			log.info("Switching to window by title: " + title);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to Switch to window with title:" + title);
		}
		return result;
	}

	/**
	 * It will open a new blank tab
	 */
	public void openNewTab() {
		try {
			executeJavascript("window.open('','','');");
			log.info("Opening new tab");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occured while opening new tab");
		}
	}

	/**
	 * It will open a new blank browser window
	 */
	public void openNewBrowserWindow() {
		try {
			executeJavascript("window.open('', '', ',',)");
			log.info("Opening new browser window");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occured while opening the new browser window");
		}
	}

	/**
	 * Switches the focus of future commands for this driver to the first or
	 * default window handles of the open browser windows.
	 * 
	 * @return boolean
	 */
	public void switchToFirstWindow() {
		try {
			Set<String> allWindows = DriverManager.getWebDriver().getWindowHandles();
			for (String curWindow : allWindows) {
				DriverManager.getWebDriver().switchTo().window(curWindow);
				break;
			}
			log.info("Switching to first window");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to Switch the focus of future commands for this driver to the first window handles of the open browser windows.");
		}
	}

	/**
	 * Switches the focus of future commands for this driver window handles of
	 * the open browser windows based on index.
	 * 
	 * @param index
	 * @return
	 */
	public void switchToWindowByIndex(int index) {
		try {
			Set<String> set = DriverManager.getWebDriver().getWindowHandles();
			List<String> allWindows = set.stream().collect(Collectors.toList());
			String curWindow = allWindows.get(index);
			DriverManager.getWebDriver().switchTo().window(curWindow);
			log.info("Switching to window by index: " + index);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to Switch the focus of future commands for this driver to " + index
							+ " window handles of the open browser windows.");
		}
	}

	/**
	 * Gets the window handles of open browser windows based on index.
	 * 
	 * @param index
	 * @return
	 */
	public String getWindowByIndex(int index) {
		String window = null;
		try {
			Set<String> set = DriverManager.getWebDriver().getWindowHandles();
			List<String> allWindows = set.stream().collect(Collectors.toList());
			window = allWindows.get(index);
			log.info("Getting window ny index: " + index);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occurred while trying to Switch the focus of future commands for this driver to " + index
							+ " window handles of the open browser windows.");
		}
		return window;
	}

	/**
	 * Method that navigate window back
	 */
	public void navigateBack() {
		try {
			DriverManager.getWebDriver().navigate().back();
			log.info("Navigating back");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while navigateing window back");
		}
	}

	/**
	 * Method that navigate window forward
	 */
	public void navigateForward() {
		try {
			DriverManager.getWebDriver().navigate().forward();
			log.info("Navigating forward");
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occurred while navigateing window forward");
		}
	}

	/**
	 * Method to select date from date picker
	 * 
	 * @param locator,date
	 * 
	 */
	public void selectCustomDateInPicker(By locator, String date) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM/dd/yyyy");
			LocalDate localDate = LocalDate.parse(date, formatter);

			String year = ("" + localDate.getYear()).trim();
			String month = ("" + localDate.getMonthValue()).trim();
			String day = ("" + localDate.getDayOfMonth()).trim();

			By lst_month = By.xpath("//*[@id='ui-datepicker-div']/div/div/select[1]");
			By lst_year = By.xpath("//*[@id='ui-datepicker-div']/div/div/select[2]");
			By lnk_day = By.xpath("//*[@id='ui-datepicker-div']/table/tbody/tr/td/a[text()='" + day + "']");

			System.out.println("Year -->" + year + " Month--->" + month + " Day--->" + day);
			// Check for waits if required
			click(locator);
			selectByVisibleText(lst_month, month);
			selectByVisibleText(lst_year, year);
			click(lnk_day);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error when selecting date" + locator);
		}

	}

	/**
	 * Method to select today or highlighted default date in picker
	 * 
	 * @param locator
	 */
	public void selectCurrentDateInPicker(By locator) {
		By selectedDate = By.xpath("//*[@id='ui-datepicker-div']/table/tbody/tr/td/a[contains(@class, 'highlight')]");
		try {
			click(locator);
			// Add wait if required
			click(selectedDate);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Error when selecting current date "
							+ DriverManager.getWebDriver().findElement(selectedDate).getText()
							+ " from angular date picker" + locator);
		}
	}

	/**
	 * Method to fetch selected option from dropdown
	 * 
	 * @param locator
	 * @return selected option
	 */
	public String getSelectedOption(By locator) {
		Select sel = null;
		try {
			scrollToElement(locator);
			sel = new Select(DriverManager.getWebDriver().findElement(locator));
			log.info("Getting selected options by locator: " + locator);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Error in selecting from dropdown " + locator);
		}
		return sel.getFirstSelectedOption().getText().trim();
	}

}

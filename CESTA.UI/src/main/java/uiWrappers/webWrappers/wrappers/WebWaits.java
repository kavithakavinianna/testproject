package uiWrappers.webWrappers.wrappers;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.google.common.base.Function;
import configuration.driverManager.DriverManager;
import exceptionHandler.CustomExceptionHandler;
import loggers.interfaces.ILogger;
import reporters.LogType;
import reporters.interfaces.IReporter;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;
import uiWrappers.WaitType;
import uiWrappers.webWrappers.interfaces.IWebWaits;

public class WebWaits implements IWebWaits {

	private TestEnvironment testEnvironment;
	WebActions webActions;
	private ILogger log;
	private IReporter report;

	public WebWaits() {
		testEnvironment = Singleton.getInstance(TestEnvironment.class);
		webActions = Singleton.getInstance(WebActions.class);
		log = Singleton.getInstance(ILogger.class);
		report = Singleton.getInstance(IReporter.class);
	}

	/**
	 * Method to get Wait type
	 * 
	 * @param waitType 
	 *            (SmallWait, MediumWait, LargeWait, ImplicitWaitTime, PageLoad,
	 *            Explicit)
	 * @return waitTime wait time based on WaitType
	 */
	public int getWait(WaitType waitType) {
		int waitTime = 0;

		try {
			switch (waitType) {
			case Small:
				waitTime = testEnvironment.SmallWait;
				break;
			case Medium:
				waitTime = testEnvironment.MediumWait;
				break;
			case Large:
				waitTime = testEnvironment.LargeWait;
				break;
			case Implicit:
				waitTime = testEnvironment.ImplicitWaitTimeout;
				break;
			case Explicit:
				waitTime = testEnvironment.ExplicitWaitTimeout;
				break;
			case PageLoad:
				waitTime = testEnvironment.PageLoadTimeout;
				break;
			default:
				throw new IllegalArgumentException("Undefined Wait Type.");
			}
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Failed While getting wait type" + waitType);
		}
		return waitTime;
	}

	/**
	 * An expectation to wait and check an element is visible and enabled such
	 * that it can be clicked.
	 * 
	 * @param locator
	 *            :: Locator for the web element
	 * @param waitType
	 *            :: Type of wait from WaitType
	 */
	public void waitForElementToBeClickable(By locator, WaitType waitType) {
		try {
			int time = getWait(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Failed Waiting For Element ::" + locator + "  To Be Clickable");
		}
	}
	
	/**
	 * An expectation for checking that an element is present on the DOM of a
	 * page. This does not necessarily mean that the element is visible or
	 * enabled.
	 * 
	 * @param locator
	 *            :: Locator for the web element
	 * @param waitType
	 *            :: Type of wait from WaitType
	 */
	public void waitForElementToBePresent(By locator, WaitType waitType) {
		try {
			int time = getWait(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Failed Waiting For Element ::" + locator + "  To Be Present");
		}
	}

	/**
	 * An expectation for checking that an element is present on the DOM of a
	 * page and visible.
	 * 
	 * @param locator
	 *            :: Locator for the web element
	 * @param waitType
	 *            :: Type of wait from WaitType
	 */
	public void waitForElementToBeVisible(By locator, WaitType waitType) {
		try {
			int time = getWait(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Failed Waiting For Element ::" + locator + "  To Be Visible");
		}
	}
	
	/**
	 * An expectation to wait and check an element is either invisible or not
	 * present in the DOM.
	 * 
	 * @param locator
	 *            :: Locator for the web element
	 * @param waitType
	 *            :: Type of wait from WaitType
	 */
	public void waitForElementToBeInvisible(By locator, WaitType waitType) {
		boolean result = false;
		try {
			int time = getWait(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
			result = wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
			if (!result) {
				throw new TimeoutException();
			}
		} catch (TimeoutException toe) {
			CustomExceptionHandler.ExceptionHandler(toe, "Timeout Exception :: " + toe.getMessage()
					+ " while waiting For Element :: " + locator + "  To Be Invisible");
		} catch (Exception e) {
			Assert.fail("Failed Waiting For Element ::" + locator + "  To Be Invisible");
		}
	}

	/**
	 * Reusable method to make the driver to wait until the document loads
	 * completely within given maximum wait time
	 * 
	 * @param maxWaitTimeInSec
	 */
	public void waitForBrowserLoad(int maxWaitTimeInSec) {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};
		try {
			Thread.sleep(2000);
			WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), maxWaitTimeInSec);
			wait.until(expectation);
		} catch (Throwable error) {
			Assert.fail("Timeout waiting for Page Load Request to complete.");
		}
	}

	/**
	 * Method to wait for page to load
	 */
	public void waitForPageLoad() {
		try {
			DriverManager.getWebDriver().manage().timeouts().pageLoadTimeout(getWait(WaitType.PageLoad),
					TimeUnit.SECONDS);
		} catch (NumberFormatException e) {
			Assert.fail("Exception occured on waiting For Page To Load and the exception is " + e.getMessage()
					+ " and the stack trace is:" + e.getStackTrace());
		}
	}

	/**
	 * An expectation for wait and check whether the given frame is available to
	 * switch to. If the frame is available it switches the driver to the
	 * specified frame.
	 * 
	 * @param locator
	 * @param waitType
	 */
	public void waitAndSwitchToFrame(By locator, WaitType waitType) {
		try {
			int time = getWait(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(locator));
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Timeout exception while waiting for frame: " + locator);
		}
	}

	/**
	 * Reusable method to make the driver to wait for document to load
	 * completely within 120 Seconds.
	 * 
	 * @param maxWaitTimeInSec
	 */
	public void waitForBrowserLoad() {
		int maxWaitTimeInSec = Integer.valueOf(getWait(WaitType.PageLoad));
		waitForBrowserLoad(maxWaitTimeInSec);
	}

	/**
	 * Fluent wait for locator to be enabled polls page every second for time
	 * set in minutes(timeout)
	 * 
	 * @param element(getter)
	 */
	@SuppressWarnings("deprecation")
	public void fluentWaitIsEnabledInSec(By locator) {
		try {
			new FluentWait<WebDriver>(DriverManager.getWebDriver())
					.withTimeout(getWait(WaitType.Explicit), TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class)
					.until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver d) {
							return DriverManager.getWebDriver().findElement(locator).isEnabled();
						}
					});
		} catch (TimeoutException te) {
			Assert.fail("Element :" + locator + " is not enabled within -" + getWait(WaitType.Explicit) + " seconds");
		}
	}

	/**
	 * Fluent wait for WebElement to be enabled polls page every Second for time
	 * set in minutes(timeout)
	 * 
	 * @param element(getter)
	 * @param timeoutInMin
	 */
	@SuppressWarnings("deprecation")
	public void fluentWaitIsEnabledInSec(By locator, int timeoutInMin) {
		try {
			new FluentWait<WebDriver>(DriverManager.getWebDriver()).withTimeout(timeoutInMin, TimeUnit.SECONDS)
					.pollingEvery(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
					.ignoring(StaleElementReferenceException.class).until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver d) {
							return DriverManager.getWebDriver().findElement(locator).isEnabled();
						}
					});
		} catch (TimeoutException te) {
			Assert.fail("Element :" + locator + " is not enabled within -" + getWait(WaitType.Explicit) + " seconds");
		}
	}

	/**
	 * Fluent wait for WebElement to be displayed polls page every second for
	 * time set in minutes(timeout)
	 * 
	 * @param element(getter)
	 * @param timeoutInMin
	 */
	@SuppressWarnings("deprecation")
	public void fluentWaitIsDisplayedInSec(By locator) {
		try {
			new FluentWait<WebDriver>(DriverManager.getWebDriver())
					.withTimeout(getWait(WaitType.Explicit), TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class)
					.until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver d) {
							return DriverManager.getWebDriver().findElement(locator).isDisplayed();
						}
					});
		} catch (TimeoutException te) {
			Assert.fail(
					"Element : " + locator + " is not displayed within -" + getWait(WaitType.Explicit) + " seconds");
		}
	}

	/**
	 * Fluent wait for WebElement to be displayed polls page every second for
	 * time set in seconds(timeout)
	 * 
	 * @param locator,
	 *            element id, xpath, cssSelector, name, class etc..
	 * @param timeoutInSec
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public void fluentWaitIsDisplayedInSec(By locator, int timeoutInSec) {
		try {
			new FluentWait<WebDriver>(DriverManager.getWebDriver()).withTimeout(timeoutInSec, TimeUnit.SECONDS)
					.pollingEvery(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
					.ignoring(StaleElementReferenceException.class).until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver d) {
							WebElement element = d.findElement(locator);
							return element.isDisplayed();
						}
					});
		} catch (TimeoutException te) {
			Assert.fail(
					"Element : " + locator + " is not displayed within -" + getWait(WaitType.Explicit) + " seconds");
		}
	}

	/**
	 * FluentWait to wait for element to be displayed. polls page every second
	 * for time set in minutes(timeout)
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public void fluentWaitTextIsDisplayedInSec(By locator, String text) {
		final String txt = text;
		try {
			new FluentWait<WebDriver>(DriverManager.getWebDriver())
					.withTimeout(getWait(WaitType.Explicit), TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class)
					.until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver d) {
							WebElement element = DriverManager.getWebDriver().findElement(locator);
							ExpectedConditions.textToBePresentInElement(element, txt);
							return element.isDisplayed();
						}
					});
		} catch (TimeoutException te) {
			Assert.fail("Text - " + text + " for the Element : " + locator + " is not displayed within -"
					+ getWait(WaitType.Explicit) + " seconds");
		}
	}

	/**
	 * FluentWait to wait for element to be disabled. polls page every second
	 * for time set in minutes(timeout)
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public void fluentWaitTextIsDisplayedInSec(By locator, String text, int timeoutInMin) {
		final String txt = text;
		try {
			new FluentWait<WebDriver>(DriverManager.getWebDriver()).withTimeout(timeoutInMin, TimeUnit.SECONDS)
					.pollingEvery(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
					.ignoring(StaleElementReferenceException.class).until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver d) {
							WebElement element = DriverManager.getWebDriver().findElement(locator);
							ExpectedConditions.textToBePresentInElement(element, txt);
							return element.isDisplayed();
						}
					});
		} catch (TimeoutException te) {
			Assert.fail("Text - " + text + " for the Element : " + locator + " is not displayed within -"
					+ getWait(WaitType.Explicit) + " seconds");
		}
	}

	/**
	 * FluentWait to wait for element to be disabled. polls page every second
	 * for time set in minutes(timeout)
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	public void fluentWaitIsDisabledInSec(By locator) {
		try {
			new FluentWait<WebDriver>(DriverManager.getWebDriver())
					.withTimeout(getWait(WaitType.Explicit), TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
					.ignoring(NoSuchElementException.class).ignoring(StaleElementReferenceException.class)
					.until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver d) {
							WebElement element = d.findElement(locator);
							return !element.isEnabled();
						}
					});
		} catch (TimeoutException te) {
			Assert.fail("Element : " + locator + " is not disabled within -" + getWait(WaitType.Explicit) + " seconds");
		}
	}

	/**
	 * Reusable method to make the driver to wait until the document loads
	 * completely within given maximum wait time
	 * 
	 * @param maxWaitTimeInSec
	 */
	@SuppressWarnings("deprecation")
	public void fluentWaitIsDisabledInSec(By locator, int timeoutInSec) {
		try {
			new FluentWait<WebDriver>(DriverManager.getWebDriver()).withTimeout(timeoutInSec, TimeUnit.SECONDS)
					.pollingEvery(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
					.ignoring(StaleElementReferenceException.class).until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver d) {
							WebElement element = d.findElement(locator);
							return !element.isEnabled();
						}
					});
		} catch (TimeoutException te) {
			Assert.fail("Element : " + locator + " is not disabled within -" + getWait(WaitType.Explicit) + " seconds");
		}
	}

	@SuppressWarnings("deprecation")
	public void waitForBrowserToOpenNewTab(int timeoutInMin) {
		try {
			new FluentWait<WebDriver>(DriverManager.getWebDriver()).withTimeout(timeoutInMin, TimeUnit.MINUTES)
					.pollingEvery(1, TimeUnit.SECONDS).until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver d) {
							ArrayList<String> tabs = new ArrayList<String>(
									DriverManager.getWebDriver().getWindowHandles());
							return tabs.size() > 1;
						}
					});
		} catch (TimeoutException te) {
			Assert.fail("Timeout of -" + getWait(WaitType.PageLoad)
					+ " seconds was reached before the browser open new tab");
		}
	}

	/**
	 * @Description meant to be used to check a file location until the file
	 *              exists or the timeout is reached
	 * @param filePath
	 * @param timeoutInSec
	 */
	@SuppressWarnings("deprecation")
	public void waitForFileToExist(String filePath, int timeoutInSec) {
		try {
			log.info("Waiting For File To Exist at given file path : " + filePath + " and time out :" + timeoutInSec);
			report.logReport(LogType.INFO,
					"Waiting For File To Exist at given file path : " + filePath + " and time out :" + timeoutInSec);
			new FluentWait<WebDriver>(DriverManager.getWebDriver()).withTimeout(timeoutInSec, TimeUnit.SECONDS)
					.pollingEvery(1, TimeUnit.SECONDS).until(new Function<WebDriver, Boolean>() {
						public Boolean apply(WebDriver d) {
							File f = new File(filePath);
							if (f.exists()) {
								System.out.println("File found at " + filePath + ". ");
								return true;
							}
							return false;
						}
					});
		} catch (TimeoutException te) {
			Assert.fail("The timeout of " + timeoutInSec + "s was reached before the file at " + filePath
					+ " could be found.");
		}
	}

	/**
	 * Waits for alert to be present.
	 * 
	 * @param waitType
	 */
	public void waitForAlertToBePresent(WaitType waitType) {
		try {
			int time = getWait(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
			wait.until(ExpectedConditions.alertIsPresent());
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occured while waiting for alert pop-up to be present");
		}
	}

	/**
	 * Waits for the specified timespan and determines whether the page title is
	 * equals to specified value.
	 * 
	 * @param pageTitle
	 * @param waitTime
	 * @return boolean
	 */
	// TODO changes
	public boolean waitAndVerifyPageTitle(String pageTitle, WaitType waitType) {
		boolean waitAndVerifyPageTitle = false;
		int time = getWait(waitType);
		WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
		try {
			waitAndVerifyPageTitle = wait.until(ExpectedConditions.titleContains(pageTitle));
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occured while waiting for page title to be '" + pageTitle + "'.");
		}
		return waitAndVerifyPageTitle;
	}

	/**
	 * An expectation to wait and check the title of a page.
	 * 
	 * @param pageTitle
	 * @param waitTime
	 * @return
	 */
	public boolean waitForPageTitle(String pageTitle, WaitType waitType) {
		boolean result = false;
		int time = getWait(waitType);
		WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
		try {
			log.info("Waiting For Page Title : " + pageTitle + ", in given wait time :" + time);
			report.logReport(LogType.INFO, "Waiting For Page Title : " + pageTitle + ", in given wait time :" + time);
			result = wait.until(ExpectedConditions.titleContains(pageTitle));
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occured while waiting for page title to be '" + pageTitle + "'.");
		}
		return result;
	}

	/**
	 * An expectation to wait and switch to the currently active modal dailog
	 * for this particular driver instance.
	 * 
	 * @param waitTime
	 * @return Alert
	 */
	public Alert waitAndGetAlert(WaitType waitType) {
		Alert alert = null;
		int time = getWait(waitType);
		WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
		try {
			log.info("Waiting for Getting Alert, with in the given time : " + time);
			report.logReport(LogType.INFO, "Waiting for Getting Alert, with in the given time : " + time);
			wait.until(ExpectedConditions.alertIsPresent());
			alert = webActions.getAlert();
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occured while trying to wait and switch to alert.");
		}
		return alert;
	}

	/**
	 * An expectation to wait and check that the given text is present in the
	 * given locator.
	 * 
	 * @param locator
	 * @param text
	 * @param waitType
	 */
	public void waitForTextToBePresentInElement(By locator, String text, WaitType waitType) {
		boolean result = false;
		try {
			int time = getWait(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
			log.info("Waiting For Text : " + text + " To Be Present In Element :" + locator + " , in given time :"
					+ time);
			report.logReport(LogType.INFO, "Waiting For Text : " + text + " To Be Present In Element :" + locator
					+ " , in given time :" + time);
			result = wait.until(ExpectedConditions.textToBePresentInElementLocated(locator, text));
			if (!result)
				throw new TimeoutException();
		} catch (TimeoutException e) {
			String message = String.format(" The text '{0}' is not present in the element '{1}' within given wait time",
					text, locator);
			CustomExceptionHandler.ExceptionHandler(e, message);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occured while waiting for text(" + text
					+ ") to be present in element: " + locator + ", with wait type: " + waitType.toString());
		}
	}

	/**
	 * An expectation to wait and check that the web element with text is either
	 * invisible or not present on the DOM.
	 * 
	 * @param locator
	 * @param text
	 * @param waitType
	 */
	public void waitForElementWithTextToBeInvisible(By locator, String text, WaitType waitType) {
		boolean result = false;
		try {
			int time = getWait(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
			log.info("Waiting For Text : " + text + " To Be Invisible In Element :" + locator + " , in given time :"
					+ time);
			report.logReport(LogType.INFO, "Waiting For Text : " + text + " To Be Invisible In Element :" + locator
					+ " , in given time :" + time);
			result = wait.until(ExpectedConditions.invisibilityOfElementWithText(locator, text));
			if (!result)
				throw new TimeoutException();
		} catch (TimeoutException e) {
			String message = String.format(
					" Failed due to the text '{0}' in the element '{1}' is not disappeared in given wait time", text,
					locator);
			CustomExceptionHandler.ExceptionHandler(e, message);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occured while waiting for element(" + locator
					+ ") with text:" + text + " to be invisible, with wait type: " + waitType.toString());
		}
	}

	/**
	 * Wait until element attribute to be present
	 * 
	 * @param locator
	 * @param attributeName
	 * @param waitType
	 */
	public void waitForElementAttributeToBePresent(By locator, String attributeName, WaitType waitType) {
		try {
			int time = getWait(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getWebDriver(), time);
			log.info("waiting For Element Attribute : " + attributeName + " To Be Present at locator : " + locator
					+ " for given time :" + time);
			report.logReport(LogType.INFO, "waiting For Element Attribute : " + attributeName
					+ " To Be Present at locator : " + locator + " for given time :" + time);
			wait.until(ExpectedConditions.attributeToBeNotEmpty(webActions.findElement(locator), attributeName));
		} catch (Exception e) {
			String message = String.format(" Locator : '{0}' and trying to get attribute '{1}'", locator,
					attributeName);
			CustomExceptionHandler.ExceptionHandler(e, message);
		}
	}

	/**
	 * An expectation to wait for ajax load to be completed.
	 */
	public void waitForAjaxLoad() {
		try {
			while (true) // Handle timeout somewhere
			{
				boolean ajaxIsComplete = (boolean) ((JavascriptExecutor) DriverManager.getWebDriver())
						.executeScript("return jQuery.active == 0");
				if (ajaxIsComplete)
					break;
			}
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occured while waiting for ajax load.");
		}
	}

	/**
	 * An expectation to wait for network calls to be finished.
	 */
	public void waitForNetworkCalls() {
		String script = "source:(" + "function(){" + "var send = XMLHttpRequest.prototype.send;"
				+ "var release = function(){ --XMLHttpRequest.active };"
				+ "var onloadend = function(){ setTimeout(release, 1) };" + "XMLHttpRequest.active = 0;"
				+ "XMLHttpRequest.prototype.send = function() {" + "++XMLHttpRequest.active;"
				+ "this.addEventListener('loadend', onloadend, true);" + "send.apply(this, arguments);" + "};})();";

		((JavascriptExecutor) DriverManager.getWebDriver()).executeScript(script);
		try {
			while (true) // Handle timeout somewhere
			{
				boolean networkCallsComplete = (boolean) ((JavascriptExecutor) DriverManager.getWebDriver())
						.executeScript(script);
				if (networkCallsComplete)
					break;
			}
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception occured while waiting for network calls to complete.");
		}
	}
}

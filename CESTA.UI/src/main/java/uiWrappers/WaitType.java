package uiWrappers;

public enum WaitType {
	Small(0), Medium(1), Large(2), Implicit(3), Explicit(4), PageLoad(5);

	private int waitType;

	WaitType(int waitType) {
		this.waitType = waitType;
	}

	public int getWaitType() {
		return this.waitType;
	}
}

package uiWrappers.mobileWrappers.interfaces;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import io.appium.java_client.MobileElement;

public interface IMobileActions {

	/**
	 * click method help for tapping the mobile element.
	 * 
	 * @param locator:
	 *            a By locator of the mobile element.
	 */
	void click(By locator);

	/**
	 * Method to send the text by providing "content-desc" as id value.
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * @param text
	 *            : Text to be inserted in the element.
	 */
	void sendKeys(By locator, String text);

	/**
	 * Method to long press by providing "content-desc" as id value.
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 */
	void longPress(By locator);

	/**
	 * Method to long press by providing "content-desc" as id value.
	 * 
	 */
	void flickWithCoordinates(int startx, int starty, int endx, int endy);

	/**
	 * Method to perform scroll operation by providing exact text to click
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * @param text
	 *            : Text to be inserted in the element.
	 */
	void scrollByText(By locator, String text);

	/**
	 * Method to drag by fetching the current location as co-ordinates to given
	 * element
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * @param xOffset
	 *            : an Integer value for endpoint x-coordinate
	 * @param yOffset
	 *            : an Integer value for endpoint y-coordinate
	 */
	void drag(By locator, int xOffset, int yOffset);

	/**
	 * Method to click on android home button
	 */
	void clickHomeButton();

	/**
	 * 
	 * Method to perform zoom action
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 */
	void Zoom(By locator);

	/**
	 * This method will get the title
	 */
	String getTitle();

	/**
	 * Method to perform a shake action on the device
	 */
	void Shake();

	/**
	 * This method helps to find web element element by IosUIAutomation
	 * 
	 * @param selector
	 *            : finds a IOS locator.
	 */
	WebElement findElementByIosAutomation(String selector);

	/**
	 * To simulate Touch ID on iOS Simulators.
	 * 
	 * @param match
	 *            : Set the desired capability "allowTouchIdEnroll" to true
	 */
	void PerformTouchId(boolean match);

	/**
	 * This method helps to find mobile element by IosUIAutomation
	 * 
	 * @param selector
	 *            : finds a IOS locator.
	 */
	WebElement findElementByIosUIAutomation(String selector);

	/**
	 * Method to get the contexts available for the mobile App
	 * 
	 * @return contextNames
	 */
	Set<String> getAppContexts();

	/**
	 * Method to find mobile element
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 *  @param throwException
	 *            : true or false based on requirement to throw exception.          
	 * @return element
	 */
	MobileElement findElement(By locator, boolean... throwException);

	/**
	 * Method to find mobile elements
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * @return elements
	 */
	List<MobileElement> findElements(By locator);

	/**
	 * Gets the value of the specified attribute within given locator
	 * 
	 * @param locator:
	 *            By locator will passed from project level
	 */

	String getAttributeValue(By locator, String attributeName);

	/**
	 * Gets the innerText of the given locator
	 * 
	 * @param locator:
	 *            By locator will passed from project level
	 */

	String getText(By locator);

	/**
	 * Gets a value indicating whether or not this element is enabled
	 * 
	 * @param locator:
	 *            By locator will passed from project level
	 */

	boolean isEnabled(By locator);
	/**
	 * Method to long  press by providing "content-desc" as id value
	 * @param locator
	 */
	void longClick(By locator);
	/**
	 * Method to swipe by providing the co-ordinates from one point to another point
	 * @param startx
	 * @param starty
	 * @param endx
	 * @param endy
	 */
	void swipeWithCoordinates(int startx, int starty, int endx, int endy);
	/**
	 * Executes javascript in the context of the currently selected frame or
	 * screen.
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 */
	void executeJavascript(String script, Object[] args);
	/**
	 * Method to which to desired view in the pass by passing the view
	 * 
	 * @param contextName
	 */
	void switchToAppContext(String contextName);
	/**
	 * Method to switch to webview of the mobile app
	 */
	void switchtoWebView();
	/**
	 * Method to switch to native view of the mobile app
	 */
	void switchtoNativeView();
	/**
	 * Method to move/scroll to provided element on UI
	 * 
	 * @param locator
	 */
	void moveToElement(By locator);
	/**
	 * clear method help for clearing text from mobile field element.
	 * 
	 * @param locator:
	 *            a By locator of the mobile element.
	 */
	void clear(By locator);
	
	/**
	 * hide keyboard method help for clearing text from mobile field element.
	 */	
	void hideKeyBoard();
	
	void refreshApp();
	
	void tapByElement(By locator);
	
	void multiTouchByElement(By Locator);
	
	void tapByCoordinates(int x, int y);
}

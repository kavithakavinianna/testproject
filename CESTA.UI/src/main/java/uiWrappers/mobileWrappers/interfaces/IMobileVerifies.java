package uiWrappers.mobileWrappers.interfaces;

import org.openqa.selenium.By;

public interface IMobileVerifies {

	void elementIsPresent(By by);

	void elementIsNotPresent(By by);

	void textboxIsEmpty(By by);

	void elementContainsText(By by, String text);

	void elementDoesNotContainsText(By by, String text);

	void exactTextInElementIs(By by, String text);

	void exactTextInElementIsNot(By by, String text);

	void elementIsEnabled(By by);

	void elementIsDisabled(By by);

	void elementIsSelected(By by);

	void elementIsNotSelected(By by);

	void exactPageTitle(String ExpectedPageTitle);

	void partialPageTitle(String PartialPageTitle);
	
	boolean isAlertPresent();

}
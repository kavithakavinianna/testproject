package uiWrappers.mobileWrappers.interfaces;

import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedCondition;

import io.appium.java_client.MobileElement;
import uiWrappers.WaitType;

public interface IMobileWaits {

	/**
	 * Set the amount of time the driver should wait when searching for elements
	 */
	void setImplicitWaitTimeOut();

	/**
	 * Configure the amount of time that a particular type of operation can execute
	 * for before they are aborted * @waitType:
	 * waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed from project
	 * level
	 */
	void setPageLoadTimeout(WaitType waitType);

	/**
	 * Set the amount of time, in milliseconds, that scripts execution are permitted
	 * to run before they are aborted (Web context only)
	 * 
	 * @waitType: waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed
	 *            from project level
	 */

	void setScriptTimeout(WaitType waitType);

	/**
	 * An expectation to wait and check for attribute to be present in current
	 * mobile element. * @locator: By locator has to be passed from project level
	 * * @attributeName: String value the element attribute name has to be passed
	 * from project level * @waitType: waitType(Small,Medium,Large,ImplicitWaitTime
	 * ) has to be passed from project level
	 */

	void waitToGetAttribute(By locator, String attributeName, WaitType waitType);

	/**
	 * An expectation to wait and check that the mobile element with text is either
	 * invisible or not present on the DOM. * @locator: By locator has to be passed
	 * from project level * @text: String value of text must be passed from project
	 * level * @waitType: waitType(Small,Medium,Large,ImplicitWaitTime ) has to be
	 * passed from project level
	 */

	void waitForInvisibilityOfMobileElementWithText(By locator, String text, WaitType waitType);

	/**
	 * An expectation to wait and check that the given text is present in the given
	 * locator. * @locator: By locator has to be passed from project level * @text:
	 * String value of text must be passed from project level * @waitType:
	 * waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed from project
	 * level
	 */
	void WaitForTextToBePresentInMobileElement(By locator, String text, WaitType waitType);

	void waitForAlertIsPresent(WaitType waitType);


	/**
	 * An expectation to wait and verify if attribute contains value
	 * @waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed from project
	 * @attribute: Pass Attribute type from project level
	 * @value: Pass Attribute value type from project level
	 * @locator: By locator has to be passed from project level 
	 */

	void waitIfAttributeContains(By locator, String attribute, String value, WaitType waitType);

	/**
	 * An expectation to wait and verify if attribute contains value
	 * @waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed from project
	 * @attribute: Pass Attribute type from project level
	 * @value: Pass Attribute value type from project level
	 * @element: mobileElement has to be passed from project level 
	 */
	void waitIfAttributeContains(MobileElement element, String attribute, String value, WaitType waitType);

	/**
	 * An expectation to get attribute value
	 * @waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed from project
	 * @attribute: Pass Attribute type from project level
	 * @value: Pass Attribute value type from project level
	 * @element: mobileElement has to be passed from project level 
	 */


	void waitToGetAttribute(MobileElement element, String attribute, String value, WaitType waitType);



	void waitForAttributeToBeNotEmpty(MobileElement element, String attribute, WaitType waitType);

	void waitForAttributeToBeNotEmpty(By locator, Boolean selected, WaitType waitType);

	void waitForMobileElementSelectionStateToBe(By locator, Boolean selected, WaitType waitType);

	void waitForMobileElementSelectionStateToBe(MobileElement element, Boolean selected, WaitType waitType);

	void waitForMobileElementToBeClickable(By locator, WaitType waitType);

	void waitForMobileElementToBeClickable(MobileElement element, WaitType waitType);

	void waitForMobileElementToBeSelected(By locator, WaitType waitType);

	void waitForMobileElementToBeSelected(MobileElement element, WaitType waitType);

	void waitForFrameToBeAvailableAndSwitchToIt(By locator, WaitType waitType);

	void waitForFrameToBeAvailableAndSwitchToIt(int frameLocator, WaitType waitType);

	void waitForFrameToBeAvailableAndSwitchToIt(String frameLocator, WaitType waitType);

	void waitForFrameToBeAvailableAndSwitchToIt(MobileElement frameLocator, WaitType waitType);

	void waitForInvisibilityOfMobileElement(MobileElement element, WaitType waitType);

	void waitForInvisibilityOfMobileElement(List<MobileElement> elements, WaitType waitType);

	void waitForInvisibilityOfMobileElementLocated(By locator, WaitType waitType);

	void waitForMobileElementSelectionStateToBe(By locator, boolean selected, WaitType waitType);

	void waitForMobileElementSelectionStateToBe(MobileElement element, boolean selected, WaitType waitType);

	void waitForNumberOfElementsToBe(By locator, int number, WaitType waitType);

	void waitForNumberOfElementsToBeLessThan(By locator, int number, WaitType waitType);

	void waitForNumberOfElementsToBeMoreThan(By locator, int number, WaitType waitType);

	void waitForNumberOfElementsToBeMoreThan(int expectedNumberOfWindows, WaitType waitType);

	void waitForTextToBePresentInMobileElement(MobileElement element, String text, WaitType waitType);

	void waitForTextToBePresentInMobileElementValue(By locator, String text, WaitType waitType);

	void waitForTextToBePresentInMobileElementValue(MobileElement element, String text, WaitType waitType);

	void waitForTextToBe(By locator, String value, WaitType waitType);

	void waitForTextToBePresentInMobileElementLocated(By locator, String text, WaitType waitType);

	void waitForTitleContains(String title, WaitType waitType);

	void waitFortitleIsExactMatch(String title, WaitType waitType);

	void waitIfUrlContains(String fraction, WaitType waitType);

	void waitIfUrlMatches(String regex, WaitType waitType);

	void waitIfUrlToBe(String url, WaitType waitType);

	void waitForVisibilityOfMobileElement(MobileElement element, WaitType waitType);

	void waitForVisibilityOfMobileElements(List<MobileElement> elements, WaitType waitType);

	void waitForVisibilityOfAllMobileElementsLocatedBy(By locator, WaitType waitType);

	void waitForVisibilityOfMobileElementLocated(By locator, WaitType waitType);

	void waitForVisibilityOfMobileElementLocated(By parentLocator, By childLocator, WaitType waitType);

	void waitForVisibilityOfNestedMobileElementLocatedBy(By parentLocator, By childLocator, WaitType waitType);

	void waitForVisibilityOfNestedMobileElementsLocatedBy(By parentLocator, By childLocator, WaitType waitType);

	void waitForPresenceOfAllMobileElementsLocatedBy(By locator, WaitType waitType);

	void waitForPresenceOfAllMobileElementLocatedBy(By locator, WaitType waitType);

	void waitForPresenceOfNestedMobileElementLocatedBy(By locator1, By locator2, WaitType waitType);

	void waitForPresenceOfNestedMobileElementLocatedBy(MobileElement element, By childLocator, WaitType waitType);

	void waitForpresenceOfNestedMobileElementsLocatedBy(By parentLocator, By childLocator, WaitType waitType);

	void waitToBeRefreshed(ExpectedCondition<T> condition, WaitType waitType);
	/**
	 * An expectation to wait and check that the mobile element with text is either invisible or not present on the DOM.
	 * @param locator
	 * @param text
	 * @param waitType
	 */
	void waitForElementWithTextToBeInvisible(By locator, String text, WaitType waitType);
	/**
	 * An expectation to wait and check for attribute to be present in current mobile element.
	 * @param locator
	 * @param attributeName
	 * @param waitType
	 */
	void waitForElementAttributeToBePresent(By locator, String attributeName, WaitType waitType);
}

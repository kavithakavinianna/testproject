package uiWrappers.mobileWrappers.wrappers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import configuration.driverManager.DriverManager;
import exceptionHandler.CustomExceptionHandler;
import io.appium.java_client.MobileElement;
import testConfiguration.Singleton;
import uiWrappers.mobileWrappers.interfaces.IMobileVerifies;

public class MobileVerify implements IMobileVerifies {

	private MobileActions Action;

	public MobileVerify() {
		Action = Singleton.getInstance(MobileActions.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#elementIsPresent(org.openqa.selenium.By)
	 */
	@Override
	public void elementIsPresent(By by) {
		List<MobileElement> elements = DriverManager.getMobileDriver().findElements(by);
		if (elements.size() < 1) {
			CustomExceptionHandler.ExceptionHandler(new NoSuchElementException(
					"Validation Failed : Expected Element:" + by + ", is not present in the DOM."));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#elementIsNotPresent(org.openqa.selenium.
	 * By)
	 */
	@Override
	public void elementIsNotPresent(By by) {
		WebElement element = Action.findElement(by, false);
		if (element != null) {
			CustomExceptionHandler.ExceptionHandler(new Exception(
					"Validation Failed. Element " + by + " should not be present in the DOM, but it is present."));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#textboxIsEmpty(org.openqa.selenium.By)
	 */
	@Override
	public void textboxIsEmpty(By by) {
		String textBoxValue = Action.getAttributeValue(by, "value");
		if (!(textBoxValue.trim().length() == 0)) {
			CustomExceptionHandler.ExceptionHandler(new Exception(
					"Validation Failed : The text-box '" + by + "' is NOT Empty; it contains : " + textBoxValue));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#elementContainsText(org.openqa.selenium.
	 * By, java.lang.String)
	 */
	@Override
	public void elementContainsText(By by, String text) {
		String uiText = Action.getText(by);
		if (!uiText.contains(text)) {
			CustomExceptionHandler.ExceptionHandler(new Exception("Validation Failed : Element text '" + uiText
					+ "' does NOT contain expected '" + text + "' : " + by));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#elementDoesNotContainsText(org.openqa.
	 * selenium.By, java.lang.String)
	 */
	@Override
	public void elementDoesNotContainsText(By by, String text) {
		String uiText = Action.getText(by);
		if (uiText.contains(text)) {
			CustomExceptionHandler.ExceptionHandler(new Exception("Validation Failed : Element(" + by
					+ ") contains unexpected text '" + text + ", Actual text " + uiText));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#exactTextInElementIs(org.openqa.selenium.
	 * By, java.lang.String)
	 */
	@Override
	public void exactTextInElementIs(By by, String text) {
		String uiText = Action.getText(by);
		if (!uiText.equals(text)) {
			CustomExceptionHandler.ExceptionHandler(new Exception("Validation Failed : Element text '" + uiText
					+ "' does NOT match expected '" + text + "' : " + by));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#exactTextInElementIsNot(org.openqa.
	 * selenium.By, java.lang.String)
	 */
	@Override
	public void exactTextInElementIsNot(By by, String text) {
		String uiText = Action.getText(by);
		if (uiText.equals(text)) {
			CustomExceptionHandler.ExceptionHandler(new Exception(
					"Validation Failed : Element text '" + uiText + "' is SAME as '" + text + "' : " + by));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#elementIsEnabled(org.openqa.selenium.By)
	 */
	@Override
	public void elementIsEnabled(By by) {
		if (!Action.isEnabled(by)) {
			CustomExceptionHandler.ExceptionHandler(new Exception("Validation Failed : Element is NOT Enabled : " + by));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#elementIsDisabled(org.openqa.selenium.By)
	 */
	@Override
	public void elementIsDisabled(By by) {
		if (!Action.isEnabled(by)) {
			CustomExceptionHandler.ExceptionHandler(new Exception("Validation Failed : Element is NOT Disabled : " + by));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#elementIsSelected(org.openqa.selenium.By)
	 */
	@Override
	public void elementIsSelected(By by) {
		if (!Action.isSelected(by)) {
			CustomExceptionHandler.ExceptionHandler(new Exception("Validation Failed : Element" + by + " is NOT selected."));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#elementIsNotSelected(org.openqa.selenium.
	 * By)
	 */
	@Override
	public void elementIsNotSelected(By by) {
		if (Action.isSelected(by)) {
			CustomExceptionHandler.ExceptionHandler(new Exception("Validation Failed : Element: " + by + " is selected."));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#exactPageTitle(java.lang.String)
	 */
	@Override
	public void exactPageTitle(String ExpectedPageTitle) {
		try {
			String ActualPageTitle = DriverManager.getMobileDriver().getTitle();

			Assert.assertTrue(ActualPageTitle.equals(ExpectedPageTitle), "Mismatch in page title. Actual page title : "
					+ ActualPageTitle + ", Expected page title : " + ExpectedPageTitle);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception thrown while trying to validate exact page title: " + ExpectedPageTitle);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mobile.helpers.IMobileVerifies#partialPageTitle(java.lang.String)
	 */
	@Override
	public void partialPageTitle(String PartialPageTitle) {
		try {
			String ActualPageTitle = DriverManager.getMobileDriver().getTitle();
			Assert.assertTrue(ActualPageTitle.contains(PartialPageTitle),
					"Actual page title does not contain expected page title. Actual page title : " + ActualPageTitle
							+ ", Expected page title : " + PartialPageTitle);
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e,
					"Exception thrown while trying to validate partial page title: " + PartialPageTitle);
		}
	}

	public boolean isAlertPresent() {
		boolean result = false;
		try {
			DriverManager.getMobileDriver().switchTo().alert();
			result = true;
		} catch (Exception e) {

		}
		return result;
	}
}

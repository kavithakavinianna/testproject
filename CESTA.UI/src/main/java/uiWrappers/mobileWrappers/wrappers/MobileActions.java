package uiWrappers.mobileWrappers.wrappers;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static java.time.Duration.ofSeconds;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import io.appium.java_client.TouchAction;
import configuration.driverManager.DriverManager;
import exceptionHandler.CustomExceptionHandler;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileBy.ByAndroidUIAutomator;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.android.nativekey.PressesKey;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;
import uiWrappers.mobileWrappers.interfaces.IMobileActions;

/**
 * This class will provide all the mobile generic methods
 * 
 * @author sushmitak
 *
 */
public class MobileActions implements IMobileActions {

	/**
	 * Method to get object for TouchAction class.
	 * 
	 * @return actions obj
	 */
	@SuppressWarnings("rawtypes")
	private TouchAction getTouchActionsObj() {

		TouchAction actions = null;

		try {
			actions = new TouchAction(DriverManager.getMobileDriver());
		} catch (Exception e) {

			throw e;
		}
		return actions;
	}

	/**
	 * Method to find mobile element
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * @param throwException
	 *            : true or false based on requirement to throw exception.
	 * @return element
	 */
	public MobileElement findElement(By locator, boolean... throwException) {

		if (throwException.length == 0) {
			throwException = new boolean[1];
			throwException[0] = true;
		}

		MobileElement element = null;
		// int retryCount = 0;
		int retry = 0;
		TestEnvironment testEnvironment = Singleton.getInstance(TestEnvironment.class);

		try {
			retry = testEnvironment.containsProperty("RetryCount")
					? Integer.parseInt(testEnvironment.getProperty("RetryCount")) : 0;
			do {
				try {
					retry--;
					element = DriverManager.getMobileDriver().findElement(locator);
				} catch (Exception e) {
					if (retry > 0) {
						continue;
					} else {
						throw e;
					}
				}
			} while ((retry > 0) && (element == null));
		} catch (Exception e) {
			if (throwException[0]) {
				throw e;
			}
		}
		return element;
	}

	/**
	 * Method to find mobile elements
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * @return elements
	 */
	public List<MobileElement> findElements(By locator) {
		List<MobileElement> elements = null;
		try {
			elements = DriverManager.getMobileDriver().findElements(locator);
		} catch (Exception e) {

			throw e;

		}
		return elements;
	}

	/**
	 * Method to send the text by providing "content-desc" as id value.
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * @param text
	 *            : Text to be inserted in the element.
	 */
	public void sendKeys(By locator, String text) {
		try {
			findElement(locator).setValue(text);

		} catch (Exception e) {

			throw e;
		}
	}

	/**
	 * Method to refresh the app.
	 * 
	 * @throws Exception
	 * 
	 */
	public void refreshApp() {

		DriverManager.getMobileDriver().navigate().refresh();
	}

	/**
	 * Method to send the text by providing "content-desc" as id value.
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * @param text
	 *            : Text to be inserted in the element.
	 */
	public void hideKeyBoard() {
		try {
			DriverManager.getMobileDriver().hideKeyboard();

		} catch (Exception e) {

			throw e;
		}
	}

	/**
	 * click method help for tapping the mobile element.
	 * 
	 * @param locator:
	 *            a By locator of the mobile element.
	 */
	public void click(By locator) {
		try {

			findElement(locator).click();
		}

		catch (Exception e) {

			throw e;

		}
	}

	/**
	 * clear method help for clearing text from mobile field element.
	 * 
	 * @param locator:
	 *            a By locator of the mobile element.
	 */
	public void clear(By locator) {
		try {
			findElement(locator).clear();
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method to long press by providing "content-desc" as id value.
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * 
	 */
	public void longPress(By locator) {
		try {
			getTouchActionsObj().longPress(LongPressOptions.longPressOptions()).release().perform();
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method to swipe by providing the co-ordinates from one point to another
	 * point
	 */
	public void flickWithCoordinates(int startx, int starty, int endx, int endy) {
		try {
			/*
			 * getTouchActionsObj().flick(startx, starty).move(endx,
			 * endy).release().perform();
			 */
		}

		catch (Exception e) {
			throw e;
		}

	}

	/**
	 * Method to perform scroll operation by providing exact text to click
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * @param text
	 *            : Text to be inserted in the element.
	 */
	public void scrollByText(By locator, String text) {
		try {
			DriverManager.getMobileDriver().findElementByAccessibilityId("Views").click();
			AndroidElement list = (AndroidElement) findElement(locator);
			ByAndroidUIAutomator element = new MobileBy.ByAndroidUIAutomator(
					"new UiScrollable(new UiSelector()).scrollIntoView(" + "new UiSelector().text(\"" + text + "\"));");
			MobileElement group = list.findElement(element);

			if ((group.getLocation()) != null) {
				group.click();
			}
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method to drag by fetching the current location as co-ordinates to given
	 * element
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 * @param xOffset
	 *            : an Integer value for endpoint x-coordinate
	 * @param yOffset
	 *            : an Integer value for endpoint y-coordinate
	 */
	@SuppressWarnings("unused")
	public void drag(By locator, int xOffset, int yOffset) {
		try {

			WebElement webElement = findElement(locator);

			/*
			 * getTouchActionsObj().dragAndDropBy(webElement, xOffset, yOffset);
			 */ }

		catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method to click on android home button
	 */
	public void clickHomeButton() {

		try {
			((PressesKey) DriverManager.getMobileDriver()).pressKey(new KeyEvent(AndroidKey.HOME));

		} catch (Exception e) {
			throw e;
		}

	}

	/**
	 * 
	 * Method to perform zoom action
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 */

	public void Zoom(By locator) {
	}

	/**
	 * Method to perform zoom out
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 */
	public void Pinch(By locator) {

	}

	/**
	 * Executes javascript in the context of the currently selected frame or
	 * screen.
	 * 
	 * @param locator
	 *            : a By locator of the mobile element.
	 */
	public void executeJavascript(String script, Object[] args) {
		try {

			JavascriptExecutor js = (JavascriptExecutor) DriverManager.getMobileDriver();

			js.executeScript(script, args);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * This method will get the title
	 */
	public String getTitle() {

		String _title = null;
		try {
			_title = DriverManager.getMobileDriver().getTitle();
		} catch (Exception e) {
			throw e;
		}
		return _title;
	}

	/**
	 * This method helps to find mobile element by IosUIAutomation
	 * 
	 * @param selector
	 *            : finds a IOS locator.
	 */

	public MobileElement findElementByIosUIAutomation(String selector) {
		MobileElement _uiautomationelement = null;

		try {
			_uiautomationelement = ((IOSDriver<MobileElement>) DriverManager.getMobileDriver())
					.findElementByIosUIAutomation(selector);

		} catch (Exception e) {

			throw e;
		}
		return _uiautomationelement;
	}

	/**
	 * This method helps to find web element element by IosUIAutomation
	 * 
	 * @param selector
	 *            : finds a IOS locator.
	 */
	public WebElement findElementByIosAutomation(String selector) {
		WebElement _element = null;
		try {
			_element = ((IOSDriver<MobileElement>) DriverManager.getMobileDriver())
					.findElementByIosUIAutomation(selector);
		} catch (Exception e) {
			throw e;
		}
		return _element;
	}

	/**
	 * To simulate Touch ID on iOS Simulators.
	 * 
	 * @param match
	 *            : Set the desired capability "allowTouchIdEnroll" to true
	 */

	public void PerformTouchId(boolean match) {
		try {
			((IOSDriver<MobileElement>) DriverManager.getMobileDriver()).performTouchID(match);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method to perform a shake action on the device
	 */

	public void Shake() {
		try {

			((IOSDriver<MobileElement>) DriverManager.getMobileDriver()).shake();
		} catch (Exception e) {
			throw e;
		}

	}

	/**
	 * Method to get the contexts available for the mobile App
	 * 
	 * @return contextNames
	 */
	public Set<String> getAppContexts() {

		Set<String> contextNames = null;

		try {
			contextNames = DriverManager.getMobileDriver().getContextHandles();

		} catch (Exception e) {

			throw e;
		}
		return contextNames;
	}

	/**
	 * Method to which to desired view in the pass by passing the view
	 * 
	 * @param contextName
	 */
	public void switchToAppContext(String contextName) {

		Collection<String> appContextNames = getAppContexts();
		try {
			{
				for (String context : appContextNames) {
					if (contextName.contains(context)) {

						DriverManager.getMobileDriver().context(context);
						break;
					}
				}
			}
		} catch (Exception e) {
			throw e;
		}

	}

	/**
	 * Method to switch to webview of the mobile app
	 */
	public void switchtoWebView() {
		Set<String> AppContexts = getAppContexts();
		try {
			for (String context : AppContexts) {
				if (context.contains("WEBVIEW")) {
					DriverManager.getMobileDriver().context(context);
					break;
				}
			}
		} catch (Exception e) {
			throw e;
		}

	}

	/**
	 * Method to switch to native view of the mobile app
	 */
	public void switchtoNativeView() {
		Set<String> AppContexts = getAppContexts();
		try {
			for (String context : AppContexts) {
				if (context.contains("NATIVE")) {
					DriverManager.getMobileDriver().context(context);
					break;
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Method to move/scroll to provided element on UI
	 * 
	 * @param locator
	 */
	public void moveToElement(By locator) {
		/*
		 * Actions action = new Actions(DriverManager.getMobileDriver()); try {
		 * action.moveToElement(findElement(locator)).build().perform(); } catch
		 * (Exception e) { throw e; }
		 */ }

	/**
	 * Gets the value of the specified attribute within given locator
	 * 
	 * @param locator:
	 *            By locator will passed from project level
	 */

	public String getAttributeValue(By locator, String attributeName) {
		String value = null;
		WebElement element;
		try {
			element = findElement(locator);
			value = element.getAttribute(attributeName);
		} catch (Exception e) {
			throw e;
		}
		return value;
	}

	/**
	 * Gets the innerText of the given locator
	 * 
	 * @param locator:
	 *            By locator will passed from project level
	 */

	public String getText(By locator) {
		String text = null;
		WebElement element;
		try {
			element = findElement(locator);
			text = element.getText();
		} catch (Exception e) {
			throw e;
		}
		return text;
	}

	/**
	 * Gets a value indicating whether or not this element is enabled
	 * 
	 * @param locator:
	 *            By locator will passed from project level
	 */

	public boolean isEnabled(By locator) {
		boolean flag = false;
		try {
			WebElement element = findElement(locator);
			flag = element.isEnabled();
		} catch (Exception e) {
			flag = false;
			throw e;
		}
		return flag;
	}

	/**
	 * Gets a value indicating whether or not this element is selected
	 * 
	 * @param locator:
	 *            By locator will passed from project level
	 */

	public boolean isSelected(By locator) {
		boolean flag = false;
		WebElement element = null;
		try {
			element = findElement(locator);
			flag = element.isSelected();
		} catch (Exception e) {
			flag = false;
			throw e;
		}
		return flag;
	}

	/**
	 * Method to long press by providing "content-desc" as id value
	 * 
	 * @param locator
	 */
	public void longClick(By locator) {
		try {
			/*
			 * getTouchActionsObj().longPress(DriverManager.getMobileDriver().
			 * findElement( locator)).release().perform();
			 */ } catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception occured when performing long press on element " + locator);
		}
	}

	/**
	 * Method to swipe by providing the co-ordinates from one point to another
	 * point
	 * 
	 * @param startx
	 * @param starty
	 * @param endx
	 * @param endy
	 */
	@SuppressWarnings("rawtypes")
	public void swipeWithCoordinates(int startx, int starty, int endx, int endy) {
		try {
			TouchAction action = new TouchAction(DriverManager.getMobileDriver());
			action.tap(PointOption.point(209, 322)).waitAction(WaitOptions.waitOptions(ofSeconds(1))).release()
					.perform();
		} catch (Exception e) {
			CustomExceptionHandler.ExceptionHandler(e, "Exception when performing swipe action using provided coordinates");
		}
	}

	public void toZoom(By locator) {

	}

	public void toPinch(By locator) {

	}

	@SuppressWarnings("rawtypes")
	public void tapByCoordinates(int x, int y) {
		PointOption pointOption = new PointOption();
		pointOption.withCoordinates(x, y);
		new TouchAction(DriverManager.getMobileDriver()).tap(pointOption).perform();

	}

	// Tap to an element for 250 milliseconds
	@SuppressWarnings("rawtypes")
	public void tapByElement(By Locator) {
		new TouchAction(DriverManager.getMobileDriver()).tap(tapOptions().withElement(element(findElement(Locator))))
				.waitAction(waitOptions(Duration.ofMillis(250))).perform();
	}

	// Multitouch action by using an android element
	public void multiTouchByElement(By Locator) {
	}
}

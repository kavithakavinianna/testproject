package uiWrappers.mobileWrappers.wrappers;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import configuration.driverManager.DriverManager;
import exceptionHandler.CustomExceptionHandler;
import io.appium.java_client.MobileElement;
import testConfiguration.Singleton;
import testConfiguration.TestEnvironment;
import uiWrappers.WaitType;
import uiWrappers.mobileWrappers.interfaces.IMobileWaits;

/**
 * This class defines variety of verify required when performing different
 * actions on a mobile applications.
 * 
 * @author sushmitak
 *
 */
public class MobileWaits implements IMobileWaits {

	private TestEnvironment testEnvironment;

	/**
	 * Creates new instance of TestEnvironment
	 * 
	 * @param testEnvironment
	 */
	public MobileWaits() {
		try {

			testEnvironment = Singleton.getInstance(TestEnvironment.class);

		} catch (Exception e) {
			throw e;
		}

	}

	/**
	 * Set the amount of time the driver should wait when searching for elements
	 */
	public void setImplicitWaitTimeOut()  {
		try {
			DriverManager.getMobileDriver().manage().timeouts().wait(testEnvironment.ImplicitWaitTimeout);
		} 

		catch (Exception e) {
			e.printStackTrace();

		}
	}

	/**
	 * Configure the amount of time that a particular type of operation can execute
	 * for before they are aborted * @waitType:
	 * waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed from project
	 * level
	 */
	public void setPageLoadTimeout(WaitType waitType)  {
		try {
			int duration = getWaitType(waitType);
			DriverManager.getMobileDriver().manage().timeouts().wait(duration);
		} 
		catch (Exception e) {

			e.printStackTrace();

		}
	}

	/**
	 * Set the amount of time, in milliseconds, that scripts execution are permitted
	 * to run before they are aborted (Web context only)
	 * 
	 * @waitType: waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed
	 *            from project level
	 */

	public void setScriptTimeout(WaitType waitType)  {
		try {
			int duration = getWaitType(waitType);
			DriverManager.getMobileDriver().manage().timeouts().wait(duration);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	/**
	 * An expectation to wait and check for attribute to be present in current
	 * mobile element. * @locator: By locator has to be passed from project level
	 * * @attributeName: String value the element attribute name has to be passed
	 * from project level * @waitType: waitType(Small,Medium,Large,ImplicitWaitTime
	 * ) has to be passed from project level
	 */

	public void waitToGetAttribute(By locator, String attributeName, WaitType waitType)

	{
		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.attributeToBe(locator, attributeName,
					DriverManager.getMobileDriver().findElement(locator).getAttribute(attributeName)));
		}

		catch (Exception e) {

			throw e;

		}
	}

	/**
	 * An expectation to wait and check that the mobile element with text is either
	 * invisible or not present on the DOM. * @locator: By locator has to be passed
	 * from project level * @text: String value of text must be passed from project
	 * level * @waitType: waitType(Small,Medium,Large,ImplicitWaitTime ) has to be
	 * passed from project level
	 */

	public void waitForInvisibilityOfMobileElementWithText(By locator, String text, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.invisibilityOfElementWithText(locator, text));

		} catch (Exception e) {

			throw e;

		}

	}

	/**
	 * An expectation to wait and check that the given text is present in the given
	 * * @locator: By locator has to be passed from project level 
	 * *@text:String value of text must be passed from project level * @waitType:
	 * waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed from project
	 * level
	 */
	public void WaitForTextToBePresentInMobileElement(By locator, String text, WaitType waitType) {

		try {

			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), waitType.ordinal());
			wait.until(ExpectedConditions.textToBePresentInElement(DriverManager.getMobileDriver().findElement(locator),
					text));

		} catch (Exception e) {

			throw e;

		}
	}

	/**
	 * An expectation to wait till alert is present
	 * waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed from project
	 * 
	 */

	public void waitForAlertIsPresent(WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.alertIsPresent());

		} catch (Exception e) {

			throw e;

		}

	}

	/**
	 * An expectation to wait and verify if attribute contains value
	 * @waitType(Small,Medium,Large,ImplicitWaitTime ) has to be passed from project
	 * @attribute: Pass Attribute type from project level
	 * @value: Pass Attribute value type from project level
	 * @locator: By locator has to be passed from project level 
	 */

	public void waitIfAttributeContains(By locator, String attribute, String value, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.attributeContains(locator, attribute, value));

		} catch (Exception e) {

			throw e;

		}

	}


	/* (non-Javadoc)
	 * @see mobile.interfaces.IMobileWaits#waitIfAttributeContains(io.appium.java_client.MobileElement, java.lang.String, java.lang.String, helpers.WaitType)
	 */
	public void waitIfAttributeContains(MobileElement element, String attribute, String value, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.attributeContains(element, attribute, value));

		} catch (Exception e) {

			throw e;

		}

	}



	public void waitToGetAttribute(MobileElement element, String attribute, String value, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.attributeToBe(element, attribute, value));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForAttributeToBeNotEmpty(MobileElement element, String attribute, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.attributeToBeNotEmpty(element, attribute));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForAttributeToBeNotEmpty(By locator, Boolean selected, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.elementSelectionStateToBe(locator, selected));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForMobileElementSelectionStateToBe(By locator, Boolean selected, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.elementSelectionStateToBe(locator, selected));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForMobileElementSelectionStateToBe(MobileElement element, Boolean selected, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.elementSelectionStateToBe(element, selected));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForMobileElementToBeClickable(By locator, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.elementToBeClickable(locator));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForMobileElementToBeClickable(MobileElement element, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.elementToBeClickable(element));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForMobileElementToBeSelected(By locator, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.elementToBeSelected(locator));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForMobileElementToBeSelected(MobileElement element, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.elementToBeSelected(element));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForFrameToBeAvailableAndSwitchToIt(By locator, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(locator));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForFrameToBeAvailableAndSwitchToIt(int frameLocator, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));

		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForFrameToBeAvailableAndSwitchToIt(String frameLocator, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));
		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForFrameToBeAvailableAndSwitchToIt(MobileElement frameLocator, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));
		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForInvisibilityOfMobileElement(MobileElement element, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.invisibilityOf(element));
		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForInvisibilityOfMobileElement(List<MobileElement> elements, WaitType waitType) {

		try {
			List<WebElement> webElements = new ArrayList<WebElement>();
			for (MobileElement element : elements) {
				webElements.add(element);
			}
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.invisibilityOfAllElements(webElements));
		} catch (Exception e) {

			throw e;

		}

	}

	public void waitForInvisibilityOfMobileElementLocated(By locator, WaitType waitType) {
		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForMobileElementSelectionStateToBe(By locator, boolean selected, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.elementSelectionStateToBe(locator, selected));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForMobileElementSelectionStateToBe(MobileElement element, boolean selected, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.elementSelectionStateToBe(element, selected));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForNumberOfElementsToBe(By locator, int number, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.numberOfElementsToBe(locator, number));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForNumberOfElementsToBeLessThan(By locator, int number, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.numberOfElementsToBeLessThan(locator, number));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForNumberOfElementsToBeMoreThan(By locator, int number, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(locator, number));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForNumberOfElementsToBeMoreThan(int expectedNumberOfWindows, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.numberOfWindowsToBe(expectedNumberOfWindows));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForTextToBePresentInMobileElement(MobileElement element, String text, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.textToBePresentInElement(element, text));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForTextToBePresentInMobileElementValue(By locator, String text, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.textToBePresentInElementValue(locator, text));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForTextToBePresentInMobileElementValue(MobileElement element, String text, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.textToBePresentInElementValue(element, text));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForTextToBe(By locator, String value, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.textToBe(locator, value));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForTextToBePresentInMobileElementLocated(By locator, String text, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.textToBePresentInElementLocated(locator, text));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForTitleContains(String title, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.titleContains(title));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitFortitleIsExactMatch(String title, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.titleIs(title));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitIfUrlContains(String fraction, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.urlContains(fraction));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitIfUrlMatches(String regex, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.urlMatches(regex));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitIfUrlToBe(String url, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.urlToBe(url));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForVisibilityOfMobileElement(MobileElement element, WaitType waitType) {

		try {
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.visibilityOf(element));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForVisibilityOfMobileElements(List<MobileElement> elements, WaitType waitType) {

		try {

			List<WebElement> webElement = new ArrayList<WebElement>();

			for (MobileElement mobileElement : elements) {

				webElement.add(mobileElement);
			}
			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.visibilityOfAllElements(webElement));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForVisibilityOfAllMobileElementsLocatedBy(By locator, WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForVisibilityOfMobileElementLocated(By locator, WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForVisibilityOfMobileElementLocated(By parentLocator, By childLocator, WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(parentLocator, childLocator));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForVisibilityOfNestedMobileElementLocatedBy(By parentLocator, By childLocator, WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(parentLocator, childLocator));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForVisibilityOfNestedMobileElementsLocatedBy(By parentLocator, By childLocator, WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(parentLocator, childLocator));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForPresenceOfAllMobileElementsLocatedBy(By locator, WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForPresenceOfAllMobileElementLocatedBy(By locator, WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForPresenceOfNestedMobileElementLocatedBy(By locator1, By locator2, WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(locator1, locator2));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForPresenceOfNestedMobileElementLocatedBy(MobileElement element, By childLocator,
			WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(element, childLocator));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitForpresenceOfNestedMobileElementsLocatedBy(By parentLocator, By childLocator, WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.presenceOfNestedElementsLocatedBy(parentLocator, childLocator));
		}

		catch (Exception e) {

			throw e;

		}

	}

	public void waitToBeRefreshed(ExpectedCondition<T> condition, WaitType waitType) {

		try {

			int duration = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), duration);
			wait.until(ExpectedConditions.refreshed(condition));
		}

		catch (Exception e) {

			throw e;

		}

	}

	private int getWaitType(WaitType type) {
		int waitDuration;
		switch (type) {
		case Small:
			waitDuration = testEnvironment.SmallWait;
			break;
		case Medium:
			waitDuration = testEnvironment.MediumWait;
			break;

		case Large:
			waitDuration = testEnvironment.LargeWait;
			break;
		case Implicit:
			waitDuration = testEnvironment.ImplicitWaitTimeout;
			break;
		case Explicit:
			waitDuration = testEnvironment.ExplicitWaitTimeout;
			break;
		case PageLoad:
			waitDuration = testEnvironment.PageLoadTimeout;
			break;
		default:
			throw new IllegalArgumentException("Ivalid waitType provided : " + type.toString());
		}
		return waitDuration;
	}
	/**
	 * An expectation to wait and check for attribute to be present in current mobile element.
	 * @param locator
	 * @param attributeName
	 * @param waitType
	 */
	public void waitForElementAttributeToBePresent(By locator, String attributeName, WaitType waitType)
	{
		try
		{
			int time = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), time);
			wait.until(ExpectedConditions.attributeToBeNotEmpty(DriverManager.getMobileDriver().findElement(locator),attributeName));
		}
		catch (Exception e)
		{
			String message = String.format(" Locator : '{0}' and trying to get attribute '{1}'", locator, attributeName);
			CustomExceptionHandler.ExceptionHandler(e, message);
		}
	}
	/**
	 * An expectation to wait and check that the mobile element with text is either invisible or not present on the DOM.
	 * @param locator
	 * @param text
	 * @param waitType
	 */
	public void waitForElementWithTextToBeInvisible(By locator, String text, WaitType waitType)
	{
		boolean result = false;
		try
		{
			int time = getWaitType(waitType);
			WebDriverWait wait = new WebDriverWait(DriverManager.getMobileDriver(), time);
			result = wait.until(ExpectedConditions.invisibilityOfElementWithText(locator, text));
			if (!result)
				throw new TimeoutException();
		}
		catch (TimeoutException e)
		{
			String message = String.format(" Failed due to the text '{0}' in the element '{1}' is not disappeared in given wait time", text, locator);
			CustomExceptionHandler.ExceptionHandler(e, message);
		}
		catch (Exception e)
		{
			CustomExceptionHandler.ExceptionHandler(e, "Exception occured while waiting for element(" + locator + ") with text:" + text + " to be invisible, with wait type: " + waitType.toString());
		}
	}
}
